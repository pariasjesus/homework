/* 
 * File:   Menus.cpp
 * Author: Jesus Parias-Castillo
 * Created on June 8, 2019, 9:02 PM
 */

#include "Bush.h"

Bush::Bush()
    :Plant(){
    area=0;
    type=bush;
}
Bush::Bush(string name)
    :Plant(name){
    area=0;
    type=bush;
}
Bush::Bush(string name, int type)
    :Plant(name, type){
    area=0;
}
Bush::Bush(const Bush &bush)
    :Plant(bush){
    this->area=bush.area;
}
const Bush &Bush::operator=(const Bush &bush){
    if(this==&bush){
       return *this; 
    }
    Plant::operator =(bush);
    this->area=bush.area;
    return *this;
}


Bush::~Bush(){}
void Bush::output(){
    cout<<this;
}
ostream &operator <<(ostream &out,const Bush &bush){
    out<<"Name: "<<bush.name<<"\n"
            <<"Height: "<<bush.height<<"\n"
            <<"Stage: "<<getLifeStageString(bush.lifeStage)<<"\n"
            <<"Type: "<<getTypeString(bush.type)<<"\n"
            <<"Age: "<<bush.age<<"\n"
            <<"Description: "<<bush.description<<"\n"
            <<"Area: "<<bush.area<<"\n";
    return out;
}
ostream &operator <<(ostream &out,const Bush* bush){
    out<<"Name: "<<bush->name<<"\n"
            <<"Height: "<<bush->height<<"\n"
            <<"Stage: "<<getLifeStageString(bush->lifeStage)<<"\n"
            <<"Type: "<<getTypeString(bush->type)<<"\n"
            <<"Age: "<<bush->age<<"\n"
            <<"Description: "<<bush->description<<"\n"
            <<"Area: "<<bush->area<<"\n";
    return out;
}
void Bush::setArea(int areas){
    this->area=areas;
}
int Bush::getArea(){
    return area;
}
void Bush::askArea(){
    try{
        int maxOps=typeMax;
        bool num=true;
        const int maxChars = 50;
        char* temp= new char[maxChars];
        cout<<"How much area does the tree take up in meters?: ";
        cin>>temp;
        
        for(int i=0;temp[i]!='\0';i++){
            if(!isdigit(temp[i])){
                num=false;
            }
        }
        if(num==false||atoi(temp)<0){
            delete[] temp;
            string error=(num==false?"Error: character found; please enter a number \n":"Please Enter a valid option\n");
            throw error;
        }
        area=atoi(temp);
        delete[] temp;
    }
    catch(string error){
        cout<<error;
        askArea();
    }
}
void Bush::editPlantMenu(char* choice){
    try{
        cout<<this;
        int maxOps =7;
        cout<<"What would you like to do?"<<endl;
        cout<<"[0]Change Name\n"
                <<"[1]Change Height\n"
                <<"[2]Change Age\n"
                <<"[3]Change LifeStage\n"
                <<"[4]Change Type\n"
                <<"[5]Change Description\n"
                <<"[6]Change Area Taken Up"
                <<"[7]Change Everything\n";
        cin>>choice;
        if(atoi(choice)>maxOps||atoi(choice)<0){
            string error="Please Enter a valid option\n";
            throw error;
        }
        switch(atoi(choice)){
            case 0:
                askName();
                break;
            case 1:
                askHeight();
                break;
            case 2:
                askAge();
                break;
            case 3:
                askLifeStage();
                break;
            case 4:
                askType();
                break;
            case 5:
                askDescription();
                break;
            case 6:
                askArea();
                break;
            case 7:
                askName();
                askHeight();
                askAge();
                askLifeStage();
                askType();
                askDescription();
                askArea();
                break;
            default:
                cout<<"Error: option does not exist"<<endl;
                break;
        }
    }
    catch(string error){
        cout<<error;
        editPlantMenu(choice);
    }
}
void Bush::editPlantMenu(){
    askName();
    askHeight();
    askAge();
    askLifeStage();
    askType();
    askDescription();
    askArea();
}
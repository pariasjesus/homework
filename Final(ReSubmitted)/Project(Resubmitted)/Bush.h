/* 
 * File:   Menus.cpp
 * Author: Jesus Parias-Castillo
 * Created on June 8, 2019, 9:02 PM
 */

#ifndef BUSH_H
#define BUSH_H
#include "Plant.h"
#include "Tree.h"

//bush plant
class Bush: public Plant{
    //bushes grow fatter and not taller
private:
    int area;    //area in square feet bush takes up
public:
    //constructors
    Bush();
    Bush(string);
    Bush(string,int);
    Bush(const Bush &);
    
    //assignment operator
    const Bush &operator=(const Bush &);
    
    //destructor
    ~Bush();
    
    //outputs
    void output();
    friend ostream& operator<<(ostream&,const Bush &);
    friend ostream& operator<<(ostream&,const Bush*);
    
    //setters
    void setArea(int);
    
    //getters
    int getArea();
    
    //asks user for input
    void askArea();
    
    //menus
    void editPlantMenu(char*);
    void editPlantMenu();
};

#endif /* BUSH_H */


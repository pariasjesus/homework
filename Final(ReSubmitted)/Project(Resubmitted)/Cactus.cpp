/* 
 * File:   Menus.cpp
 * Author: Jesus Parias-Castillo
 * Created on June 8, 2019, 9:02 PM
 */

#include "Cactus.h"

Cactus::Cactus()
    :Plant(){
    needleLength=0;
    waterDays=0;
    type=cactus;
}
Cactus::Cactus(string name)
    :Plant(name){
    needleLength=0;
    waterDays=0;
    type=cactus;
}
Cactus::Cactus(string name,int type)
    :Plant(name,type){
    needleLength=0;
    waterDays=0;
}
Cactus::Cactus(const Cactus &cactus)
    :Plant(cactus){
    this->needleLength=cactus.needleLength;
    this->waterDays=cactus.waterDays;
}
const Cactus &Cactus::operator=(const Cactus &cactus){
    if(this==&cactus){
       return *this; 
    }
    Plant::operator =(cactus);
    this->needleLength=cactus.needleLength;
    this->waterDays=cactus.waterDays;
    return *this;
}Cactus::~Cactus(){}
void Cactus::setWaterDays(int waterDays){
    this->waterDays=waterDays;
}
void Cactus::setNeedleLength(int needleLength){
    this->needleLength=needleLength;
}
int Cactus::getWaterDays(){
    return waterDays;
}
int Cactus::getNeedleLength(){
    return needleLength;
}
void Cactus::output(){
    cout<<this;
}
ostream &operator <<(ostream &out,const Cactus &cactus){
    out<<"Name: "<<cactus.name<<"\n"
            <<"Height: "<<cactus.height<<"\n"
            <<"Stage: "<<getLifeStageString(cactus.lifeStage)<<"\n"
            <<"Type: "<<getTypeString(cactus.type)<<"\n"
            <<"Age: "<<cactus.age<<"\n"
            <<"Description: "<<cactus.description<<"\n"
            <<"Days without Water: "<<cactus.needleLength<<"\n"
            <<"Length of Needles: "<<cactus.waterDays<<"\n";
    return out;
}
ostream &operator <<(ostream &out,const Cactus* cactus){
    out<<"Name: "<<cactus->name<<"\n"
            <<"Height: "<<cactus->height<<"\n"
            <<"Stage: "<<getLifeStageString(cactus->lifeStage)<<"\n"
            <<"Type: "<<getTypeString(cactus->type)<<"\n"
            <<"Age: "<<cactus->age<<"\n"
            <<"Description: "<<cactus->description<<"\n"
            <<"Days without Water: "<<cactus->waterDays<<"\n"
            <<"Length of Needles: "<<cactus->needleLength<<"\n";
    return out;
}
void Cactus::askNeedleLength(){
    try{
        bool num=true;
        const int maxChars = 50;
        char* temp= new char[maxChars];
        cout<<"How long are the needles in centimeters?: ";
        cin>>temp;
        
        for(int i=0;temp[i]!='\0';i++){
            if(!isdigit(temp[i])){
                num=false;
            }
        }
        if(num==false||atoi(temp)<0){
            delete[] temp;
            string error="Error: character found; please enter a number \n";
            throw error;
        }
        needleLength=atoi(temp);
        delete[] temp;
    }
    catch(string error){
        cout<<error;
        askNeedleLength();
    }
}
void Cactus::askWaterDays(){
    try{
        bool num=true;
        const int maxChars = 50;
        char* temp= new char[maxChars];
        cout<<"How many days can the cactus go without Water?: ";
        cin>>temp;
        
        for(int i=0;temp[i]!='\0';i++){
            if(!isdigit(temp[i])){
                num=false;
            }
        }
        if(num==false||atoi(temp)<0){
            delete[] temp;
            string error="Error: character found; please enter a number \n";
            throw error;
        }
        waterDays=atoi(temp);
        delete[] temp;
    }
    catch(string error){
        cout<<error;
        askHeight();
    }
}

void Cactus::editPlantMenu(char* choice){
    try{
        cout<<this;
        int maxOps =8;
        cout<<"What would you like to do?"<<endl;
        cout<<"[0]Change Name\n"
                <<"[1]Change Height\n"
                <<"[2]Change Age\n"
                <<"[3]Change LifeStage\n"
                <<"[4]Change Type\n"
                <<"[5]Change Description\n"
                <<"[6]Change Needle Length\n"
                <<"[7]Change Water days\n"
                <<"[8]Change Everything\n";
        cin>>choice;
        if(atoi(choice)>maxOps||atoi(choice)<0){
            string error="Please Enter a valid option\n";
            throw error;
        }
        switch(atoi(choice)){
            case 0:
                askName();
                break;
            case 1:
                askHeight();
                break;
            case 2:
                askAge();
                break;
            case 3:
                askLifeStage();
                break;
            case 4:
                askType();
                break;
            case 5:
                askDescription();
                break;
            case 6:
                askNeedleLength();
                break;
            case 7:
                askWaterDays();
                break;
            case 8:
                askName();
                askHeight();
                askAge();
                askLifeStage();
                askType();
                askDescription();
                askNeedleLength();
                askWaterDays();
                break;
            default:
                cout<<"Error: option does not exist"<<endl;
                break;
        }
    }
    catch(string error){
        cout<<error;
        editPlantMenu(choice);
    }
}
void Cactus::editPlantMenu(){
    askName();
    askHeight();
    askAge();
    askLifeStage();
    askType();
    askDescription();
    askNeedleLength();
    askWaterDays();
}

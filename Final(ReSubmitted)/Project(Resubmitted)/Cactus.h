/* 
 * File:   Menus.cpp
 * Author: Jesus Parias-Castillo
 * Created on June 8, 2019, 9:02 PM
 */

#ifndef CACTUS_H
#define CACTUS_H
#include "Plant.h"

//cactus plant
class Cactus: public Plant{
    //Cactus hold more water and have needles
private:
    int needleLength;    //needle length in centimeters
    int waterDays;   //days the cactus can go without water
public:
    //constructors
    Cactus();
    Cactus(string);
    Cactus(string,int);
    Cactus(const Cactus &);
    
    //assignment operator
    const Cactus &operator=(const Cactus &);
    
    //destructor
    ~Cactus();
    
    //outputs
    void output();
    friend ostream& operator<<(ostream&,const Cactus &);
    friend ostream& operator<<(ostream&,const Cactus*);
    
    //setters
    void setNeedleLength(int);
    void setWaterDays(int);
    
    //getters
    int getNeedleLength();
    int getWaterDays();
    
    //asks user for input
    void askNeedleLength();
    void askWaterDays();
    
    //menus
    void editPlantMenu(char*);
    void editPlantMenu();
};

#endif /* CACTUS_H */


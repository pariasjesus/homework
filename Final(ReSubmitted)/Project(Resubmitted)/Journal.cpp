/* 
 * File:   Menus.cpp
 * Author: Jesus Parias-Castillo
 * Created on June 8, 2019, 9:02 PM
 */

#include "Journal.h"

Journal::Journal(){
    size=0;
    firstPage=nullptr;
}
void Journal::append(string date,string heading,string entry,int pageNumber){
    size++;//increases size by 1
    
    //copies node information to new node
    Page* newPage= new Page;
    newPage->date=date;
    newPage->entry=entry;
    newPage->heading=heading;
    newPage->pageNumber=pageNumber;
    newPage->nextPage=nullptr;
    
    //if no head then assigns new node to head
    if(!firstPage){
        firstPage=newPage;
    }
    //else traverse to back and makes ending null value a new node
    else{
        Page* nodePtr=firstPage;
        while(nodePtr->nextPage){
            nodePtr=nodePtr->nextPage;
        }
        nodePtr->nextPage=newPage;
    }
}
void Journal::remove(int pageNumber){
    Page* nodePtr;  //traversal pointer
    Page* prev;     //previous pointer
    //checks if there is a firstpage
    //no then go back
    if(!firstPage){
        return;
    }
    //yes then check if i need to delete start
    if(firstPage->pageNumber==pageNumber){
        nodePtr=firstPage->nextPage;    //temperary pointr
        delete firstPage;
        firstPage=nodePtr;
    }else{
        //bedgin traversal
        nodePtr=firstPage;
        //traverse
        while(nodePtr!=nullptr&&nodePtr->pageNumber!=pageNumber){
            prev =nodePtr;
            nodePtr=nodePtr->nextPage;
        }
        //delete
        if(nodePtr){
            prev=nodePtr->nextPage;
            delete nodePtr;
        }
    }
}
Journal::~Journal(){
    //deletes current node and moves on to next
    Page* nodePtr=firstPage;
    while(nodePtr){
        Page* temp=nodePtr;
        nodePtr=nodePtr->nextPage;
        delete temp;
    }
    //once all nodes are deleted makes firstPage a null
    firstPage=nullptr;
}
void Journal::print(){
    Page* nodePtr=firstPage;
    //if no journal exists them does nothing
    if(!firstPage){
        cout<<"There are no entry in this journal"<<endl;
    }
    //else outputs page
    while(nodePtr){
        cout<<"___________________________________________"<<endl;
        cout<<"Page "<<(nodePtr->pageNumber)+1<<endl;
        cout<<nodePtr->date<<endl;
        cout<<nodePtr->heading<<endl;
        cout<<nodePtr->entry<<endl;
        nodePtr = nodePtr->nextPage;
    }cout<<endl;
}
int Journal::getSize(){
    return size;
}
void Journal::myJournalMenu(char* choice,int numberOfPlants,Plant** plants,Journal &journal){
    try{//try code
        print();//outputs journal so far
        cout<<"[A] Add Entry"<<endl;
        cout<<"[D] Remove Entry"<<endl;
        cout<<"[B] <-- Back"<<endl;
        cin>>choice;
        //adds entry
        if(choice[0]=='A'||choice[0]=='a'){
            cin.ignore();
            addEntryMenu();
            myJournalMenu(choice,numberOfPlants,plants,journal);
        }
        //deletes entry
        else if(choice[0]=='D'||choice[0]=='d'){
            askRemoveMenu(choice,size);
            myJournalMenu(choice,numberOfPlants,plants,journal);
        }//goes back
        else if(choice[0]=='B'||choice[0]=='b'){
            return;
        }
        //throws error if no case
        else{
            string error="Please Enter a valid option\n";
            throw error;
        }
    }
    //if catches error then calls self
    catch(string error){
        cout<<error;
        myJournalMenu(choice,numberOfPlants,plants,journal);
    }
}
void Journal::askDate(string &temp){
    //asks the user for the date
    string y;
    string m;
    string d;
    cout<<"Please enter the Month as a digit:";
    getline(cin,m);
    cout<<"Please enter the Day as a digit:";
    getline(cin,d);
    cout<<"Please enter the Year as a digit:";
    getline(cin,y);
    temp= string(m)+string("/")+string(d)+string("/")+string(y);
}
void Journal::askHeading(string &temp){
    cout<<"What would you like the title to say?"<<endl;
    getline(cin,temp);
}
void Journal::askEntry(string &temp){
    cout<<"What would you like to write?"<<endl;
    getline(cin,temp);
}
string Journal::getHeading(){
    return firstPage->heading;
}
string Journal::getEntry(){
    return firstPage->entry;
}
string Journal::getDate(){
    return firstPage->date;
}
void Journal::askRemoveMenu(char* choice,int maxOps){
    try{
        char* temp =new char[100];
        cout<<"Which page would you like to remove?:";
        cin>>temp;
        if(atoi(temp)>maxOps||atoi(temp)<0){
            delete[] temp;
            string error="Please Enter a valid option\n";
            throw error; 
        }else{
            char* check=new char[50];
            cout<<"Are you sure?:"<<endl;
            cin>>check;
            if(check[0]=='y'||check[0]=='Y'){
                cout<<"Deleting..."<<endl;
                remove(atoi(temp)-1);
                size--;
                delete[] temp;
            }
            else{
                delete[] temp;
                cout<<"Will not delete"<<endl;
            }
            delete[] check;
        }
    }
    catch(string error){
        cout<<error;
        return;
    }
}
void Journal::addEntryMenu(){
    //adds entry to journal
    string tempHeading;
    string tempDate;
    string tempEntry;
    askDate(tempDate);
    askHeading(tempHeading);
    askEntry(tempEntry);
    append(tempDate,tempHeading,tempEntry,size);
}
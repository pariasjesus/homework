/* 
 * File:   Menus.cpp
 * Author: Jesus Parias-Castillo
 * Created on June 8, 2019, 9:02 PM
 */

#ifndef JOURNAL_H
#define JOURNAL_H
#include <string>
#include <iostream>
#include "Bush.h"
#include "Tree.h"
#include "Cactus.h"

using namespace std;

//Page node
struct Page{
    int pageNumber;
    string entry;
    string date;
    string heading;
    Page* nextPage;
};

//journal class
class Journal {
private:
    int size;       //total pages
    Page* firstPage;    //head
public:
    //constructor
    Journal();
    
    //destructor
    ~Journal();
    
    //operations
    void append(string,string,string,int);
    void insert(string,string,string,int);
    void remove(int);
    void print();
    
    //getters
    int getSize();
    string getDate();
    string getHeading();
    string getEntry();
    
    //asks users for input
    void askDate(string&);
    void askHeading(string&);
    void askEntry(string&);
    
    //menus
    void myJournalMenu(char*,int,Plant**,Journal&);
    void addEntryMenu();
    void askRemoveMenu(char*,int);
};

#endif /* JOURNAL_H */


/* 
 * File:   Menus.cpp
 * Author: Jesus Parias-Castillo
 * Created on June 8, 2019, 9:02 PM
 */

#include "Menus.h"

void mainMenu(char* choice,int &numberOfPlants,Plant** plants,Journal &journal){
    const int opts=2;//sets max options
    cout<<"Gardeners Journal"<<endl;
    cout<<"[1] My Plants"<<endl;
    cout<<"[2] Garden Journal"<<endl;
    cout<<"[X] Exit App"<<endl;
    cout<<"For Help press H"<<endl;
    
    //user inputs choice
    cin>>choice;
    
    //checks validation for choice
    mainMenuValidation(choice,opts);
    
    //calls menus depending on choice
    if(choice[0]=='1'){//calls plants
        myGardenMenu(choice,numberOfPlants,plants);
        mainMenu(choice,numberOfPlants,plants,journal);
    }
    if(choice[0]=='2'){//calls journal
        journal.myJournalMenu(choice,numberOfPlants,plants,journal);
        mainMenu(choice,numberOfPlants,plants,journal);
    }
    if(choice[0]=='X'||choice[0]=='x'){//user exits
        cout<<"Exiting Please Wait..."<<endl;
    }
    if(choice[0]=='H'||choice[0]=='h'){//helps user
        helpMenu();
        mainMenu(choice,numberOfPlants,plants,journal);
    }
}
void myGardenMenu(char* choice,int &numberOfPlants,Plant** plants){
    //loops throught plants to show them
    for(int n=0;n<numberOfPlants;n++){
        cout<<"["<<n+1<<"]"<<" "<<plants[n]->getName()<<endl;
    }cout<<endl;
    cout<<"[A] Add/Delete Plants"<<endl;
    cout<<"[B] <-- Back"<<endl;
    cin>>choice;//user inputs choice
    
    //choice validation
    gardenMenuValidation(choice,numberOfPlants);
    if(atoi(choice)){//edit plant menu then comes back
        plants[atoi(choice)-1]->editPlantMenu(choice);
        myGardenMenu(choice,numberOfPlants,plants);
    }
    if(choice[0]=='A'||choice[0]=='a'){//edit garden  menu then comes back
        editGardenMenu(choice,numberOfPlants,plants);
        myGardenMenu(choice,numberOfPlants,plants);
    }
    if(choice[0]=='B'||choice[0]=='b'){//goes back to previous screen
        return;
    }
}
void mainMenuValidation(char* choice, int max){
    bool choiceValidation=false;//sets validation for main menu input
    if(choice[0]=='x'||choice[0]=='X'){
        choiceValidation=true;
    }
    if(choice[0]=='h'||choice[0]=='H'){
        choiceValidation=true;
    }
    if(atoi(choice)>0&&atoi(choice)<=max){
        choiceValidation=true;
    }
    //if fails then asks again for input
    if(choiceValidation==false){
        cout<<"Please enter a valid option: ";
        cin>>choice;
        mainMenuValidation(choice,max);
    }
}
void gardenMenuValidation(char* choice, int max){
    bool choiceValidation=false;//sets validation for main menu input
    if(choice[0]=='A'||choice[0]=='a'){
        choiceValidation=true;
    }
    if(choice[0]=='b'||choice[0]=='B'){
        choiceValidation=true;
    }
    if(atoi(choice)>0&&atoi(choice)<=max){
        choiceValidation=true;
    }
    //if fails then asks again for input
    if(choiceValidation==false){
        cout<<"Please enter a valid option: ";
        cin>>choice;
        gardenMenuValidation(choice,max);
    }
}
void editGardenValidation(char* choice){
    bool choiceValidation=false;//sets validation for main menu input
    if(choice[0]=='A'||choice[0]=='a'){
        choiceValidation=true;
    }
    if(choice[0]=='D'||choice[0]=='d'){
        choiceValidation=true;
    }
    if(choice[0]=='B'||choice[0]=='b'){
        choiceValidation=true;
    }
    //if fails then asks again for input
    if(choiceValidation==false){
        cout<<"Please enter a valid option: ";
        cin>>choice;
        editGardenValidation(choice);
    }
}
void helpMenu(){
    string choice;
    cout<<"To navigate press the key on the left colum that is within the brackets [_] "<<endl;
    cout<<"You can always go back at any moment to the previous screen"<<endl;
    cout<<"If you would like to use a text file to input information, please"<<endl;
    cout<<"     modify the text file templates located within the home folders"<<endl;
    cin>>choice;
}
void editGardenMenu(char* choice,int &numberOfPlants,Plant** plants){
    cout<<"[A] Add a Plant"<<endl;
    cout<<"[D] Get Rid of a Plant"<<endl;
    cout<<"[B] <--Back"<<endl;
    cin>>choice;//user inputs
    //validates voice
    editGardenValidation(choice);
    
    //picks proper menu
    if(choice[0]=='A'||choice[0]=='a'){
        //if garden is full then backs
        if(numberOfPlants>50){
            cout<<"The garden is full, please get rid of a plant if you wish to add"<<endl;
        }else{
            addPlantMenu(choice,numberOfPlants,plants);
        }
    }
    if(choice[0]=='D'||choice[0]=='d'){
        removePlantMenu(choice,numberOfPlants,plants);
    }
    if(choice[0]=='B'||choice[0]=='b'){
        myGardenMenu(choice,numberOfPlants,plants);
    }
}
void addPlantMenu(char* choice,int &numberOfPlants,Plant** plants){
    cout<<"What kind of plant is it?\n"
            <<"[0]Plant\n"
            <<"[1]Cactus\n"
            <<"[2]Tree\n"
            <<"[3]Bush\n";
    try{//try code
        const int max=3;//sets max
        bool num=true;//validation for number validation
        const int maxChars = 50;
        char* temp= new char[maxChars];
        cin>>choice;//user inputs
        //checks that no characters are present
        for(int i=0;choice[i]!='\0';i++){
            if(!isdigit(choice[i])){
                num=false;//if not a number then fails
            }
        }
        //if fails validation throws error
        if(num==false||atoi(choice)<0||atoi(choice)>max){
            string error;
            num==false?error="Error: character found; please enter a number \n":error="Please Enter a valid option\n";
            throw error;
        }
        //else add an extra plant
        numberOfPlants++;
        //deletes last value
        delete plants[numberOfPlants-1];
        //sets last value to new type of plant
        if(atoi(choice)==0){
            plants[numberOfPlants-1]=new Plant;
            plants[numberOfPlants-1]->editPlantMenu();
        }
        else if(atoi(choice)==1){
           plants[numberOfPlants-1]=new Cactus;
           plants[numberOfPlants-1]->editPlantMenu();
        }
        else if(atoi(choice)==2){
           plants[numberOfPlants-1]=new Tree;
           plants[numberOfPlants-1]->editPlantMenu();
        }
        else if(atoi(choice)==3){
           plants[numberOfPlants-1]=new Bush;
           plants[numberOfPlants-1]->editPlantMenu();
        }
    }
    //if catches error then calls self
    catch(string error){
        cout<<error;
        addPlantMenu(choice,numberOfPlants,plants);
    }
}
void removePlantMenu(char* choice,int &numberOfPlants,Plant** plants){
    int temp;
    //loops to show plants
    for(int n=0;n<numberOfPlants;n++){
        cout<<"["<<n+1<<"]"<<" "<<plants[n]->getName()<<endl;
    }cout<<endl;
    cout<<"Which plant would you like to remove?"<<endl;
    cin>>choice;//user inputs
    temp=(atoi(choice))-1;//sets temp to choice
    cout<<"Are you sure you want to remove "<<plants[temp]->getName()<<"?"<<endl;
    cin>>choice;//asks user for confirmation
    //if confirmation fail then does not remove
    if(choice[0]=='n'||choice[0]=='N'){
        cout<<"Plant will not be removed"<<endl;
    }else{
        //does give option to remove if not plants are available
        if(numberOfPlants==0){
            cout<< "No Plants"<<endl;
        }
        else{
            numberOfPlants--;
            cout<<"Plant has been removed"<<endl;
            for(int i=temp;i<numberOfPlants;i++){
                plants[i]=plants[i+1];
            }
        }
    }
    //goes back
    myGardenMenu(choice,numberOfPlants,plants);
}

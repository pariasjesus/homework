/* 
 * File:   Menus.cpp
 * Author: Jesus Parias-Castillo
 * Created on June 8, 2019, 9:02 PM
 */

#ifndef MENUS_H
#define MENUS_H

#include "Bush.h"
#include "Cactus.h"
#include "Tree.h"
#include "Journal.h"
#include <cstdlib>
#include <iomanip>
#include <fstream>

using namespace std;

void mainMenu(char*,int&,Plant**,Journal&);     //main menu
void mainMenuValidation(char*,int);             //validation for the main menu
void gardenMenuValidation(char*,int);           //validation for the garden menu
void editGardenValidation(char*);               //validation for the edit garden manu
void myGardenMenu(char*,int&,Plant**);          //garden menu
void helpMenu();                                //help when user asks how to use
void editGardenMenu(char*,int&,Plant**);        //edit garden menu
void addPlantMenu(char*,int&,Plant**);          //add plant menu
void removePlantMenu(char*,int&,Plant**);       //remove plant menu
void editPlantMenu(char*,Cactus*);              //edit plant menu

#endif /* MENUS_H */


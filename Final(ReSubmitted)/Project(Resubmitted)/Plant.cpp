/* 
 * File:   Menus.cpp
 * Author: Jesus Parias-Castillo
 * Created on June 8, 2019, 9:02 PM
 */
#include "Plant.h"

string getLifeStageString(int stage){
    //switches enum to return string
    switch(stage){
        case seed:
            return "Seed";
            break;
        case sapling:
            return "Sapling";
            break;
        case mature:
            return "Mature";
            break;
        case harvest:
            return "Harvest";
            break;
        default:
            return "Error: LifeStageUnknown";
            break;
    }
}
string getTypeString(int type){
    //switches enum to return string
   switch(type){
        case plant:
            return "Plant";
            break;
        case cactus:
            return "Cactus";
            break;
        case tree:
            return "Tree";
            break;
        case bush:
            return "Bush";
            break;
        default:
            return "Error: no type";
            break;
    } 
}

Plant::Plant(){
    name="";
    height=0;
    age=0;
    lifeStage=0;
    type=plant;
    description = "";
}
Plant::Plant(string name){
    this->name=name;
    height=0;
    age=0;
    lifeStage=0;
    type=plant;
    description = "";
}
Plant::Plant(string name, int type){
    this->name=name;
    height=0;
    age=0;
    lifeStage=0;
    this->type=type;
    description = "";
}
Plant::Plant(const Plant &plant){
    this->age=plant.age;
    this->height=plant.height;
    this->lifeStage=plant.lifeStage;
    this->name=plant.name;
    this->type=plant.type;
    this->description=plant.description;
}

Plant::~Plant() {
}

const Plant &Plant::operator=(const Plant &plant){
    //if same address then just goes back
    if(this==&plant){
       return *this; 
    }
    this->age=plant.age;
    this->height=plant.height;
    this->lifeStage=plant.lifeStage;
    this->name=plant.name;
    this->type=plant.type;
    this->description=plant.description;
    return *this;
}
void Plant::output(){
    cout<<this;
}
ostream &operator <<(ostream &out,const Plant &plant){
    //returns plant information
    out<<"Name: "<<plant.name<<"\n"
            <<"Height: "<<plant.height<<"\n"
            <<"Stage: "<<getLifeStageString(plant.lifeStage)<<"\n"
            <<"Type: "<<getTypeString(plant.type)<<"\n"
            <<"Age: "<<plant.age<<"\n"
            <<"Description: "<<plant.description<<"\n";
    return out;
}
ostream &operator <<(ostream &out,const Plant* plant){
    //returns plant information
    out<<"Name: "<<plant->name<<"\n"
            <<"Height: "<<plant->height<<"\n"
            <<"Stage: "<<getLifeStageString(plant->lifeStage)<<"\n"
            <<"Type: "<<getTypeString(plant->type)<<"\n"
            <<"Age: "<<plant->age<<"\n"
            <<"Description: "<<plant-> description<<"\n";
    return out;
}
void Plant::setName(string name){
    this->name=name;
}
void Plant::setAge(int age){
    this->age=age;
}
void Plant::setLifeStage(int lifeStage){
    this->lifeStage=lifeStage;
}
void Plant::setType(int type){
    this->type=type;
}
void Plant::setDescription(string description){
    this->description=description;
}
int Plant::getAge(){
    return age;
}
int Plant::getLifeStage(){
    return lifeStage;
}
int Plant::getType(){
    return type;
}
string Plant::getDescription(){
    return description;
}
string Plant::getName(){
    return name;
}

void Plant::askName(){
    cout<<"What would you like to name the plant?: ";
    //ignores older inputs and inputs new stuff
    cin.ignore();
    getline(cin,name);
}
void Plant::askHeight(){
    try{//try code
        bool num=true;//bool to make sure its a number being inputted
        const int maxChars = 50;
        char* temp= new char[maxChars];//user input variable
        cout<<"How tall is the plant in centimeters?: ";
        cin>>temp;//user inputs height
        //checks all for numbers
        for(int i=0;temp[i]!='\0';i++){
            if(!isdigit(temp[i])){
                num=false;//if not a number then num is false
            }
        }
        //if num is false then throw error
        if(num==false){
            delete[] temp;
            string error="Error: character found; please enter a number \n";
            throw error;
        }
        height=atoi(temp);//sets height
        delete[] temp;//deallocates temp
    }
    //if catch then call self
    catch(string error){
        cout<<error;
        askHeight();
    }
}
void Plant::askAge(){
    try{//try code
        bool num=true;//bool to make sure its a number being inputted
        const int maxChars = 50;
        char* temp= new char[maxChars];//user input variable
        cout<<"How old is the plant in days?: ";
        cin>>temp;//user inputs age
        //checks all for numbers
        for(int i=0;temp[i]!='\0';i++){
            if(!isdigit(temp[i])){
                num=false;//if not a number then num is false
            }
        }
        //if num is false then throw error
        if(num==false){
            delete[] temp;
            string error="Error: character found; please enter a number \n";
            throw error;
        }
        age=atoi(temp);//sets age
        delete[] temp;//deallocates temp
    }
    //if catch then call self
    catch(string error){
        cout<<error;
        askAge();
    }
}
void Plant::askLifeStage(){
    try{//try code
        int maxOps=lifeStageMax;//sets maximum amount of options
        const int maxChars = 50;
        char* temp= new char[maxChars];//user input variable
        cout<<"What stage of its life is the plant in?"<<endl;
        cout<<"[0]Seed\n"
                <<"[1]Sapling\n"
                <<"[2]Mature\n"
                <<"[3]Harvest\n";
        cin>>temp;//user inputs
        //makes sure input is within scope else throw error
        if(atoi(temp)>maxOps||atoi(temp)<0){
            delete[] temp;
            string error="Please Enter a valid option\n";
            throw error;
        }
        lifeStage=atoi(temp);//sets life stage
        delete[] temp;//deallocates temp
    }
    //if catch then call self
    catch(string error){
        cout<<error;
        askLifeStage();
    }
}
void Plant::askType(){
    try{//try code
        int maxOps=typeMax;//sets maximum amount of options
        const int maxChars = 50;
        char* temp= new char[maxChars];//user input variable
        cout<<"What type of plant is it?"<<endl;
        cout<<"[0]Unknown\n"
                <<"[1]Vegetable\n"
                <<"[2]Fruit\n"
                <<"[3]Nut\n"
                <<"[4]Bush\n";
        cin>>temp;//user inputs
        //makes sure input is within scope else throw error
        if(atoi(temp)>maxOps||atoi(temp)<0){
            delete[] temp;
            string error="Please Enter a valid option\n";
            throw error;
        }
        type=atoi(temp);//sets type
        delete[] temp;//deallocates temp
    }
    //if catch then call self
    catch(string error){
        cout<<error;
        askType();
    }
}
void Plant::askDescription(){
    //asks for description then sets it
    cout<<"Please Write a description of the plant"<<endl;
    cin.ignore();
    getline(cin,description);
}
void Plant::editPlantMenu(char* choice){
    try{//try code
        cout<<this;
        int maxOps =6;//sets max
        cout<<"What would you like to do?"<<endl;
        cout<<"[0]Change Name\n"
                <<"[1]Change Height\n"
                <<"[2]Change Age\n"
                <<"[3]Change LifeStage\n"
                <<"[4]Change Type\n"
                <<"[5]Change Description\n"
                <<"[6]Change Everything\n";
        cin>>choice;//user inputs
        //checks if user is within scope
        if(atoi(choice)>maxOps||atoi(choice)<0){
            string error="Please Enter a valid option\n";
            throw error;
        }
        //choose case based on user input
        switch(atoi(choice)){
            case 0:
                askName();
                break;
            case 1:
                askHeight();
                break;
            case 2:
                askAge();
                break;
            case 3:
                askLifeStage();
                break;
            case 4:
                askType();
                break;
            case 5:
                askDescription();
                break;
            case 6:
                askName();
                askHeight();
                askAge();
                askLifeStage();
                askType();
                askDescription();
                break;
            default:
                cout<<"Error: option does not exist"<<endl;
                break;
        }
    }
    //if catches error then calls self
    catch(string error){
        cout<<error;
        editPlantMenu(choice);
    }
}
void Plant::editPlantMenu(){
    //calls all other ask functions to ask everything
    askName();
    askHeight();
    askAge();
    askLifeStage();
    askType();
    askDescription();
}

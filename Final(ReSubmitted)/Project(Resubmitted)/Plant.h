/* 
 * File:   Menus.cpp
 * Author: Jesus Parias-Castillo
 * Created on June 8, 2019, 9:02 PM
 */
#ifndef PLANT_H
#define PLANT_H

#include <string>
#include <iostream>

using namespace std;

enum PlantLifeStages{seed,sapling,mature,harvest,lifeStageMax};//make user input easier
enum PlantType{plant,cactus,tree,bush,typeMax};//make user input easier

string getLifeStageString(int);//return string for enum
string getTypeString(int);//returns string for enum

//Parent plant class
class Plant {
public:
    //constructors
    Plant();
    Plant(string);
    Plant(string,int);
    Plant(const Plant &);
    //assignment operator
    const Plant &operator=(const Plant &);
    //destructor
    virtual ~Plant();
    
    //outputs
    virtual void output();
    friend ostream& operator<<(ostream&,const Plant &);
    friend ostream& operator<<(ostream&,const Plant*);
    
    //setters
    void setName(string);
    void setAge(int);
    void setLifeStage(int);
    void setType(int);
    void setDescription(string);
    
    //getters
    int getAge();
    int getLifeStage();
    int getType();
    string getName();
    string getDescription();
    
    //ask user for values
    void askName();
    void askHeight();
    void askAge();
    void askLifeStage();
    void askType();
    void askDescription();
    
    //menus
    virtual void editPlantMenu(char*);
    virtual void editPlantMenu();
protected:
    string name;    //plant name
    int height;  //in centimeters
    int age;     //in days
    int lifeStage;   //enum PlantLifeStages
    int type;    //unknown,vegetable,fruit,nut
    string description; //plant description
};

#endif /* PLANT_H */


/* 
 * File:   Menus.cpp
 * Author: Jesus Parias-Castillo
 * Created on June 8, 2019, 9:02 PM
 */

#include "Tree.h"


Tree::Tree()
    :Plant(){
    years=0;
    meters=0;
    area=0;
    type=tree;
}
Tree::Tree(string name)
    :Plant(name){
    years=0;
    meters=0;
    area=0;
    type=tree;
}
Tree::Tree(string name, int type)
    :Plant(name,type){
    years=0;
    meters=0;
    area=0;
}
Tree::Tree(const Tree &tree)
    :Plant(){
    this->years=tree.years;
    this->meters=tree.meters;
    this->area=tree.area;
}
const Tree &Tree::operator=(const Tree &tree){
    if(this==&tree){
       return *this; 
    }
    Plant::operator =(tree);
    this->area=tree.area;
    this->meters=tree.meters;
    this->years=tree.years;
    return *this;
}
Tree::~Tree(){}
void Tree::setYears(int years){
    this->years=years;
}
void Tree::setMeters(int meters){
    this->meters=meters;
}
void Tree::setArea(int areas){
    this->area=areas;
}
int Tree::getYears(){
    return years;
}
int Tree::getMeters(){
    return meters;
}
int Tree::getArea(){
    return area;
}
void Tree::output(){
    cout<<this;
}
ostream &operator <<(ostream &out,const Tree &tree){
    out<<"Name: "<<tree.name<<"\n"
            <<"Height: "<<tree.height<<"\n"
            <<"Stage: "<<getLifeStageString(tree.lifeStage)<<"\n"
            <<"Type: "<<getTypeString(tree.type)<<"\n"
            <<"Age in days: "<<tree.age<<"\n"
            <<"Age in years: "<<tree.years<<"\n"
            <<"Description: "<<tree.description<<"\n"
            <<"Area in Meters: "<<tree.area<<" m^2"<<"\n"
            <<"Height in Meters: "<<tree.meters<<" meters"<<"\n";
    return out;
}
ostream &operator <<(ostream &out,const Tree* tree){
    out<<"Name: "<<tree->name<<"\n"
            <<"Height: "<<tree->height<<"\n"
            <<"Stage: "<<getLifeStageString(tree->lifeStage)<<"\n"
            <<"Type: "<<getTypeString(tree->type)<<"\n"
            <<"Age in days: "<<tree->age<<"\n"
            <<"Age in years: "<<tree->years<<"\n"
            <<"Description: "<<tree->description<<"\n"
            <<"Area in Meters: "<<tree->area<<" m^2"<<"\n"
            <<"Height in Meters: "<<tree->meters<<" meters"<<"\n";
    return out;
}
void Tree::editPlantMenu(char* choice){
    try{
        cout<<this;
        int maxOps =9;
        cout<<"What would you like to do?"<<endl;
        cout<<"[0]Change Name\n"
                <<"[1]Change Height\n"
                <<"[2]Change Age\n"
                <<"[3]Change LifeStage\n"
                <<"[4]Change Type\n"
                <<"[5]Change Description\n"
                <<"[6]Change Area Taken Up"
                <<"[7]Change Age in Years"
                <<"[8]Change Height in Meters"
                <<"[9]Change Everything\n";
        cin>>choice;
        if(atoi(choice)>maxOps||atoi(choice)<0){
            string error="Please Enter a valid option\n";
            throw error;
        }
        switch(atoi(choice)){
            case 0:
                askName();
                break;
            case 1:
                askHeight();
                break;
            case 2:
                askAge();
                break;
            case 3:
                askLifeStage();
                break;
            case 4:
                askType();
                break;
            case 5:
                askDescription();
                break;
            case 6:
                askArea();
                break;
            case 7:
                askYears();
                break;
            case 8:
                askMeters();
                break;
            case 9:
                askName();
                askHeight();
                askAge();
                askLifeStage();
                askType();
                askDescription();
                askArea();
                askYears();
                askMeters();
                break;
            default:
                cout<<"Error: option does not exist"<<endl;
                break;
        }
    }
    catch(string error){
        cout<<error;
        editPlantMenu(choice);
    }
}
void Tree::askMeters(){
    try{
        int maxOps=typeMax;
        bool num=true;
        const int maxChars = 50;
        char* temp= new char[maxChars];
        cout<<"How tall is the Tree in meters?: ";
        cin>>temp;
        
        for(int i=0;temp[i]!='\0';i++){
            if(!isdigit(temp[i])){
                num=false;
            }
        }
        if(num==false||atoi(temp)<0){
            delete[] temp;
            string error=(num==false?"Error: character found; please enter a number \n":"Please Enter a valid option\n");
            throw error;
        }
        meters=atoi(temp);
        delete[] temp;
    }
    catch(string error){
        cout<<error;
        askMeters();
    }
}
void Tree::askArea(){
    try{
        int maxOps=typeMax;
        bool num=true;
        const int maxChars = 50;
        char* temp= new char[maxChars];
        cout<<"How much area does the tree take up in meters?: ";
        cin>>temp;
        
        for(int i=0;temp[i]!='\0';i++){
            if(!isdigit(temp[i])){
                num=false;
            }
        }
        if(num==false||atoi(temp)<0){
            delete[] temp;
            string error=(num==false?"Error: character found; please enter a number \n":"Please Enter a valid option\n");
            throw error;
        }
        area=atoi(temp);
        delete[] temp;
    }
    catch(string error){
        cout<<error;
        askArea();
    }
}
void Tree::askYears(){
    try{
        int maxOps=typeMax;
        bool num=true;
        const int maxChars = 50;
        char* temp= new char[maxChars];
        cout<<"How old is the tree in years?: ";
        cin>>temp;
        
        for(int i=0;temp[i]!='\0';i++){
            if(!isdigit(temp[i])){
                num=false;
            }
        }
        if(num==false||atoi(temp)<0){
            delete[] temp;
            string error;
            num==false?error="Error: character found; please enter a number \n":error="Please Enter a valid option\n";
            throw error;
            throw error;
        }
        years=atoi(temp);
        delete[] temp;
    }
    catch(string error){
        cout<<error;
        askYears();
    }
}
void Tree::editPlantMenu(){
    askName();
    askHeight();
    askAge();
    askLifeStage();
    askType();
    askDescription();
    askArea();
    askMeters();
    askYears();
}
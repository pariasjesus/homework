/* 
 * File:   Menus.cpp
 * Author: Jesus Parias-Castillo
 * Created on June 8, 2019, 9:02 PM
 */

#ifndef TREE_H
#define TREE_H
#include "Plant.h"

//plant is a tree
class Tree:public Plant{
    //trees live longer and grow taller
private:
    int years;   //age in years
    int meters; //height in meters
    int area;    //area in meters square tree takes up
public:
    //constructors
    Tree();
    Tree(string);
    Tree(string,int);
    Tree(const Tree &);
    
    //assignment operator
    const Tree &operator=(const Tree &);
    
    //destructor
    ~Tree();
    
    //outputs
    void output();
    friend ostream& operator<<(ostream&,const Tree &);
    friend ostream& operator<<(ostream&,const Tree*);
    
    //setters
    void setYears(int);
    void setMeters(int);
    void setArea(int);
    
    //getters
    int getYears();
    int getMeters();
    int getArea();
    
    //asks user for inputs
    void askYears();
    void askMeters();
    void askArea();
    
    //menus
    void editPlantMenu(char*);
    void editPlantMenu();
};

#endif /* TREE_H */


/* 
 * File:   Menus.cpp
 * Author: Jesus Parias-Castillo
 * Created on June 8, 2019, 9:02 PM
 */
#include "Bush.h"
#include "Cactus.h"
#include "Tree.h"
#include "Journal.h"
#include "Menus.h"
#include <cstdlib>
int main(){
    const int maxPlants=50;   //max of 50 plants in garden
    int numOfPlants=0;  //number of plants in garden  
    char* choice = new char[25];        //choice will be main user input during program
    Plant** plants=new Plant*[maxPlants];       //pointers of plants to store derived plants
    
    Journal journal;        //journal for garden
    
    mainMenu(choice,numOfPlants,plants,journal);    //main menu for program
    
    //deallocate a space
    for(int i=0;i<maxPlants;i++){
        delete[] plants[i]; 
    }delete[] plants;
    plants=nullptr;
    delete[] choice;
    choice=nullptr;
    return 0;
}
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Bush.h
 * Author: User
 *
 * Created on June 6, 2019, 10:21 PM
 */

#ifndef BUSH_H
#define BUSH_H
#include "Plant.h"

class Bush: public Plant{
    //bushes grow fatter and not taller
private:
    int area;    //area in square feet bush takes up
public:
    Bush();
    Bush(string);
    Bush(string,int);
    Bush(const Bush &);
    const Bush &operator=(const Bush &);
    ~Bush();
    friend ostream& operator<<(ostream&,const Bush &);
    friend ostream& operator<<(ostream&,const Bush*);
    void setArea(int);
    int getArea();
    
    void askArea();
    
    void editPlantMenu(char*);
    void editPlantMenu();
};

#endif /* BUSH_H */


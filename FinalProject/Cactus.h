/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Cactus.h
 * Author: User
 *
 * Created on June 6, 2019, 10:21 PM
 */

#ifndef CACTUS_H
#define CACTUS_H
#include "Plant.h"

class Cactus: public Plant{
    //Cactus hold more water and have needles
private:
    int needleLength;    //needle length in centimeters
    int waterDays;   //days the cactus can go without water
public:
    Cactus();
    Cactus(string);
    Cactus(string,int);
    Cactus(const Cactus &);
    const Cactus &operator=(const Cactus &);
    ~Cactus();
    friend ostream& operator<<(ostream&,const Cactus &);
    friend ostream& operator<<(ostream&,const Cactus*);
    //setters
    void setNeedleLength(int);
    void setWaterDays(int);
    //getters
    int getNeedleLength();
    int getWaterDays();
    
    void askNeedleLength();
    void askWaterDays();
    
    void editPlantMenu(char*);
    void editPlantMenu();
};

#endif /* CACTUS_H */


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Journal.cpp
 * Author: User
 * 
 * Created on June 7, 2019, 4:27 AM
 */

#include "Journal.h"

Journal::Journal(){
    size=0;
    name="";
}
Journal::Journal(string name){
    size=0;
    this->name=name;
}
Journal::Journal(string name,int size){
    this->size=size;
    this->name=name;
    pages=new Page[this->size];
}
Journal::Journal(const Journal& journal){
    this->size=journal.size;
    this->name=journal.name;
    delete[] pages;
    pages=new Page[this->size];
    for(int i=0;i<journal.size;i++){
        pages[i]=journal.pages[i];
    }
}
const Journal &Journal::operator=(const Journal &journal){
    if(this==&journal){
        return *this;
    }
    this->size=journal.size;
    this->name=journal.name;
    delete[] pages;
    pages=new Page[this->size];
    for(int i=0;i<journal.size;i++){
        pages[i]=journal.pages[i];
    }
}
Journal::~Journal(){
    delete[] pages;
}
ostream& operator<<(ostream& out,const Journal &journal){
    out<<"Journal: "<<journal.name<<endl;
    for(int i=0;i<journal.size;i++){
        out<<"Page "<<i+1<<endl;
        out<<journal.pages[i].heading<<endl;
        out<<journal.pages[i].date<<endl;
        out<<journal.pages[i].entry<<endl;
    }
    return out;
}
ostream& operator<<(ostream& out,const Journal* journal){
    out<<"Journal: "<<journal->name<<endl;
    for(int i=0;i<journal->size;i++){
        out<<"Page "<<i+1<<endl;
        out<<journal->pages[i].heading<<endl;
        out<<journal->pages[i].date<<endl;
        out<<journal->pages[i].entry<<endl;
    }
    return out;
}
    
void Journal::setSize(int size){
    this->size=size;
}
void Journal::setName(string name){
    this->name=name;
}
    
int Journal::getSize(){
    return size;
}
string Journal::getName(){
    return name;
}


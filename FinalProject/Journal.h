/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Journal.h
 * Author: User
 *
 * Created on June 7, 2019, 4:27 AM
 */

#ifndef JOURNAL_H
#define JOURNAL_H
#include <string>
#include <iostream>

using namespace std;

struct Page{
    string date;
    string heading;
    string entry;
};

class Journal {
private:
    int size;
    string name;
    Page* pages;
public:
    Journal();
    Journal(string);
    Journal(string,int);
    Journal(const Journal& orig);
    const Journal &operator=(const Journal&);
    virtual ~Journal();
    
    friend ostream& operator<<(ostream&,const Journal &);
    friend ostream& operator<<(ostream&,const Journal*);
    
    void setSize(int);
    void setName(string);
    
    int getSize();
    string getName();
};

#endif /* JOURNAL_H */


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Menus.cpp
 * Author: User
 * 
 * Created on June 8, 2019, 9:02 PM
 */

#include "Menus.h"

void mainMenu(char* choice,int &numberOfPlants,Plant** plants){
    const int opts=2;
    cout<<"Gardeners Journal"<<endl;
    cout<<"[1] My Plants"<<endl;
    cout<<"[2] Garden Journal"<<endl;
    cout<<"[X] Exit App"<<endl;
    cout<<"For Help press H"<<endl;
    
    cin>>choice;
    mainMenuValidation(choice,opts);
    
    if(choice[0]=='1'){
        myGardenMenu(choice,numberOfPlants,plants);
    }
    if(choice[0]=='2'){
        
    }
    if(choice[0]=='X'||choice[0]=='x'){
        cout<<"Bye"<<endl;
    }
    if(choice[0]=='H'||choice[0]=='h'){
        helpMenu(choice,numberOfPlants,plants);
    }
}

void myGardenMenu(char* choice,int &numberOfPlants,Plant** plants){
    for(int n=0;n<numberOfPlants;n++){
        cout<<"["<<n+1<<"]"<<" "<<plants[n]->getName()<<endl;
    }cout<<endl;
    cout<<"[A] Add/Delete Plants"<<endl;
    cout<<"[B] <-- Back"<<endl;
    cin>>choice;
    gardenMenuValidation(choice,numberOfPlants);
    if(atoi(choice)){
        plants[atoi(choice)-1]->editPlantMenu(choice);
        mainMenu(choice,numberOfPlants,plants);
    }
    if(choice[0]=='A'||choice[0]=='a'){
        editGardenMenu(choice,numberOfPlants,plants);
        mainMenu(choice,numberOfPlants,plants);
    }
    if(choice[0]=='B'||choice[0]=='b'){
        mainMenu(choice,numberOfPlants,plants);
        mainMenu(choice,numberOfPlants,plants);
    }
}
void mainMenuValidation(char* choice, int max){
    bool choiceValidation;
    if(choice[0]=='x'||choice[0]=='X'){
        choiceValidation=true;
    }
    if(choice[0]=='h'||choice[0]=='H'){
        choiceValidation=true;
    }
    if(atoi(choice)>0&&atoi(choice)<=max){
        choiceValidation=true;
    }
    if(choiceValidation==false){
        cout<<"Please enter a valid option: ";
        cin>>choice;
        mainMenuValidation(choice,max);
    }
}
void gardenMenuValidation(char* choice, int max){
    bool choiceValidation;
    if(choice[0]=='A'||choice[0]=='a'){
        choiceValidation=true;
    }
    if(choice[0]=='b'||choice[0]=='B'){
        choiceValidation=true;
    }
    if(atoi(choice)>0&&atoi(choice)<=max){
        choiceValidation=true;
    }
    if(choiceValidation==false){
        cout<<"Please enter a valid option: ";
        cin>>choice;
        gardenMenuValidation(choice,max);
    }
}
void editGardenValidation(char* choice){
    bool choiceValidation;
    if(choice[0]=='A'||choice[0]=='a'){
        choiceValidation=true;
    }
    if(choice[0]=='D'||choice[0]=='d'){
        choiceValidation=true;
    }
    if(choice[0]=='B'||choice[0]=='b'){
        choiceValidation=true;
    }
    if(choiceValidation==false){
        cout<<"Please enter a valid option: ";
        cin>>choice;
        editGardenValidation(choice);
    }
}
void helpMenu(char* choice,int &numberOfPlants,Plant** plants){
    cout<<"To navigate press the key on the left colum that is within the brackets [_] "<<endl;
    cout<<"You can always go back at any moment to the previous screen"<<endl;
    cout<<"If you would like to use a text file to input information, please"<<endl;
    cout<<"     modify the text file templates located within the home folders"<<endl;
    cout<<"[B] <--Back"<<endl;
    cin>>choice;
    mainMenu(choice,numberOfPlants,plants);
}


void editGardenMenu(char* choice,int &numberOfPlants,Plant** plants){
    cout<<"[A] Add a Plant"<<endl;
    cout<<"[D] Get Rid of a Plant"<<endl;
    cout<<"[B] <--Back"<<endl;
    cin>>choice;
    editGardenValidation(choice);
    if(choice[0]=='A'||choice[0]=='a'){
        if(numberOfPlants>50){
            cout<<"The garden is full, please get rid of a plant if you wish to add"<<endl;
        }else{
            addPlantMenu(choice,numberOfPlants,plants);
        }
    }
    if(choice[0]=='D'||choice[0]=='d'){
        removePlantMenu(choice,numberOfPlants,plants);
    }
    if(choice[0]=='B'||choice[0]=='b'){
        myGardenMenu(choice,numberOfPlants,plants);
    }
}

void addPlantMenu(char* choice,int &numberOfPlants,Plant** plants){
    cout<<"What kind of plant is it?\n"
            <<"[0]Plant\n"
            <<"[1]Cactus\n"
            <<"[2]Tree\n"
            <<"[3]Bush\n";
    try{
        const int max=3;
        bool num=true;
        const int maxChars = 50;
        char* temp= new char[maxChars];
        cin>>choice;
        
        for(int i=0;choice[i]!='\0';i++){
            if(!isdigit(choice[i])){
                num=false;
            }
        }
        if(num==false||atoi(choice)<0||atoi(choice)>max){
            string error;
            num==false?error="Error: character found; please enter a number \n":error="Please Enter a valid option\n";
            throw error;
        }
        numberOfPlants++;
        if(atoi(choice)==0){
           plants[numberOfPlants-1]=new Plant;
           plants[numberOfPlants-1]->editPlantMenu();
        }
        else if(atoi(choice)==1){
           plants[numberOfPlants-1]=new Cactus;
           plants[numberOfPlants-1]->editPlantMenu();
        }
        else if(atoi(choice)==2){
           plants[numberOfPlants-1]=new Tree;
           plants[numberOfPlants-1]->editPlantMenu();
        }
        else if(atoi(choice)==3){
           plants[numberOfPlants-1]=new Bush;
           plants[numberOfPlants-1]->editPlantMenu();
        }
    }
    catch(string error){
        cout<<error;
        addPlantMenu(choice,numberOfPlants,plants);
    }
}
void removePlantMenu(char* choice,int &numberOfPlants,Plant** plants){
    int temp;
    for(int n=0;n<numberOfPlants;n++){
        cout<<"["<<n+1<<"]"<<" "<<plants[n]->getName()<<endl;
    }cout<<endl;
    cout<<"Which plant would you like to remove?"<<endl;
    cin>>choice;
    temp=(atoi(choice))-1;
    cout<<"Are you sure you want to remove "<<plants[temp]->getName()<<"?"<<endl;
    cin>>choice;
    if(choice[0]=='n'||choice[0]=='N'){
        cout<<"Plant will not be removed"<<endl;
    }else{
        if(numberOfPlants==0){
            cout<< "No Plants"<<endl;
        }
        else{
            numberOfPlants--;
            cout<<"Plant has been removed"<<endl;
        }
    }
    myGardenMenu(choice,numberOfPlants,plants);
}

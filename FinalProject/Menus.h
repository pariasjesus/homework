/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Menus.h
 * Author: User
 *
 * Created on June 8, 2019, 9:02 PM
 */

#ifndef MENUS_H
#define MENUS_H

#include "Bush.h"
#include "Cactus.h"
#include "Tree.h"
#include "Journal.h"
#include <cstdlib>
#include <iomanip>
#include <fstream>

using namespace std;

void mainMenu(char*,int&,Plant**);
void mainMenuValidation(char*,int);
void gardenMenuValidation(char*,int);
void editGardenValidation(char*);
void myGardenMenu(char*,int&,Plant**);
void helpMenu(char*,int&,Plant**);
void editGardenMenu(char*,int&,Plant**);
void addPlantMenu(char*,int&,Plant**);
void removePlantMenu(char*,int&,Plant**);

void editPlantMenu(char*,Cactus*);


#endif /* MENUS_H */


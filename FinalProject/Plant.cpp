/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Plant.cpp
 * Author: User
 * 
 * Created on June 6, 2019, 9:52 PM
 */

#include "Plant.h"

string getLifeStageString(int stage){
    switch(stage){
        case seed:
            return "Seed";
            break;
        case sapling:
            return "Sapling";
            break;
        case mature:
            return "Mature";
            break;
        case harvest:
            return "Harvest";
            break;
        default:
            return "Error: LifeStageUnknown";
            break;
    }
}
string getTypeString(int type){
   switch(type){
        case unknown:
            return "Unknown";
            break;
        case fruit:
            return "Fruit";
            break;
        case vegetable:
            return "Vegetable";
            break;
        case nut:
            return "Nut";
            break;
        case bush:
            return "Bush";
            break;
        default:
            return "Error: LifeStageUnknown";
            break;
    } 
}

Plant::Plant(){
    name="";
    height=0;
    age=0;
    lifeStage=0;
    type=unknown;
    description = "";
}
Plant::Plant(string name){
    this->name=name;
    height=0;
    age=0;
    lifeStage=0;
    type=unknown;
    description = "";
}
Plant::Plant(string name, int type){
    this->name=name;
    height=0;
    age=0;
    lifeStage=0;
    this->type=type;
    description = "";
}
Plant::Plant(const Plant &plant){
    this->age=plant.age;
    this->height=plant.height;
    this->lifeStage=plant.lifeStage;
    this->name=plant.name;
    this->type=plant.type;
    this->description=plant.description;
}

Plant::~Plant() {
}

const Plant &Plant::operator=(const Plant &plant){
    if(this==&plant){
       return *this; 
    }
    this->age=plant.age;
    this->height=plant.height;
    this->lifeStage=plant.lifeStage;
    this->name=plant.name;
    this->type=plant.type;
    this->description=plant.description;
    return *this;
}
ostream &operator <<(ostream &out,const Plant &plant){
    out<<"Name: "<<plant.name<<"\n"
            <<"Height: "<<plant.height<<"\n"
            <<"Stage: "<<getLifeStageString(plant.lifeStage)<<"\n"
            <<"Type: "<<getTypeString(plant.type)<<"\n"
            <<"Age: "<<plant.age<<"\n"
            <<"Description: "<<plant.description<<"\n";
    return out;
}
ostream &operator <<(ostream &out,const Plant* plant){
    out<<"Name: "<<plant->name<<"\n"
            <<"Height: "<<plant->height<<"\n"
            <<"Stage: "<<getLifeStageString(plant->lifeStage)<<"\n"
            <<"Type: "<<getTypeString(plant->type)<<"\n"
            <<"Age: "<<plant->age<<"\n"
            <<"Description: "<<plant-> description<<"\n";
    return out;
}
void Plant::setName(string name){
    this->name=name;
}
void Plant::setAge(int age){
    this->age=age;
}
void Plant::setLifeStage(int lifeStage){
    this->lifeStage=lifeStage;
}
void Plant::setType(int type){
    this->type=type;
}
void Plant::setDescription(string description){
    this->description=description;
}
int Plant::getAge(){
    return age;
}
int Plant::getLifeStage(){
    return lifeStage;
}
int Plant::getType(){
    return type;
}
string Plant::getDescription(){
    return description;
}
string Plant::getName(){
    return name;
}

void Plant::askName(){
    cout<<"What would you like to name the plant?: ";
    cin.ignore();
    getline(cin,name);
}
void Plant::askHeight(){
    try{
        bool num=true;
        const int maxChars = 50;
        char* temp= new char[maxChars];
        cout<<"How tall is the plant in centimeters?: ";
        cin>>temp;
        
        for(int i=0;temp[i]!='\0';i++){
            if(!isdigit(temp[i])){
                num=false;
            }
        }
        if(num==false){
            delete[] temp;
            string error="Error: character found; please enter a number \n";
            throw error;
        }
        height=atoi(temp);
        delete[] temp;
    }
    catch(string error){
        cout<<error;
        askHeight();
    }
}
void Plant::askAge(){
    try{
        bool num=true;
        const int maxChars = 50;
        char* temp= new char[maxChars];
        cout<<"How old is the plant in days?: ";
        cin>>temp;
        
        for(int i=0;temp[i]!='\0';i++){
            if(!isdigit(temp[i])){
                num=false;
            }
        }
        if(num==false){
            delete[] temp;
            string error="Error: character found; please enter a number \n";
            throw error;
        }
        age=atoi(temp);
        delete[] temp;
    }
    catch(string error){
        cout<<error;
        askAge();
    }
}
void Plant::askLifeStage(){
    try{
        int maxOps=lifeStageMax;
        bool num=true;
        const int maxChars = 50;
        char* temp= new char[maxChars];
        cout<<"What stage of its life is the plant in?"<<endl;
        cout<<"[0]Seed\n"
                <<"[1]Sapling\n"
                <<"[2]Mature\n"
                <<"[3]Harvest\n";
        cin>>temp;
        
        if(atoi(temp)>maxOps||atoi(temp)<0){
            delete[] temp;
            string error="Please Enter a valid option\n";
            throw error;
        }
        lifeStage=atoi(temp);
        delete[] temp;
    }
    catch(string error){
        cout<<error;
        askLifeStage();
    }
}
void Plant::askType(){
    try{
        int maxOps=typeMax;
        bool num=true;
        const int maxChars = 50;
        char* temp= new char[maxChars];
        cout<<"What type of plant is it?"<<endl;
        cout<<"[0]Unknown\n"
                <<"[1]Vegetable\n"
                <<"[2]Fruit\n"
                <<"[3]Nut\n"
                <<"[4]Bush\n";
        cin>>temp;
        
        if(atoi(temp)>maxOps||atoi(temp)<0){
            delete[] temp;
            string error="Please Enter a valid option\n";
            throw error;
        }
        type=atoi(temp);
        delete[] temp;
    }
    catch(string error){
        cout<<error;
        askType();
    }
}
void Plant::askDescription(){
    cout<<"Please Write a description of the plant"<<endl;
    cin.ignore();
    getline(cin,description);
}
void Plant::editPlantMenu(char* choice){
    try{
        cout<<this;
        int maxOps =6;
        cout<<"What would you like to do?"<<endl;
        cout<<"[0]Change Name\n"
                <<"[1]Change Height\n"
                <<"[2]Change Age\n"
                <<"[3]Change LifeStage\n"
                <<"[4]Change Type\n"
                <<"[5]Change Description\n"
                <<"[6]Change Everything\n";
        cin>>choice;
        if(atoi(choice)>maxOps||atoi(choice)<0){
            string error="Please Enter a valid option\n";
            throw error;
        }
        switch(atoi(choice)){
            case 0:
                askName();
                break;
            case 1:
                askHeight();
                break;
            case 2:
                askAge();
                break;
            case 3:
                askLifeStage();
                break;
            case 4:
                askType();
                break;
            case 5:
                askDescription();
                break;
            case 6:
                askName();
                askHeight();
                askAge();
                askLifeStage();
                askType();
                askDescription();
                break;
            default:
                cout<<"Error: option does not exist"<<endl;
                break;
        }
    }
    catch(string error){
        cout<<error;
        editPlantMenu(choice);
    }
}
void Plant::editPlantMenu(){
    askName();
    askHeight();
    askAge();
    askLifeStage();
    askType();
    askDescription();
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Plant.h
 * Author: User
 *
 * Created on June 6, 2019, 9:52 PM
 */

#ifndef PLANT_H
#define PLANT_H

#include <string>
#include <iostream>

using namespace std;


enum PlantLifeStages{seed,sapling,mature,harvest,lifeStageMax};
enum PlantType{unknown,vegetable,fruit,nut,bush,typeMax};

string getLifeStageString(int);
string getTypeString(int);

class Plant {
public:
    Plant();
    Plant(string);
    Plant(string,int);
    Plant(const Plant &);
    const Plant &operator=(const Plant &);
    virtual ~Plant();
    
    //outputs
    friend ostream& operator<<(ostream&,const Plant &);
    friend ostream& operator<<(ostream&,const Plant*);
    
    //setters
    void setName(string);
    void setAge(int);
    void setLifeStage(int);
    void setType(int);
    void setDescription(string);
    
    //getters
    int getAge();
    int getLifeStage();
    int getType();
    string getName();
    string getDescription();
    
    //ask user for values
    void askName();
    void askHeight();
    void askAge();
    void askLifeStage();
    void askType();
    void askDescription();
    
    virtual void editPlantMenu(char*);
    virtual void editPlantMenu();
protected:
    string name;
    int height;  //in centimeters
    int age;     //in days
    int lifeStage;   //enum PlantLifeStages
    int type;    //unknown,vegetable,fruit,nut
    string description;
};

#endif /* PLANT_H */


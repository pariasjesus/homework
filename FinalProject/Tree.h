/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Tree.h
 * Author: User
 *
 * Created on June 6, 2019, 10:21 PM
 */

#ifndef TREE_H
#define TREE_H
#include "Plant.h"

class Tree:public Plant{
    //trees live longer and grow taller
private:
    int years;   //age in years
    int meters; //height in meters
    int area;    //area in meters square tree takes up
public:
    Tree();
    Tree(string);
    Tree(string,int);
    Tree(const Tree &);
    const Tree &operator=(const Tree &);
    ~Tree();
    friend ostream& operator<<(ostream&,const Tree &);
    friend ostream& operator<<(ostream&,const Tree*);
    //setters
    void setYears(int);
    void setMeters(int);
    void setArea(int);
    //getters
    int getYears();
    int getMeters();
    int getArea();
    
    void askYears();
    void askMeters();
    void askArea();
    
    void editPlantMenu(char*);
    void editPlantMenu();
};

#endif /* TREE_H */


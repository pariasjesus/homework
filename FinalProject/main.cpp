#include <cstdlib>
#include "Bush.h"
#include "Cactus.h"
#include "Tree.h"
#include "Journal.h"
#include "Menus.h"
#include <iomanip>
#include <fstream>

using namespace std;


int main(){
    int maxPlants=50;   //max of 50 plants in garden
    int numOfPlants=0;
    char* choice = new char[25];
    Plant** plants=new Plant*[maxPlants];
    
    Journal gardenJournal("My Journal",numOfPlants);
    mainMenu(choice,numOfPlants,plants);
    return 0;
}
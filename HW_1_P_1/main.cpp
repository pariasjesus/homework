/* 
 * File:   main.cpp
 * Author: Jesus Parias-Castillo
 * Purpose: Homework 1 Problem 1
 * Created on April 19, 2019, 9:21 PM
 */

#include <cstdlib>
#include <iostream>
#include <ctime>

using namespace std;

int* createArray(int);
void fillArray(int*,int);
void disArray(int*,int);


int main(int argc, char** argv) {
    srand(time(0));
    int size;
    cout << "Enter the size of the array:" ;
    cin >> size;
    int* array = createArray(size);
    fillArray(array,size);
    disArray(array,size);
    delete[] array;
    array = NULL;
    return 0;
}
int* createArray(int size){
	int* array = new int[size];
	return array;
}

void fillArray(int* array, int size){
	for(int n=0;n<size;n++){
		array[n]= rand()%100;
	}
}

void disArray(int* array, int size){
	cout << "Array Values" <<endl;
	for(int n=0;n<size;n++){
		cout << n << "	" << array[n]<<endl;
	}
}
/* 
 * File:   main.cpp
 * Author: Jesus Parias-Castillo
 * Purpose: Homework 1 Problem 2
 * Created on April 19, 2019, 9:27 PM
 */

#include <cstdlib>
#include <iostream>
#include <ctime>

using namespace std;

string* createArray(int);
void fillArray(string*,int);
void disArray(string*,int);
string* deleteEntry(string*,int&,int);


int main(int argc, char** argv) {
    srand(time(0));
    int size = 5;
    int loc;
    string* array = createArray(size);
    fillArray(array,size);
    disArray(array,size);
    cout << "which spot would you like to delete?:";
    cin >> loc;
    array=deleteEntry(array,size,loc);
    disArray(array,size);
    delete[] array;
    array = NULL;
    return 0;
}
string* createArray(int size){
	string* array = new string[size];
	return array;
}

void fillArray(string* array, int size){
	for(int n=0;n<size;n++){
		array[n]= "12345" + n;
	}
}

void disArray(string* array, int size){
	cout << "Array Values" <<endl;
	for(int n=0;n<size;n++){
		cout << n << "	" << array[n]<<endl;
	}
}
string* deleteEntry(string *array, int& size, int loc){
    size--;
    string* newArray = new string[size];
    for(int n=0;n<size;n++){
        if(n<loc){
            newArray[n]=array[n];
        }
        else if(n>=loc){
            newArray[n]=array[n+1];
        }
    }
    return newArray;
}
/* 
 * File:   main.cpp
 * Author: Jesus Parias-Castillo
 * Purpose: Homework 1 Problem 3
 * Created on April 19, 2019, 9:47 PM
 */

#include <cstdlib>
#include <iostream>
#include <ctime>

using namespace std;

int* createArray(int);
void coinToss(int*,int);
void disArray(int*,int);


int main(int argc, char** argv) {
    srand(time(0));
    int flips;
    cout << "Enter the number of flips:" ;
    cin >> flips;
    int* array = createArray(flips);
    coinToss(array,flips);
    disArray(array,flips);
    delete[] array;
    array = NULL;
    return 0;
}
int* createArray(int size){
	int* array = new int[size];
	return array;
}

void coinToss(int* array, int flips){
	for(int n=0;n<flips;n++){
		array[n]= rand() % 2+1;
	}
}

void disArray(int* array, int size){
	for(int n=0;n<size;n++){
            cout << n+1 << "   ";
            if(array[n]==1){
		cout << "Heads" <<endl;
            }
            else if(array[n]==2){
		cout << "Tails" <<endl;
            }
	}
}
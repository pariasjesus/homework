/* 
 * File:   main.cpp
 * Author: Jesus Parias-Castillo
 * Purpose: Homework 1 Problem 5
 * Created on April 19, 2019, 11:26 PM
 */

#include <cstdlib>
#include <iostream>
#include <ctime>

using namespace std;

float* createArray(int);
void fillArray(float*,int);
void sortTests(float*,int);
void disTests(float*,int);
float getAve(float*,int);
void isValid(float&,int);

int main(int argc, char** argv) {
    srand(time(0));
    int tests;
    cout << "How many tests are being graded?:" ;
    cin >> tests;
    float* testScores = createArray(tests);
    fillArray(testScores,tests);
    sortTests(testScores,tests);
    disTests(testScores,tests);
    delete[] testScores;
    testScores = NULL;
    return 0;
}
float* createArray(int size){
	float* array = new float[size];
	return array;
}

void fillArray(float* array, int size){
	for(int n=0;n<size;n++){
            cout << "Test " << n+1<< ": ";
            cin >> array[n];
            isValid(array[n],n);
	}
}

void sortTests(float* array, int size){
    float temp=array[0];
    for(int i=0;i<size;i++){
        for(int n=0;n<size-1;n++){
            if(array[n]>array[n+1]){
		temp=array[n];
                array[n]=array[n+1];
                array[n+1]=temp;
            }
	}
    }
}

void disTests(float* array, int size){
    cout << "Sorted Tests" << endl;
	for(int n=0;n<size;n++){
            cout << "Test " << n+1<< ":  " << array[n] << "%" <<endl;
	}
    cout << "The lowest score will be dropped" <<endl;
    cout << "Average Score" << endl;
    cout << getAve(array,size) << "%" << endl;
}
float getAve(float* array,int size){
    float ave = 0;
    for(int n=1;n<size;n++){
        ave += array[n];
    }
    ave /= size-1;
    return ave;
}
void isValid(float &score,int testNum){
    while(score<0){
        cout << "Error: Can't accept a negative score, Try again " <<endl;
        cout << "Test " << testNum+1 << ": ";
        cin >> score;
    }
}
/* 
 * File:   main.cpp
 * Author: Jesus Parias-Castillo
 * Purpose: Homework 1
 * Created on April 19, 2019, 11:26 PM
 */

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <ctime>


using namespace std;

int* createArray(int);
string* createArray2(int);
string* deleteEntry(string*,int&,int);
void coinToss(int*,int);
void fillArray(int*,int);
void fillArray2(string*,int);
void fillArray4(float*,int);
void fillArray6(float*,int);
void sortTests(float*,int);
void disArray(int*,int);
void disArray2(string*,int);
void disArray3(int*,int);
void disTests4(float*,int);
void disTests5(float*,int);
void disTests6(float*,int);
float getAve4(float*,int);
float getAve6(float*,int);
void isValid(float&,int);
void readScores(float*,int);

int main(int argc, char** argv) {
    srand(time(0));
    int size=5;
    int loc;
    int flips;
    int prob;
    int tests4;
    int tests5;
    int tests6;
        fstream testNum;
        testNum.open("testNum.dat");
        testNum>>tests6;
        testNum.close();
    string* array2 = new string[size];
        fillArray2(array2,size);
    float* testScores6 = new float[tests6];
        fillArray6(testScores6,tests6);
        sortTests(testScores6,tests6);
   
    for(int i=1;i<7;i++){
        if(i==1){
                cout << "Problem 1"<<endl;
                cout << "Enter the size of the array:" ;
                cin >> size;
                int* array1 = createArray(size);
                fillArray(array1,size);
                disArray(array1,size);
                delete[] array1;
                array1 = NULL;
        }else if(i==2){
                int size = 5;
                cout<< "Problem 2"<<endl;
                disArray2(array2,size);
                cout << "which spot would you like to delete?:";
                cin >> loc;
                array2=deleteEntry(array2,size,loc);
                disArray2(array2,size);
        }else if(i==3){
                cout<< "Problem 3"<<endl;
                cout << "Enter the number of flips:" ;
                cin >> flips;
                int* stats = new int[flips];
                coinToss(stats,flips);
                disArray3(stats,flips);
                delete[] stats;
                stats = NULL;
        }else if(i==4){
                cout<< "Problem 4"<<endl;
                cout << "How many tests are being graded?:" ;
                cin >> tests4;
                float* testScores4 = new float [tests4];
                fillArray4(testScores4,tests4);
                sortTests(testScores4,tests4);
                disTests4(testScores4,tests4);
                delete[] testScores4;
                testScores4 = NULL;
        }else if(i==5){
                cout<< "Problem 5"<<endl;
                cout << "How many tests are being graded?:" ;
                cin >> tests5;
                float* testScores5 = new float[tests5];
                fillArray4(testScores5,tests5);
                sortTests(testScores5,tests5);
                disTests5(testScores5,tests5);
                delete[] testScores5;
                testScores5 = NULL;
        }else if(i==6){
                cout<< "Problem 6"<<endl;
                disTests6(testScores6,tests6);
        }
    }
    delete[] array2;
    array2 = NULL;
    delete[] testScores6;
    testScores6 = NULL;
    return 0;
}
int* createArray(int size){
	int* array = new int[size];
	return array;
}

void coinToss(int* array, int flips){
	for(int n=0;n<flips;n++){
		array[n]= rand() % 2+1;
	}
}
string* deleteEntry(string *array, int& size, int loc){
    size--;
    string* newArray = new string[size];
    for(int n=0;n<size;n++){
        if(n<loc){
            newArray[n]=array[n];
        }
        else if(n>=loc){
            newArray[n]=array[n+1];
        }
    }
    return newArray;
}
void fillArray(int* array, int size){
	for(int n=0;n<size;n++){
		array[n]= rand()%100;
	}
}
void fillArray2(string* array, int size){
	for(int n=0;n<size;n++){
		array[n]= "12345" + n;
	}
}
void fillArray4(float* array, int size){
	for(int n=0;n<size;n++){
            cout << "Test " << n+1<< ": ";
            cin >> array[n];
            isValid(array[n],n);
	}
}
void fillArray6(float* array, int size){
        fstream scores;
        scores.open("scores.dat");
        for(int line=0;line<size;line++){
            scores>>array[line];
        }
        scores.close();
}

void sortTests(float* array, int size){
    float temp=array[0];
    for(int i=0;i<size;i++){
        for(int n=0;n<size-1;n++){
            if(array[n]>array[n+1]){
		temp=array[n];
                array[n]=array[n+1];
                array[n+1]=temp;
            }
	}
    }
}
void disArray(int* array, int size){
	cout << "Array Values" <<endl;
	for(int n=0;n<size;n++){
		cout << n << "	" << array[n]<<endl;
	}
}
void disArray2(string* array, int size){
	cout << "Array Values" <<endl;
	for(int n=0;n<size;n++){
		cout << n << "	" << array[n]<<endl;
	}
}
void disArray3(int* array, int size){
	for(int n=0;n<size;n++){
            cout << n+1 << "   ";
            if(array[n]==1){
		cout << "Heads" <<endl;
            }
            else if(array[n]==2){
		cout << "Tails" <<endl;
            }
	}
}
void disTests4(float* array, int size){
    cout << "Sorted Tests" << endl;
	for(int n=0;n<size;n++){
            cout << "Test " << n+1<< ":  " << array[n] << "%" <<endl;
	}
    cout << "Average Score" << endl;
    cout << getAve4(array,size) << "%" << endl;
}
void disTests5(float* array, int size){
    cout << "Sorted Tests" << endl;
	for(int n=0;n<size;n++){
            cout << "Test " << n+1<< ":  " << array[n] << "%" <<endl;
	}
    cout << "The lowest score will be dropped" <<endl;
    cout << "Average Score" << endl;
    cout << getAve6(array,size) << "%" << endl;
}
void disTests6(float* array, int size){
    cout << "Sorted Tests from file" << endl;
	for(int n=0;n<size;n++){
            cout << "Test " << n+1<< ":  " << array[n] << "%" <<endl;
	}
    cout << "The lowest score will be dropped" <<endl;
    cout << "Average Score" << endl;
    cout << getAve6(array,size) << "%" << endl;
}
float getAve4(float* array,int size){
    float ave = 0;
    for(int n=0;n<size;n++){
        ave += array[n];
    }
    ave /= size;
    return ave;
}
float getAve6(float* array,int size){
    float ave = 0;
    for(int n=1;n<size;n++){
        ave += array[n];
    }
    ave /= size-1;
    return ave;
}
void isValid(float &score,int testNum){
    while(score<0){
        cout << "Error: Can't accept a negative score, Try again " <<endl;
        cout << "Test " << testNum+1 << ": ";
        cin >> score;
    }
}
void readScores(float* array, int size){
    fstream scores;//Look through file to find the number
        scores.open("scores.dat");
        for(int n=0;n<size;n++){
            scores>>array[n];
        }
        scores.close();
}
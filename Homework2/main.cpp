/* 
 * File:   main.cpp
 * Author: Jesus Parias-Castillo
 * Purpose: Homework2
 * Created on April 22, 2019, 9:57 PM
 */

#include <cstdlib>
#include <iostream>

using namespace std;

int* revArray(int*,int);
void disArray(string,int*,int);
void isValidPos(int&);
void fillMovies(int*,int);
void sortLtG(int*,int);
int getAve(int*,int);
int getMed(int*,int);
int getMode(int*,int);
int getChars(char*);
void chDisFlip(char*);
void* sortNames(char*,char*,char*,char*);
int getCharSum(char*);
void disLaH(char*);

int main(int argc, char** argv) {
    
    //Prob 1
    cout << "Problem 1"<<endl;
    int size=8;
    int* array = new int[size];
    for(int n=0;n<size;n++){
        array[n]=n;
    }
    disArray("Original",array,size);
    array = revArray(array,size);
    disArray("Reversed",array,size);
    cout<<endl;
    
    //Prob 2
    int studS;  //students surveyed
    int* movies;  //movies
    cout << "Problem 2"<<endl;
    cout << "How many students were surveyed?:";
    cin >> studS;
    isValidPos(studS);
    movies = new int[studS];
    fillMovies(movies,studS);
    cout<<"Average:"<<getAve(movies,studS)<<endl;
    cout<<"Median:"<<getMed(movies,studS)<<endl;
    cout<<"Mode:"<<getMode(movies,studS)<<endl<<endl;
    
    //Prob 3
    char* word = new char[100];
    cout << "Problem 3"<<endl;
    cout << "Enter a word and I will count the letters"<<endl;
    cin >> word;
    cout << "Word: " <<word<<endl;
    cout << "Word Count: "<< getChars(word)<<endl<<endl;
    
    //Prob 4
    cout << "Problem 4"<<endl;
    chDisFlip(word);
    cout << endl;
    
    //Prob 5
    cout << "Problem 5"<<endl;
    char* firstN = new char[30];
    char* midN = new char[30];
    char* lastN = new char[30];
    char* sortedN = new char[100];
    cout << "Enter your first name: ";
    cin >> firstN;
    cout << "Enter your middle name: ";
    cin >> midN;
    cout << "Enter your last name: ";
    cin >> lastN;
    sortNames(firstN,midN,lastN,sortedN);
    cout << "Sorted Names: "<<sortedN<<endl<<endl;
    
    //Prob 6
    char* num = new char[100];
    cout << "Problem 6"<<endl;
    cout << "Enter some numbers, with no spaces, and I will add them together"<<endl<<": ";
    cin >> num;
    cout << "Sum: "<<getCharSum(num)<<endl;
    disLaH(num);
    
    delete[] firstN;
    firstN = NULL;
    delete[] midN;
    midN = NULL;
    delete[] lastN;
    lastN = NULL;
    delete[] sortedN;
    sortedN = NULL;
    delete[] array;
    array = NULL;
    delete[] movies;
    movies = NULL;
    delete[] word;
    word = NULL;
    delete[] num;
    num = NULL;
    return 0;
}
//Reverse Array
int* revArray(int* array,int size){
    int* tempArray = new int[size];
    size--;
    for(int n=0;n<=size;n++){
        tempArray[n]=array[size-n];
    }
    return tempArray;
}
void disArray(string word,int* array,int size){
    cout<<word + " Array"<<endl;
    for(int n =0;n<size;n++){
        cout<<array[n]<<endl;
    }
}
void isValidPos(int &value){
    while(value<0){
        cout<<"Cant accept negative values"<<endl;
        cout<<"Enter a positive value:";
        cin>>value;
    }
}
void fillMovies(int* array,int size){
    cout<<"Enter the number of movies each student saw;"<<endl;
    for(int n=0;n<size;n++){
        cout<<"Student "<<n+1<<":";
        cin >> array[n];
        isValidPos(array[n]);
    }
}
int getAve(int* array,int size){
    int ave=0;
    for(int n=0;n<size;n++){
        ave += array[n];
    }
    return ave/size;
}
int getMode(int* array,int size){
    int count=0;
    int highCount=1;
    int mode;
    int temp;
    for(int n=0;n<size;n++){
        temp = array[n];
        count = 0;
        for(int n=0;n<size;n++){
            if(temp==array[n]){
                count++;
            }
        }
        if(count>=highCount){
            if(count==highCount){
                if(temp>mode){
                    mode=temp;
                    highCount=count;
                }
            }else
            mode=temp;
            highCount=count;
        }
    }  
    return mode;
}
void sortLtG(int* array, int size){
    int temp=array[0];
    for(int i=0;i<size;i++){
        for(int n=0;n<size-1;n++){
            if(array[n]>array[n+1]){
		temp=array[n];
                array[n]=array[n+1];
                array[n+1]=temp;
            }
	}
    }
}
int getMed(int* array,int size){
    int med;
    int *temp = new int[size];
    for(int n=0;n<size;n++){
        temp[n]=array[n];
    }
    sortLtG(temp,size);
    if(size%2!=0){
        med = temp[size/2];
    }else if(size%2==0){
        med = (temp[size/2] + temp[(size/2)+1])/2;
    }
    
    delete[] temp;
    temp = NULL;
    return med;
}
int getChars(char* word){
    int count =0;
    while(word[count]!='\0'){
        count++;
    }
    return count;
}
void chDisFlip(char* word){
    int count =0;
    while(word[count]!='\0'){
        count++;
    }
    cout << "String: " << word<<endl;
    cout << "String Backwards: ";
    for(int n=0;n<=count;n++){
        cout << word[count-n];
    }cout << endl;
}
void* sortNames(char* first,char* mid,char* last,char* sorted){
    int count1 =0;
    int count2 =0;
    int count3 =0;
    int tot=0;
    while(last[count1]!='\0'){
        count1++;
    }
    for(int n=0;n<count1;n++){
        sorted[n]=last[n];
    }
    sorted[count1]=',';
    sorted[count1+1]=' ';
    tot = count1 +2;
    while(first[count2]!='\0'){
        count2++;
    }
    for(int n=0;n<count2;n++){
        sorted[tot+n]=first[n];
    }
    sorted[tot+count2]=' ';
    tot += count2 + 1;
    while(mid[count3]!='\0'){
        count3++;
    }
    for(int n=0;n<count3;n++){
        sorted[tot+n]=mid[n];
    }
}
int getCharSum(char* digi){
    int sum=0;
    int count =0;
    while(digi[count]!='\0'){
        if(isdigit(digi[count])){
            sum += static_cast<int>(digi[count]-48);
        }
        count++;
    }
    return sum;
}
void disLaH(char* digi){
    int low=100;
    int high=0;
    int count =0;
    while(digi[count]!='\0'){
        if(isdigit(digi[count])){
            if(digi[count]-48<low){
                low = digi[count]-48;
            }
            if(digi[count]-48>high){
                high = digi[count]-48;
            }
        }
        count++;
    }
    cout<<"Lowest Number: "<< low<<endl;
    cout<<"Highest Number: "<< high<<endl;
}
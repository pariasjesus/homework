 /* 
 * File:   main.cpp
 * Author: Jesus Parias-Castillo
 * Purpose: Homework 3
 * Created on April 26, 2019, 8:01 PM
 */

#include <cstdlib>
#include <iostream>


using namespace std;

void printDate(char*);
string getMonth(int);
void sortNames(char*,char*,char*,char*);

struct MovieData{
    string title;
    string director;
    int yearR;
    int time;
};
struct MovieData2{
    string title;
    string director;
    int yearR;
    int time;
    int prodCost;
    int yearRev;
};

struct MonthlyWeather{
    int totRainfall;
    int highTemp;
    int lowTemp;
    int aveTemp;
};

void getMovies(MovieData*,int);
void displayMovies(MovieData*,int);
void getMovies2(MovieData2*,int);
void displayMovies2(MovieData2*,int);
void getProfit(int,int);
void isTempValid(int &);
void getTotRain(MonthlyWeather*,int,int&);
void getAveATemp(MonthlyWeather*,int,int &);
void getAve(MonthlyWeather*,int);
void getExtremaTemp(MonthlyWeather*,int,int &,int &);
void getAveARain(MonthlyWeather*,int,int &a);
void printAnnualWeather(MonthlyWeather*,int,int,int,int,int,int);
void getWeather(MonthlyWeather*,int);

int main(int argc, char** argv) {
    
    cout<<"Problem 1"<<endl;
    char* date = new char[20];
    cout<<"Please enter a date in the form mm/dd/yyyy: ";
    cin>>date;
    printDate(date);
   
    cout<<"Problem 2"<<endl;
    char* firstN = new char[30];
    char* midN = new char[30];
    char* lastN = new char[30];
    char* sortedN = new char[100];
    cout << "Enter your first name: ";
    cin >> firstN;
    cout << "Enter your middle name: ";
    cin >> midN;
    cout << "Enter your last name: ";
    cin >> lastN;
    sortNames(firstN,midN,lastN,sortedN);
    cout << "Sorted Names: "<<sortedN<<endl<<endl;
     
    cout<<"Problem 3"<<endl;
    int size = 2;
    MovieData* movie= new MovieData[size];
    getMovies(movie,size);
    displayMovies(movie,size);
    
    cout<<"Problem 4"<<endl;
    MovieData2* movie2= new MovieData2[size];
    getMovies2(movie2,size);
    displayMovies2(movie2,size);
    
    cout<<"Problem 5"<<endl;
    int highest;
    int lowest;
    int aveMTemp;
    int months = 12;
    int aveMRain;
    int totRF;
    MonthlyWeather* currentYear = new MonthlyWeather[months];
    getWeather(currentYear,months);
    getAve(currentYear,months);
    getTotRain(currentYear,months,totRF);
    getAveATemp(currentYear,months,aveMTemp);
    getExtremaTemp(currentYear,months,highest,lowest);
    getAveARain(currentYear,months,aveMRain);
    printAnnualWeather(currentYear,months,highest,lowest,aveMRain,totRF,aveMTemp);
    
    
    delete[] currentYear;
    currentYear=NULL;
    delete[] movie;
    movie=NULL;
    delete[] movie2;
    movie2=NULL;
    delete[] firstN;
    firstN = NULL;
    delete[] midN;
    midN = NULL;
    delete[] lastN;
    lastN = NULL;
    delete[] sortedN;
    sortedN = NULL;
    delete[] date;
    date = NULL;
    return 0;
}

void printDate(char* date){
    char* month = new char[3];
    char* day = new char[3];
    char* year = new char[5];
    //--/--/----
    //0123456789
    //each loop separates the three components of the date
    //separates month 
    for(int n=0;n<3;n++){
        month[n]=date[n];
    }
    //separates day
    for(int n=0;n<3;n++){
        day[n]=date[n+3];
    }
    //separates year
    for(int n=0;n<5;n++){
        year[n]=date[n+3+3];
    }
    //converts month from character/digit to word in output
    //converts day and year to digit in output
    cout<<getMonth(atoi(month))<<" "<<atoi(day)<<", "<<atoi(year)<<endl;
    
    delete[] month;
    month = NULL;
    delete[] day;
    day = NULL;
    delete[] year;
    year = NULL;
}
string getMonth(int month){
    string wordM;
    //checks converted month character to its digit
    //and sets it to its word
    switch(month){
        case 1:
            wordM =  "January";
            break;
        case 2:
            wordM =  "February";
            break;
        case 3:
            wordM =  "March";
            break;
        case 4:
            wordM =  "April";
            break;
        case 5:
            wordM =  "May";
            break;
        case 6:
            wordM =  "June";
            break;
        case 7:
            wordM =  "July";
            break;
        case 8:
            wordM =  "August";
            break;
        case 9:
            wordM =  "September";
            break;
        case 10:
            wordM =  "October";
            break;
        case 11:
            wordM =  "November";
            break;
        case 12:
            wordM =  "December";
            break;
        default:
            break;
    }
    return wordM;
}
void sortNames(char* first,char* mid,char* last,char* sorted){
    int count1 =0;
    int count2 =0;
    int count3 =0;
    int tot=0;
    //counts length of last name
    while(last[count1]!='\0'){
        count1++;
    }
    //adds last name to sorted array
    for(int n=0;n<count1;n++){
        sorted[n]=last[n];
    }
    //adds " ,  " between names
    sorted[count1]=',';
    sorted[count1+1]=' ';
    //increase total size of sorted array so far by 2
    tot = count1 +2;
    //counts length of first name
    while(first[count2]!='\0'){
        count2++;
    }
    //adds first name to sorted array
    for(int n=0;n<count2;n++){
        sorted[tot+n]=first[n];
    }
    //adds space between words
    sorted[tot+count2]=' ';
    //increase total size of sorted array so far by 1
    tot += count2 + 1;
    //counts length of middle name
    while(mid[count3]!='\0'){
        count3++;
    }
    //adds middle name to sorted array
    for(int n=0;n<count3;n++){
        sorted[tot+n]=mid[n];
    }
}
void getMovies(MovieData* movie,int size){
    for(int n=0;n<size;n++){
        cout<<"Movie "<<n+1<<endl;
        cout<<"Title of movie:";
        cin>>movie[n].title;
        cout<<"Director of movie:";
        cin>>movie[n].director;
        cout<<"Year movie released:";
        cin>>movie[n].yearR;
        cout<<"Running time in minutes:";
        cin>>movie[n].time;
    }
}
void displayMovies(MovieData* movie,int size){
    for(int n=0;n<size;n++){
        cout<<"Movie "<<n+1<<endl;
        cout<<"Title: "<<movie[n].title<<endl;
        cout<<"Director: "<<movie[n].director<<endl;
        cout<<"Year Released: "<<movie[n].yearR<<endl;
        cout<<"Runtime in minutes: "<<movie[n].time<<endl<<endl;
    }
}
void getMovies2(MovieData2* movie,int size){
    for(int n=0;n<size;n++){
        cout<<"Movie "<<n+1<<endl;
        cout<<"Title of movie: ";
        cin>>movie[n].title;
        cout<<"Director of movie: ";
        cin>>movie[n].director;
        cout<<"Year movie released: ";
        cin>>movie[n].yearR;
        cout<<"Running time in minutes: ";
        cin>>movie[n].time;
        cout<<"Costs of production: ";
        cin>>movie[n].prodCost;
        cout<<"First year revenue: ";
        cin>>movie[n].yearRev;
    }
}
void displayMovies2(MovieData2* movie,int size){
    for(int n=0;n<size;n++){
        cout<<"Movie "<<n+1<<endl;
        cout<<"Title: "<<movie[n].title<<endl;
        cout<<"Director: "<<movie[n].director<<endl;
        cout<<"Year Released: "<<movie[n].yearR<<endl;
        cout<<"Runtime in minutes: "<<movie[n].time<<endl;
        cout<<"Production Cost: $"<<movie[n].prodCost<<endl;
        cout<<"Year End Revenue: $"<<movie[n].yearRev<<endl;
        getProfit(movie[n].prodCost,movie[n].yearRev);
    }
}
void getProfit(int cost,int revenue){
    if(cost>revenue){
        cout<<"Money Lost: "<<cost-revenue<<endl;
    }else if(cost<revenue){
        cout<<"Profit: "<<revenue-cost<<endl;
    }
}
void getWeather(MonthlyWeather* year,int size){
    cout<<"Please enter a the weather for each month within the last year"<<endl;
    for(int n=1;n<=size;n++){
        cout<<getMonth(n)<<endl;
        cout<<"Enter Total Rainfall: ";
        cin>>year[n].totRainfall;
        cout<<"Enter Highest Temperature in Fahrenheit: ";
        cin>>year[n].highTemp;
        isTempValid(year[n].highTemp);
        cout<<"Enter Lowest Temperature in Fahrenheit: ";
        cin>>year[n].lowTemp;
        isTempValid(year[n].lowTemp);
        cout<<endl;
    }
    getAve(year,size);
}
void isTempValid(int &temp){
    while(temp<(-100)||temp>140){
        cout<<"Please enter a valid temperature(-100/140):";
        cin>>temp;
    }
}
void printAnnualWeather(MonthlyWeather* year,int size,int highest,int lowest,int aveRain,int totRain,int aveTemp){
    cout<<"Average Monthly Rainfall: "<<aveRain<<endl;
    cout<<"Total Annual Rainfall: "<<totRain<<endl;
    cout<<"Highest Monthly Temperature: "<<year[highest].highTemp<<getMonth(highest+1)<<endl;
    cout<<"Lowest Monthly Temperature: "<<year[lowest].lowTemp<<getMonth(lowest+1)<<endl;
    cout<<"Average Monthly Temperature"<<aveTemp<<endl;
}
void getAve(MonthlyWeather* year, int size){
    for(int n=0;n<=size-1;n++){
        year[n].aveTemp=0;
        year[n].aveTemp=(year[n].highTemp+year[n].lowTemp);
        year[n].aveTemp=(year[n].aveTemp/size);
    }
}
void getAveATemp(MonthlyWeather* year,int size,int &aveTemp){
    for(int n=0;n<=size-1;n++){
        aveTemp+=year[n].aveTemp;
    }
    aveTemp=aveTemp/size;
}
void getAveARain(MonthlyWeather* year,int size,int &aveRain){
    for(int n=0;n<=size-1;n++){
        aveRain+=year[n].totRainfall;
    }
    aveRain=aveRain/size;
}
void getTotRain(MonthlyWeather* year,int size,int &total){
    for(int n=0;n<size;n++){
        total += year[n].totRainfall; 
    }
}
void getExtremaTemp(MonthlyWeather* year,int size,int &highest,int &lowest){
    for(int n=0;n<size-1;n++){
        if(year[n].highTemp>year[n+1].highTemp){
            highest=n;
        }
    }
    for(int n=0;n<size-1;n++){
        if(year[n].lowTemp<year[n+1].lowTemp){
            lowest=n;
        }
}
}
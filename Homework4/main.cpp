/* 
 * File:   main.cpp
 * Author: Jesus Parias-Castillo
 * Purpose: Homework4
 * Created on April 30, 2019, 9:37 PM
 */

# include <iostream>
# include <cstdlib>
# include <iomanip>
# include <string>

using namespace std;

struct Player{  //A player
    string name="";
    int number=0;
    int points=0;
};//A player

enum Month{ //enumerator for months of the year
    January,
    February,
    March,
    April,
    May,
    June,
    July,
    August,
    September,
    October,
    November,
    December
};//enumerator for months of the year

struct MonthlyWeather{  //weather of the month
    int totRainfall=0;  //total monthly rainfall
    int highTemp=0; //highest temperature of the month
    int lowTemp=0;  //lowest temperature of the month
    int aveTemp=0;  //average temperature of the month
};//weather of the month
class Inventory{
private:
    int itemNumber;
    int quantity;
    double cost;
    double totalCost;
public:
    Inventory();
    Inventory(int,int,double);
    ~Inventory();
    Inventory(const Inventory &);
    Inventory operator=(const Inventory &);
    void setTotalCost();
    void setItemNumber(int);
    void setQuantity(int);
    void setCost(double);
    double getTotalCost();
    double getCost();
    int getQuantity();
    int getItemNumber();
    void isValidPos();
};

class Date{//makes a date of the year
private:
    int month;
    int day;
    int year;
public:
    //constructors
    Date();
    Date(int);  //given month
    Date(int,int);  //given month and day
    Date(int,int,int);  //given all parameters
    //getters
    int getMonth();
    int getDay();
    int getYear();
    //setters
    void setMonth(int);
    void setDay(int);
    void setYear(int);
    //output
    void display();
};//makes a date of the year

class Employee{
private:
    string name;
    int idNumber;
    string department;
    string position;
public:
    //constructors
    Employee();
    Employee(int,string,string,string); //given name,id,department and position
    Employee(int,string); //given name and id
    //getters
    int getIdNumber();
    string getName();
    string getDepatment();
    string getPosition();
    //setters
    void setIdNumber(int);
    void setName(string);
    void setDepartment(string);
    void setPosition(string);
    //output
    void display();
};

void getPlayerInfo(Player*,int);
void getNames(Player*,int);
void getNumbers(Player*,int);
void getScores(Player*,int);
int calcTotalP(Player*,int);
int findHighScore(Player*,int);
void displayAll(Player*,int,int,int);
void isValidPos(int&);

void getWeather(MonthlyWeather*);
void getRainfall(MonthlyWeather*);
void getHighTemp(MonthlyWeather*);
void getLowTemp(MonthlyWeather*);
void getAveTemp(MonthlyWeather*);
string getMonth(int);
void isTempValid(int&);
int calcAveMonthTemp(MonthlyWeather*);
int calcAveMonthRain(MonthlyWeather*);
int calcTotAnnRain(MonthlyWeather*);
int calcHighestTemp(MonthlyWeather*);
int calcLowestTemp(MonthlyWeather*);
void printAnnualWeather(MonthlyWeather*,int,int,int,int,int);

void getDate(Date&);

void getEmployee(Employee*); 

void problemOne();
void problemTwo();
void problemThree();
void problemFour();

int main(){
    
    problemOne();
    problemTwo();
    problemThree();
    problemFour();
    
    return 0;
}

void problemOne(){  //Problem 1
    int size = 12;  //number of players on team
    int totalPts;   //total points team accured
    int highScore;  //represents the spot in the array of the player with highest score
    Player* team = new Player[size];    //array of type Player is being initialized with given size

    getPlayerInfo(team, size);  //gets player information
    totalPts=calcTotalP(team,size);  //calculates total points team accured
    highScore=findHighScore(team,size); //finds the player with the highest score in the team
    displayAll(team,size,totalPts,highScore);   //display all information needed
    
    delete[] team;
    team = NULL;
}
void problemTwo(){  //Problem 2
    int size=12;    //number of months
    //everything set to 0 to avoid memory errors
    int highest=0;  //spot in array with hottest temperature
    int lowest=0;   //spot in array with coldest temperature
    int aveMTemp=0; //average monthly temperature for the year
    int aveMRain=0; //average monthly rainfall for the year
    int totRF=0;    //total annual rainfall
    MonthlyWeather* currentYear = new MonthlyWeather[size]; //array of type monthly weather set to size 12, onr for each month

    getWeather(currentYear);    //gets weather
    totRF=calcTotAnnRain(currentYear);  //calculates total annual rainfall
    aveMRain=calcAveMonthRain(currentYear); //calculates average monthly rainfall for the year
    aveMTemp= calcAveMonthTemp(currentYear); //calculates average monthly temperature for the year
    highest=calcHighestTemp(currentYear);   //calculates hottest month of the year
    lowest=calcLowestTemp(currentYear);     //calculates coldest month of the year
    printAnnualWeather(currentYear,highest,lowest,aveMRain,totRF,aveMTemp);   //displays weather
    
    delete[] currentYear;
    currentYear = NULL;
}
void problemThree(){    //Problem3
    Date date;
    
    cout<<"This program will show you the date in three formats."<<endl;
    getDate(date);
    date.display();
}
void problemFour(){
    int size=3;
    Employee* employee=new Employee[size];
    getEmployee(employee);
    cout<<"Name"<<setw(16)<<setfill(' ')<<fixed<<setw(20)<<"ID Number"<<fixed<<setw(20)<<"Department"<<fixed<<setw(20)<<"Position"<<endl;
    for(int n=0;n<size;n++){
        employee[n].display();
    }
    
    delete[] employee;
    employee=NULL;
}

void getPlayerInfo(Player* team, int size){ //gets the player information
    //runs all functions needed to get player information
    getNames(team,size);
    getNumbers(team,size);
    getScores(team,size);
}
void getNames(Player* team, int size){  //gets the players names
    cout << "The team has " << size << " players." <<endl;
    //loops through number of players
    for(int n=0;n<size;n++){
        //user is asked to enter the name of the selected player 
        cout << "Enter the name of a player: ";
        //user must enter the player's name
        cin >> team[n].name;
    }
}
void getNumbers(Player* team, int size){    //gets the player number for the players
    //asks the user to enter the players number
    cout << "Please enter the players number"<<endl;
    //loops through the number of players
    for(int n=0;n<size;n++){
        //user is asked to enter the selected player's number
        cout << "Enter the number for "<< team[n].name<<": ";
        //user must enter the player's number
        cin >> team[n].number;
        //validates number
        isValidPos(team[n].number);
    }
}
void getScores(Player* team, int size){ //gets the score for the players
    //asks the user to enter the score for each player
    cout << "Please enter how many points each player scored"<<endl;
    //loops through the number of players
    for(int n=0;n<size;n++){
        //user is asked to enter the points for player selected
        cout << "Enter the number of points "<< team[n].name<<" scored: ";
        //user must input player's points
        cin >> team[n].points;
        //validates points
        isValidPos(team[n].points);
    }
}
int calcTotalP(Player* team, int size){ //calculates the total points the team accumulated
    //initializes total score to 0
    int total=0;
    //loops through team adding each team members score to the total
    for(int n=0;n<size;n++){
        total += team[n].points;
    }
    //returns total score
    return total;
}
int findHighScore(Player* team, int size){  //finds the player with the highest score
    //initializes high score to first player in team
    int high=0;
    //loops through team to find the high score
    for(int n=0;n<size-1;n++){
        //if preceeding player has a higher score than the following player, then the preceeding player gets the high score if following statement is met
        if(team[n].points>team[n+1].points){
            //if the preceeding player has a higher score than the player with the high score then the preceeding player gets the high score
            if(team[n].points>team[high].points){
                high = n;
            }
        //if the preceeding player has a lower score than the following player, then the following player gets the high score if the following statement is met
        }else if(team[n].points<team[n+1].points){
            //if the following player has a higher score than the player with the high score then the following player gets the high score
            if(team[n+1].points>team[high].points){
                high = n+1;
            }
        }
    }
    //returns the integer that represents the spot in the array(player list) of the player with the highest score
    return high;
}
void displayAll(Player* team, int size, int totalPts, int high){    //displays all information stored
    cout<<"Here is the list of Players on the team."<<endl;
    cout<<"The player's name, number and total points acured will be displayed"<<endl;
    cout<<"As will the total points the team accumulated and the player with \n"
            "highest number of points."<<endl<<endl;
    for(int n=0;n<size;n++){
        cout<<"Player Name:"<<team[n].name<<endl;
        cout<<"Player Number: #"<<team[n].number<<endl;
        cout<<"Player Points: "<<team[n].points<<endl<<endl;
    }
    cout<<"Total Points of team: "<<totalPts<<endl;
    cout<<"Highest Score: "<<team[high].points<< "  Player: #"<<team[high].number<<"  "<<team[high].name<<endl<<endl;
}
void isValidPos(int &ans){  //checks to see and validate the answer given
    //loops while the answer given is less than 0, 0 will be accepted
    while(ans<0){
        cout<<"Please enter a positive number, negative numbers will not be accepted."<<endl;
        cin>> ans;
    }
}

void getWeather(MonthlyWeather* year){  //gets the weather
    cout<<"Please enter a the weather for each month within the last year"<<endl;
    getRainfall(year);
    getHighTemp(year);
    getLowTemp(year);
    getAveTemp(year);
}
void getRainfall(MonthlyWeather* year){ //gets total rainfall for each month
    //Tells the user to enter the total rainfall for each month
    cout<<"Enter the total rainfall for each month"<<endl;
    //loops through year
    for(Month n = January;n<=December;n=static_cast<Month>(n+1)){
        //outputs the month and asks the user to enter data
        cout<<getMonth(n)<<endl;
        cout<<"Enter Total Rainfall: ";
        cin>>year[n].totRainfall;
    }
}
void getHighTemp(MonthlyWeather* year){ //gets high temperatures for each month
    //Tells the user to enter the high temperatures for each month
    cout<<"Enter the highest temperature for each month"<<endl;
    //loops through the year
    for(Month n = January;n<=December;n=static_cast<Month>(n+1)){
        //outputs the month and asks the user to enter data
        cout<<getMonth(n)<<endl;
        cout<<"Enter Highest Temperature in Fahrenheit: ";
        cin>>year[n].highTemp;
        //validates the temperature
        isTempValid(year[n].highTemp);
    }
}
void getLowTemp(MonthlyWeather* year){  //gets the low temperatures for each month
    //Tells the user to enter the low temperatures for each month
    cout<<"Enter the lowest temperature for each month"<<endl;
    //loops through the year
    for(Month n = January;n<=December;n=static_cast<Month>(n+1)){
        //outputs the month and asks the user to enter data
        cout<<getMonth(n)<<endl;
        cout<<"Enter Lowest Temperature in Fahrenheit: ";
        cin>>year[n].lowTemp;
        //validates the temperature
        isTempValid(year[n].lowTemp);
    }
}
void getAveTemp(MonthlyWeather* year){  //calculates the average temperature for each month
    //loops through year
    for(Month n = January;n<=December;n=static_cast<Month>(n+1)){
        //sets average temperature to avoid memory issues
        year[n].aveTemp=0;
        //adds together the low and high temperatures
        year[n].aveTemp=(year[n].highTemp+year[n].lowTemp);
        //divides by 2 for the two terms used to get the mean
        year[n].aveTemp=(year[n].aveTemp/2);
    }
}
string getMonth(int month){ //gets the month name from the given enumeration(integer)
    string wordM;   //string for the word of the month
    //converts month to string depending on the case
    switch(month){
        case January:
            wordM = "January";
            break;
        case February:
            wordM = "February";
            break;
        case March:
            wordM = "March";
            break;
        case April:
            wordM = "April";
            break;
        case May:
            wordM = "May";
            break;
        case June:
            wordM = "June";
            break;
        case July:
            wordM = "July";
            break;
        case August:
            wordM = "August";
            break;
        case September:
            wordM = "September";
            break;
        case October:
            wordM = "October";
            break;
        case November:
            wordM = "November";
            break;
        case December:
            wordM = "December";
            break;
        default:    //anything else will set name of month as an error message
            wordM = "Error(naming of month)";
            break;
    }
    return wordM;
}
void isTempValid(int &temp){    //validate temperature given
    //loops until desired conditions are met(between -100 and 140)
    while(temp<(-100)||temp>140){
        cout<<"Please enter a valid temperature(-100/140):";
        cin>>temp;
    }
}
int calcAveMonthTemp(MonthlyWeather* year){   //calculates the average temperature of the year given the average temperatures of each months
    //sets average annual temperature to 0 to avoid memory errors
    int AnnAT=0;    //average annual temperature in Fahrenheit
    //loops through year
    for(Month n = January;n<=December;n=static_cast<Month>(n+1)){
        //adds average temperature of month to the average temperature of the year
        AnnAT+=year[n].aveTemp;
    }
    //divides average temperature by 12 to get mean temperature of the year
    AnnAT=AnnAT/12;
    return AnnAT;
}
int calcAveMonthRain(MonthlyWeather* year){   //calculates the average monthly rainfall for the year 
    //sets monthly average rainfall to 0 to avoid memory errors
    int monthAR=0;  //month average rainfall
    //loops through year
    for(Month n = January;n<=December;n=static_cast<Month>(n+1)){
        //adds total rainfall for the month to average monthly rainfall for the year
        monthAR+=year[n].totRainfall;
    }
    //divides average monthly rainfall by 12 to get mean rainfall for the year
    monthAR=monthAR/12;
    return monthAR;
}
int calcTotAnnRain(MonthlyWeather* year){   //calculates total annual rainfall
    //sets total annual rainfall to 0 to avoid memory errors
    int totAR=0;    //total annual rainfall
    //loops through the year
    for(Month n = January;n<=December;n=static_cast<Month>(n+1)){
        //adds total rainfall for the month to the total rainfall for they ear
        totAR+=year[n].totRainfall;
    }
    return totAR;
}
int calcHighestTemp(MonthlyWeather* year){ //calculates the highest temperature for the year
    //sets highest spot to 0 to avoid memory errors
    int highest=0;  //represents the spot in the array that has the highest temperature 
    //loops through the year up to November
    for(Month n = January;n<December;n=static_cast<Month>(n+1)){
        //If the preceeding month is hotter than the following month then check following statement
        if(year[n].highTemp>year[n+1].highTemp){
            //if preceeding month is hotter than the currently recorded hottest month, set the preceeding month to be the hottest month
            if(year[n].highTemp>year[highest].highTemp){
                highest = n;
            }
        }
    }
    return highest;
}
int calcLowestTemp(MonthlyWeather* year){//calculates the coldest temperature for the year
    //sets lowest spot to 0 to avoid memory erorrs
    int lowest=0;  //represents the spot in the array that has the coldest temperature 
    //loops through the year up to November
    for(Month n = January;n<December;n=static_cast<Month>(n+1)){
        //If the preceeding month is colder than the following month then check following statement
        if(year[n].lowTemp<year[n+1].lowTemp){
            //if preceeding month is hotter than the currently recorded coldest month, set the preceeding month to be the coldest month
            if(year[n].lowTemp<year[lowest].lowTemp){
                lowest = n;
            }
        }
    }
    return lowest;
}
void printAnnualWeather(MonthlyWeather* year,int highest,int lowest,int aveRain,int totRain,int aveTemp){   //Print the desired information
    cout<<"Average Monthly Rainfall: "<<aveRain<<endl;
    cout<<"Total Annual Rainfall: "<<totRain<<endl;
    cout<<"Highest Monthly Temperature: "<<year[highest].highTemp<<"Fahrenheit, "<<getMonth(highest)<<endl;
    cout<<"Lowest Monthly Temperature: "<<year[lowest].lowTemp<<"Fahrenheit, "<<getMonth(lowest)<<endl;
    cout<<"Average Monthly Temperature"<<aveTemp<<endl;
}

void getDate(Date &date){
    int month;
    int day;
    int year;
    cout<<"Enter the month as a number: ";
    cin>>month;
    while(month<1||month>12){
        cout<<"Please enter a valid month: ";
        cin>>month;
    }
    date.setMonth(month);
    cout<<"Enter the day: ";
    cin>>day;
    while(month<1||month>31){
        cout<<"Please enter a valid day: ";
        cin>>day;
    }
    date.setDay(day);
    cout<<"Enter the year: ";
    cin>>year;
    date.setYear(year);
}
Date::Date(){
   month=1;
   day=1;
   year=2019;
}
Date::Date(int month){
    this->month=month;
    day=1;
    year=2019;
}
Date::Date(int month,int day){
    this->month=month;
    this->day=day;
    year=2019;
}
Date::Date(int month,int day,int year){
    this->month=month;
    this->day=day;
    this->year=year;
}
int Date::getMonth(){
    return month;
}
int Date::getDay(){
    return day;
}
int Date::getYear(){
    return year;
}
void Date::setMonth(int month){
    this->month=month;
}
void Date::setDay(int day){
    this->day=day;
}
void Date::setYear(int year){
    this->year=year;
}
void Date::display(){
    cout<<month<<"/"<<day<<"/"<<year<<endl;
    cout<<::getMonth(month-1)<<" "<<day<<", "<<year<<endl;
    cout<<day<<" "<<::getMonth(month-1)<<" "<<year<<endl;
}

void getEmployee(Employee* employee){
    employee[0].setName("Susan Meyers");
    employee[1].setName("Mark Jones");
    employee[2].setName("JoyRogers");
    employee[0].setIdNumber(47899);
    employee[1].setIdNumber(39119);
    employee[2].setIdNumber(81774);
    employee[0].setDepartment("Accounting");
    employee[1].setDepartment("IT");
    employee[2].setDepartment("Manufacturing");
    employee[0].setPosition("Vice President");
    employee[1].setPosition("Programmer");
    employee[2].setPosition("Engineer");
}
Employee::Employee(){
    idNumber=0;
    name="";
    department="";
    position="";
}
Employee::Employee(int idNumber,string name,string department,string position){
    this->idNumber=idNumber;
    this->name=name;
    this->department=department;
    this->position=position;
}
Employee::Employee(int idNumber,string name){
    this->idNumber=idNumber;
    this->name=name;
    department="";
    position="";
}
int Employee::getIdNumber(){
    return idNumber;
}
string Employee::getName(){
    return name;
}
string Employee::getDepatment(){
    return department;
}
string Employee::getPosition(){
    return position;
}
void Employee::setIdNumber(int idNumber){
    this->idNumber=idNumber;
}
void Employee::setName(string name){
    this->name=name;
}
void Employee::setDepartment(string department){
    this->department=department;
}
void Employee::setPosition(string position){
    this->position=position;
}
void Employee::display(){
    int temp = name.length();
    cout<<name<<setw(20-temp)<<setfill('0')<<setw(20)<<idNumber<<setw(20)<<department<<setw(20)<<position<<endl;
}

Inventory::Inventory(){
    itemNumber=0;
    quantity=0;
    cost=0;
    totalCost=0;
}
Inventory::Inventory(int itemNumber,int quantity,double cost){
    isValidPos();
    this->itemNumber=itemNumber;
    this->quantity=quantity;
    this->cost=cost;
    this->setTotalCost();
}
void Inventory::setTotalCost(){
    totalCost=quantity * cost;
}
void Inventory::setItemNumber(int itemNumber){
    isValidPos();
    this->itemNumber=itemNumber;
}
void Inventory::setQuantity(int quantity){
    isValidPos();
    this->quantity=quantity;
    this->setTotalCost();
}
void Inventory::setCost(double cost){
    isValidPos();
    this->cost=cost;
    this->setTotalCost();
}
double Inventory::getCost(){
    return cost;
}
double Inventory::getTotalCost(){
    return totalCost;
}
int Inventory::getItemNumber(){
   return itemNumber; 
}
int Inventory::getQuantity(){
    return quantity;
} 
void Inventory::isValidPos(){
    while(itemNumber<0||quantity<0||cost<0){
        if(itemNumber<0){
            cout<<"Please enter a positive item number: ";
            cin>>itemNumber;
        }
        if(quantity<0){
            cout<<"Please enter a positive quantity: ";
            cin>>quantity;
        }
        if(cost<0){
            cout<<"Please enter a positive cost: ";
            cin>>cost;
        }
    }
}
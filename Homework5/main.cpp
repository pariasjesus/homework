/* 
 * File:   main.cpp
 * Author: Jesus Parias-Castillo
 * Purpose Homework 5
 * Created on May 4, 2019, 8:50 PM
 */

#include <cstdlib>
#include <iostream>
#include <ctime>
#include <string.h>
#include <iomanip>

using namespace std;

class NumberArray{
private:
    float* numbers;
    int size;
public:
    NumberArray();
    NumberArray(int);
    ~NumberArray();
    NumberArray(const NumberArray &);
    NumberArray operator=(const NumberArray &);
    void setNumber(float num,int loc);
    int getSize();
    float getNumber(int loc);
    float getHighestNumber();
    float getLowestNumber();
    float getAverage();
    void output();
};
class Coin{
private:
    string sideUp;
public:
    Coin();
    ~Coin();
    Coin(const Coin &);
    Coin operator=(const Coin &);
    void toss();
    string getSideUp();
};
class Temperature{
private:
    int temperature;
public:
    Temperature();
    Temperature(int);
    ~Temperature();
    Temperature(const Temperature &);
    Temperature operator=(const Temperature &);
    void setTemperature(int);
    int getTemperature();
    bool isEthylFreezing();
    bool isEthylBoiling();
    bool isOxygenFreezing();
    bool isOxygenBoiling();
    bool isWaterFreezing();
    bool isWaterBoiling();
};
class RetailItem{
private:
    string description;
    int unitsOnHand;
    float price;
public:
    RetailItem();
    RetailItem(string);
    RetailItem(string,int);
    RetailItem(string,int,float);
    ~RetailItem();
    RetailItem(const RetailItem &);
    RetailItem operator=(const RetailItem &);
    string getDescription();
    int getUnitsOnHand();
    float getPrice();
    void setDescription(string);
    void setUnitsOnHand(int);
    void setPrice(float);
};
class CashRegister{
private:
    int inventorySize;
    RetailItem* inventory;
    int location;
    string item;
    int quantity;
    float cost;
    float unitPrice;
    double subTotal;
    double tax;
    double total;
public:
    CashRegister();
    CashRegister(RetailItem*,int);
    CashRegister(RetailItem*,int,int);
    CashRegister(RetailItem*,int,int,int);
    ~CashRegister();
    CashRegister(const CashRegister &);
    CashRegister operator=(const CashRegister &);
    void setPurchase(); //item,quantity and location
    void setItemCost(); //cost
    void setUnitPrice();    //unit price
    void setSubTotal(); //sub total
    void setTax();  //tax
    void setTotal();    //total
    void output();
    int getLocation();
    string getItem();
    int getQuantity();
    float getCost();
    float getUnitPrice();
    double getSubTotal();   
    double getTotal();
    double getTax();
    void decreaseUnitsOnHand();
    RetailItem& operator[](int loc);
    void isValidPos(int&);
    void isValidOver(int&);
};

void problemOne();
void problemTwo();
void problemThree();
void problemFour();
void problemFive();

int main(int argc, char** argv) {
    srand(time(0));
        int ans;
    string choice ;
    do{
        cout<<"Which Homework program would you like to run?(1-5)(0 to exit): ";
        cin>>ans;
        do{
            switch(ans){
                case 0:
                    cout<<"Exiting";
                    return 0;
                case 1:
                    problemOne();
                    cout<<endl;
                    break;
                case 2:
                    problemTwo();
                    cout<<endl;
                    break;
                case 3:
                    problemThree();
                    cout<<endl;
                    break;
                case 4:
                    problemFour();
                    cout<<endl;
                    break;
                case 5:
                    problemFive();
                    cout<<endl;
                    break;
                default:
                    cout<<"Error"<<endl;
                    break;
            }
            cout<<"Would you like to run the program again?(yes/no): ";
            cin>>choice;
        }while(choice=="Yes"||choice=="yes"||choice=="Y"||choice=="y");
    }while(ans!=0);
    return 0;
}

void problemOne(){
    cout<<"Problem 1"<<endl;
    int size;
    int num;
    cout<<"This program will help you make a number Array."<<endl;
    cout<<"Then it will find the highest number,lowest number and average."<<endl;
    cout<<"How many numbers should I store?: ";
    cin>>size;
    NumberArray array(size);
    for(int n=0;n<size;n++){
        cout<<"Enter a number: ";
        cin>>num;
        array.setNumber(num,n);
    }
    array.output();
}
void problemTwo(){
    cout<<"Problem 2"<<endl;
    int heads=0;
    int tails=0;
    int flips=20;
    Coin coin;
    
    cout<<"This program will flip a coin 20 times and display the results"<<endl;
    
    for(int n=0;n<flips;n++){
        cout<<coin.getSideUp()<<endl;
        if(coin.getSideUp()=="Heads"){
            heads++;
        }else if(coin.getSideUp()=="Tails"){
            tails++;
        }
        coin.toss();
    }
    cout<<"Times coin landed heads: "<<heads<<endl;
    cout<<"Times coin landed tails: "<<tails<<endl;
}
void problemThree(){
    cout<<"Problem 3"<<endl;
    int num;
    Temperature temperature;
    cout<<"Enter a temperature and this program will tell you \n"
            "if ethyl alcohol,oxygen and water boil or freeze in it"<<endl;
    cin>>num;
    temperature.setTemperature(num);
    if(temperature.isEthylBoiling()){
        cout<<"Ethyl alcohol will boil at this temperature."<<endl;
    }
    if(temperature.isEthylFreezing()){
        cout<<"Ethyl alcohol will freeze at this temperature."<<endl;
    }
    if(temperature.isOxygenBoiling()){
        cout<<"Oxygen will boil at this temperature."<<endl;
    }
    if(temperature.isOxygenFreezing()){
        cout<<"Oxygen will freeze at this temperature."<<endl;
    }
    if(temperature.isWaterBoiling()){
        cout<<"Water will boil at this temperature."<<endl;
    }
    if(temperature.isWaterFreezing()){
        cout<<"Water will freeze at this temperature."<<endl;
    }
}
void problemFour(){
    cout<<"Problem 4"<<endl;
    int size=3;
    RetailItem* items=new RetailItem[size];
    items[0].setDescription("Jacket");
    items[0].setUnitsOnHand(12);
    items[0].setPrice(59.95);
    items[1].setDescription("Designer Jeans");
    items[1].setUnitsOnHand(40);
    items[1].setPrice(34.95);
    items[2].setDescription("Shirt");
    items[2].setUnitsOnHand(20);
    items[2].setPrice(24.95);
    CashRegister transaction(items,size);
    transaction.setPurchase();
    transaction.output();
}
void problemFive(){
    
}

NumberArray::NumberArray(){
    size=1;
    numbers=new float[this->size];
}
NumberArray::NumberArray(int size){
    this->size=size;
    numbers=new float[this->size];
}
NumberArray::~NumberArray(){
    delete []numbers;
    numbers=NULL;
}
NumberArray::NumberArray(const NumberArray &array){
    this->size=size;
    this->numbers=new float[this->size];
    for(int n=0;n<this->size;n++){
        this->numbers[n]=array.numbers[n];
    }
}
NumberArray NumberArray::operator=(const NumberArray &array){
    if(this==&array){
       return *this; 
    }
    delete[] this->numbers;
    this->size=size;
    this->numbers=new float[this->size];
    for(int n=0;n<this->size;n++){
        this->numbers[n]=numbers[n];
    }
    return *this;
}
void NumberArray::setNumber(float num,int loc){
    numbers[loc]=num;
}
int NumberArray::getSize(){
    return size;
}
float NumberArray::getNumber(int loc){
    return numbers[loc];
}
float NumberArray::getHighestNumber(){
    int temp=0;
    for(int n=0;n<size-1;n++){
        if(numbers[n]>=numbers[n+1]){
            if(numbers[n]>=numbers[temp]){
                temp=n;
            }
        }
    }
    return numbers[temp];
}
float NumberArray::getLowestNumber(){
    int temp=0;
    for(int n=0;n<size-1;n++){
        if(numbers[n]<=numbers[n+1]){
            if(numbers[n]<=numbers[temp]){
                temp=n;
            }
        }
    }
    return numbers[temp];
}
float NumberArray::getAverage(){
    int average=0;
    for(int n=0;n<size;n++){
        average+=numbers[n];
    }
    average=average/size;
    return average;
}
void NumberArray::output(){
    cout<<"Number Array"<<endl;
    for(int n=0;n<size;n++){
        cout<<n+1<<": "<<getNumber(n)<<endl;
    }
    cout<<"Highest Number: "<<getHighestNumber()<<endl;
    cout<<"Lowest Number: "<<getLowestNumber()<<endl;
    cout<<"Average: "<<getAverage()<<endl;
}

Coin::Coin(){
    toss();
}
Coin::~Coin(){
    
}
Coin::Coin(const Coin &coin){
    this->sideUp=coin.sideUp;
}
Coin Coin::operator=(const Coin &coin){
    this->sideUp=coin.sideUp;
}
void Coin::toss(){
    int temp;
    temp=rand()%2;
    switch(temp){
        case 0:
            sideUp="Heads";
            break;
        case 1:
            sideUp="Tails";
            break;
        default:
            sideUp="Error";
            break;
    }
}
string Coin::getSideUp(){
    return sideUp;
}

Temperature::Temperature(){
    temperature=0;
}
Temperature::Temperature(int temperature){
    this->temperature=temperature;
}
Temperature::~Temperature(){}
Temperature::Temperature(const Temperature &temperature){
    this->temperature=temperature.temperature;
}
Temperature Temperature::operator=(const Temperature &temperature){
    this->temperature=temperature.temperature;
}
void Temperature::setTemperature(int temperature){
    this->temperature=temperature;
}
int Temperature::getTemperature(){
    return temperature;
}
bool Temperature::isEthylFreezing(){
    if(temperature<=-173){
        return true;
    }
    else if(temperature>-173){
        return false;
    }
}
bool Temperature::isEthylBoiling(){
    if(temperature>=172){
        return true;
    }
    else if(temperature<172){
        return false;
    }
}
bool Temperature::isOxygenFreezing(){
    if(temperature<=-362){
        return true;
    }
    else if(temperature>-362){
        return false;
    }
}
bool Temperature::isOxygenBoiling(){
    if(temperature>=-306){
        return true;
    }
    else if(temperature<-306){
        return false;
    }
}
bool Temperature::isWaterFreezing(){
    if(temperature<=32){
        return true;
    }
    else if(temperature>32){
        return false;
    }
}
bool Temperature::isWaterBoiling(){
    if(temperature>=212){
        return true;
    }
    else if(temperature<212){
        return false;
    }
}

RetailItem::RetailItem(){
    description="";
    unitsOnHand=0;
    price=0;
}
RetailItem::~RetailItem(){}
RetailItem::RetailItem(string description){
    this->description=description;
    unitsOnHand=0;
    price=0;
}
RetailItem::RetailItem(string description, int unitsOnHand){
    this->description=description;
    this->unitsOnHand=unitsOnHand;
    price=0;
}
RetailItem::RetailItem(string description, int unitsOnHand, float price){
    this->description=description;
    this->unitsOnHand=unitsOnHand;
    this->price=price;
}
RetailItem::RetailItem(const RetailItem &){
    this->description=description;
    this->unitsOnHand=unitsOnHand;
    this->price=price;
}
RetailItem RetailItem::operator=(const RetailItem &){
    this->description=description;
    this->unitsOnHand=unitsOnHand;
    this->price=price;
}
string RetailItem::getDescription(){
    return description;
}
int RetailItem::getUnitsOnHand(){
    return unitsOnHand;
}
float RetailItem::getPrice(){
    return price;
}
void RetailItem::setDescription(string description){
    this->description=description;
}
void RetailItem::setUnitsOnHand(int unitsOnHand){
    this->unitsOnHand=unitsOnHand;
}
void RetailItem::setPrice(float price){
    this->price=price;
}

CashRegister::CashRegister(){
    inventorySize=1;
    inventory=new RetailItem[inventorySize];
    location=0;
    item="";
    quantity=0;
    cost=0;
    unitPrice=0;
    subTotal=0;
    tax=0;
    total=0;
}
CashRegister::CashRegister(RetailItem* inventory,int inventorySize){
    this->inventorySize=inventorySize;
    this->inventory=new RetailItem[this->inventorySize];
    for(int n=0;n<this->inventorySize;n++){
        this->inventory[n].setDescription(inventory[n].getDescription());
        this->inventory[n].setPrice(inventory[n].getPrice());
        this->inventory[n].setUnitsOnHand(inventory[n].getUnitsOnHand());
    }
    location=0;
    item=inventory[location].getDescription();
    quantity=0;
    cost=0;
    unitPrice=0;
    subTotal=0;
    tax=0;
    total=0;
}
CashRegister::CashRegister(RetailItem* inventory,int inventorySize,int location){
    this->inventorySize=inventorySize;
    this->inventory=new RetailItem[this->inventorySize];
    for(int n=0;n<this->inventorySize;n++){
        this->inventory[n].setDescription(inventory[n].getDescription());
        this->inventory[n].setPrice(inventory[n].getPrice());
        this->inventory[n].setUnitsOnHand(inventory[n].getUnitsOnHand());
    }
    this->location=location;
    item=inventory[location].getDescription();
    quantity=0;
    cost=0;
    unitPrice=0;
    subTotal=0;
    tax=0;
    total=0;
}
CashRegister::CashRegister(RetailItem* inventory,int inventorySize,int location,int quantity){
    this->inventorySize=inventorySize;
    this->inventory=new RetailItem[this->inventorySize];
    for(int n=0;n<this->inventorySize;n++){
        this->inventory[n].setDescription(inventory[n].getDescription());
        this->inventory[n].setPrice(inventory[n].getPrice());
        this->inventory[n].setUnitsOnHand(inventory[n].getUnitsOnHand());
    }
    this->location=location;
    item=inventory[location].getDescription();
    this->quantity=quantity;
    cost=getCost();
    unitPrice=getUnitPrice();
    subTotal=getSubTotal();
    tax=getTax();
    total=getTotal();
}
CashRegister::~CashRegister(){
    delete[] inventory;
    inventory=NULL;
}
CashRegister::CashRegister(const CashRegister &cashRegister){
    this->inventorySize=cashRegister.inventorySize;
    this->inventory=new RetailItem[this->inventorySize];
    for(int n=0;n<this->inventorySize;n++){
        this->inventory[n]=cashRegister.inventory[n];
    }
    this->location=cashRegister.location;
    this->item=cashRegister.item;
    this->quantity=cashRegister.quantity;
    this->cost=cashRegister.cost;
    this->unitPrice=cashRegister.unitPrice;
    this->subTotal=cashRegister.subTotal;
    this->tax=cashRegister.tax;
    this->total=cashRegister.total;
}
CashRegister CashRegister::operator=(const CashRegister &cashRegister){
    this->inventorySize=cashRegister.inventorySize;
    delete[] inventory;
    this->inventory=new RetailItem[this->inventorySize];
    for(int n=0;n<this->inventorySize;n++){
        this->inventory[n]=cashRegister.inventory[n];
    }
    this->location=cashRegister.location;
    this->item=cashRegister.item;
    this->quantity=cashRegister.quantity;
    this->cost=cashRegister.cost;
    this->unitPrice=cashRegister.unitPrice;
    this->subTotal=cashRegister.subTotal;
    this->tax=cashRegister.tax;
    this->total=cashRegister.total;
}
void CashRegister::setPurchase(){
    string item="";
    char* temp =new char[100];
    int quantity;
    cout<<"Please type the item you would like to buy:";
    cin.ignore();
    cin.getline(temp,100);
    for(int n=0;n<strlen(temp);n++){
        item+=temp[n];
    }
    for(int n=0;n<inventorySize;n++){
        if(item.compare(inventory[n].getDescription())==0){
            this->item=inventory[n].getDescription();
            location=n;
        }
    }
    cout<<"How many would you like to buy?:";
    cin>>quantity;
    isValidPos(quantity);
    isValidOver(quantity);
    this->quantity=quantity;
    setItemCost();
    setUnitPrice();
    setSubTotal();
    setTax();
    setTotal();
    delete[] temp;
    temp=NULL;
}
void CashRegister::setItemCost(){
    cost=inventory[location].getPrice();
}
void CashRegister::setUnitPrice(){
    unitPrice=cost+(cost*.3);
}
void CashRegister::setSubTotal(){
    subTotal=unitPrice*quantity;
}
void CashRegister::setTax(){
    tax=subTotal*0.06;
}
void CashRegister::setTotal(){
    total=subTotal+tax;
}
int CashRegister::getLocation(){
    return location;
}
string CashRegister::getItem(){
    return item;
}
int CashRegister::getQuantity(){
    return quantity;
}
float CashRegister::getCost(){
    return cost;
}
float CashRegister::getUnitPrice(){
    return unitPrice;
}
double CashRegister::getSubTotal(){
    return subTotal;
}
double CashRegister::getTotal(){
    return total;
}
double CashRegister::getTax(){
    return tax;
}
void CashRegister::decreaseUnitsOnHand(){
    inventory[location].setUnitsOnHand(inventory[location].getUnitsOnHand()-quantity);
}
void CashRegister::output(){
    cout<<"Subtotal: $"<<fixed<<setprecision(2)<<getSubTotal()<<endl;
    cout<<"Tax:      $"<<fixed<<setprecision(2)<<getTax()<<endl;
    cout<<"Total:    $"<<fixed<<setprecision(2)<<getTotal()<<endl;
}
RetailItem& CashRegister::operator[](int loc){
    return inventory[loc];
}
void CashRegister::isValidPos(int &val){
    while(val<0){
        cout<<"Please enter a positive number"<<endl;
        cout<<"How many would you like to buy?:";
        cin>>val;
    }
}
void CashRegister::isValidOver(int &val){
    while(val>inventory[location].getUnitsOnHand()){
        cout<<"Sorry, but there is not that many units."<<endl;
        cout<<"We currently have "<<inventory[location].getUnitsOnHand()<<" in stock"<<endl;
        cout<<"How many would you like to buy?:";
        cin.ignore();
        cin>>val;
        isValidPos(val);
    }
}
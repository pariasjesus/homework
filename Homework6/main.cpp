/* 
 * File:   main.cpp
 * Author: Jesus Parias-Castillo
 * Purpose Homework 6
 * Created on May 5, 2019, 8:00 PM
 */
#include <cstdlib>
#include <iostream>
#include <ctime>
#include <string.h>
#include <iomanip>

using namespace std;

string dayToDate(int);
int dateToDay(string);
string intToString(int);
void isDayValid(string,int &);
void isMonthValid(string &);

class DayOfTheYear{
private:
    int dayOfYear;
    string date;
public:
    DayOfTheYear();
    DayOfTheYear(int);
    DayOfTheYear(string,int);
    ~DayOfTheYear();
    DayOfTheYear(const DayOfTheYear &);
    DayOfTheYear operator=(const DayOfTheYear &);
    DayOfTheYear operator++();
    DayOfTheYear operator--();
    DayOfTheYear operator++(int);
    DayOfTheYear operator--(int);
    void setDate(int);
    string getDate();
    int getDayOfYear();
    void print();
};
class FeetInches{
private:
    int inches;
    int feet;
    void simplify();    //makes sure inches is converted to feet properly
public:
    FeetInches();
    FeetInches(int);
    FeetInches(int,int);
    ~FeetInches();
    FeetInches(const FeetInches &);
    FeetInches operator=(const FeetInches &);
    FeetInches operator++();    //pre-fix
    FeetInches operator++(int);    //post-fix
    FeetInches operator--();    //pre-fix
    FeetInches operator--(int);
    bool operator<=(const FeetInches &);
    bool operator>=(const FeetInches &);
    bool operator!=(const FeetInches &);
    int getInches();
    int getFeet();
    void setInches(int);
    void setFeet(int);
    void output();
    FeetInches operator*(const FeetInches &);
};

void problemOne();
void problemTwo();
void problemThree();

int main(int argc, char** argv) {
    int ans;
    string choice ;
    do{
        cout<<"Which Homework program would you like to run?(1-3)(0 to exit): ";
        cin>>ans;
        do{
            switch(ans){
                case 0:
                    cout<<"Exiting";
                    return 0;
                case 1:
                    problemOne();
                    cout<<endl;
                    break;
                case 2:
                    problemTwo();
                    cout<<endl;
                    break;
                case 3:
                    problemThree();
                    cout<<endl;
                    break;
                default:
                    cout<<"Error"<<endl;
                    break;
            }
            cout<<"Would you like to run the program again?(yes/no): ";
            cin>>choice;
        }while(choice=="Yes"||choice=="yes"||choice=="Y"||choice=="y");
    }while(ans!=0);
    return 0;
}

void problemOne(){
    //day of the year 14.3
    cout<<"Problem 1"<<endl;
    char* date=new char[100];
    char* tempDay=new char[100];
    string month="";
    int day;
    cout<<"Enter the current month and day of the month: ";
    cin.ignore();
    cin.getline(date,100);
    for(int n=0;n<strlen(date);n++){
        if(date[n]!=' '){
            month+=date[n];
        }if(date[n]==' '){
            n=strlen(date);
        }
    }
    for(int n=0;n<strlen(date)-month.length();n++){
        tempDay[n]=date[month.length()+n];
    }
    day=atoi(tempDay);
    isMonthValid(month);
    isDayValid(month,day);
    
    DayOfTheYear dayOfTheYear(month,day);
    cout<<"Today"<<endl;
    dayOfTheYear.print();
    dayOfTheYear--;
    cout<<"Yesterday"<<endl;
    dayOfTheYear.print();
    dayOfTheYear++;
    dayOfTheYear++;
    cout<<"Tomorrow"<<endl;
    dayOfTheYear.print();
    
    delete[] date;
    date=NULL;
    delete[] tempDay;
    tempDay=NULL;
}
void problemTwo(){
    cout<<"Problem 2"<<endl;
    FeetInches m1(9,4);
    FeetInches m2(1,3);
    FeetInches m3(1,5);
    FeetInches m4(1,1);
    cout<<"m1 ";m1.output();
    cout<<"m2 ";m2.output();
    cout<<"m3 ";m3.output();
    cout<<"m4 ";m4.output();
    
    if(m1>=m2){
        cout<<"m1>=m2"<<endl;
    }
    if(m2>=m3){
        cout<<"m2>=m3"<<endl;
    }
    if(m2<=m3){
        cout<<"m2<=m3"<<endl;
    }
    if(m1!=m3){
        cout<<"m1!=m3"<<endl;
    }
    if(m2!=m3){
        cout<<"m2!=m3"<<endl;
    }
    m4=m1*m2;
    cout<<"m4=m1*m2 => ";m4.output();
}
void problemThree(){
    cout<<"Problem 2"<<endl;
    FeetInches m1(9,4);
    FeetInches m2(1,3);
    FeetInches m3(1,5);
    FeetInches m4(1,1);
    cout<<"m1 ";m1.output();
    cout<<"m2 ";m2.output();
    cout<<"m3 ";m3.output();
    cout<<"m4 ";m4.output();
    
    if(m1>=m2){
        cout<<"m1>=m2"<<endl;
    }
    if(m2>=m3){
        cout<<"m2>=m3"<<endl;
    }
    if(m2<=m3){
        cout<<"m2<=m3"<<endl;
    }
    if(m1!=m3){
        cout<<"m1!=m3"<<endl;
    }
    if(m2!=m3){
        cout<<"m2!=m3"<<endl;
    }
    m4=m1*m2;
    cout<<"m4=m1*m2 => ";m4.output();
}

DayOfTheYear::DayOfTheYear(){
    dayOfYear=1;
    date="January 1";
}
DayOfTheYear::DayOfTheYear(int dayOfYear){
    this->dayOfYear=dayOfYear;
    date=dayToDate(this->dayOfYear);
}
DayOfTheYear::DayOfTheYear(string month,int day){
    dayOfYear=dateToDay(month)+day;
    date=month+intToString(day);;
}
DayOfTheYear::~DayOfTheYear(){
    
}
DayOfTheYear::DayOfTheYear(const DayOfTheYear &dayOfTheYear){
    this->dayOfYear=dayOfTheYear.dayOfYear;
    this->date=dayOfTheYear.date;
}
DayOfTheYear DayOfTheYear::operator=(const DayOfTheYear &dayOfTheYear){
    this->dayOfYear=dayOfTheYear.dayOfYear;
    this->date=dayOfTheYear.date;
}
DayOfTheYear DayOfTheYear::operator++(){
    dayOfYear++;
    if(dayOfYear>365){
        dayOfYear=1;
    }
    date=dayToDate(dayOfYear);
}
DayOfTheYear DayOfTheYear::operator--(){
    dayOfYear--;
    if(dayOfYear<1){
        dayOfYear=365;
    }
    date=dayToDate(dayOfYear);
}
DayOfTheYear DayOfTheYear::operator++(int){
    DayOfTheYear temp(*this);
    dayOfYear++;
    if(dayOfYear>365){
        dayOfYear=1;
    }
    date=dayToDate(dayOfYear);
    return temp;
}
DayOfTheYear DayOfTheYear::operator--(int){
    DayOfTheYear temp(*this);
    dayOfYear--;
    if(dayOfYear<1){
        dayOfYear=365;
    }
    date=dayToDate(dayOfYear);
    return temp;
}
string dayToDate(int day){
    string date="";
    if(day<=31){
        date="January " + intToString(day);
    }else if(day>31 && day<=31+28){
        date="February "+ intToString(day-31);
    }else if(day>31+28 && day<=31+28+31){
        date="March "+ intToString(day-31-28);
    }else if(day>31+28+31 && day<=31+28+31+30){
        date="April "+ intToString(day-31-28-31);
    }else if(day>31+28+31+30 && day<=31+28+31+30+31){
        date="May "+ intToString(day-31-28-31-30);
    }else if(day>31+28+31+30+31  && day<=31+28+31+30+31+30){
        date="June "+ intToString(day-31-28-31-30-31);
    }else if(day>31+28+31+30+31+30 && day<=31+28+31+30+31+30+31){
        date="July "+ intToString(day-31-28-31-30-31-30);
    }else if(day>31+28+31+30+31+30+31 && day<=31+28+31+30+31+30+31+31){
        date="August "+ intToString(day-31-28-31-30-31-30-31);
    }else if(day>31+28+31+30+31+30+31+31 && day<=31+28+31+30+31+30+31+31+30){
        date="September "+ intToString(day-31-28-31-30-31-30-31-31);
    }else if(day>31+28+31+30+31+30+31+31+30 && day<=31+28+31+30+31+30+31+31+30+31){
        date="October "+ intToString(day-31-28-31-30-31-30-31-31-30);
    }else if(day>31+28+31+30+31+30+31+31+30+31 && day<=31+28+31+30+31+30+31+31+30+31+30){
        date="November "+ intToString(day-31-28-31-30-31-30-31-31-30-31);
    }else if(day>31+28+31+30+31+30+31+31+30+31+30 && day<=31+28+31+30+31+30+31+31+30+31+30+31){
        date="December "+ intToString(day-31-28-31-30-31-30-31-31-30-31-30);
    }
    return date;
}
int dateToDay(string month){
    int day=0;
    if(month=="January"){
        day+=0;
    }else if(month=="February"){
        day+=31;
    }else if(month=="March"){
        day+=31+28;
    }else if(month=="April"){
        day+=31+28+31;
    }else if(month=="May"){
        day+=31+28+31+30;
    }else if(month=="June"){
        day+=31+28+31+30+31;
    }else if(month=="July"){
        day+=31+28+31+30+31+30;
    }else if(month=="August"){
        day+=31+28+31+30+31+30+31;
    }else if(month=="September"){
        day+=31+28+31+30+31+30+31+31;
    }else if(month=="October"){
        day+=31+28+31+30+31+30+31+31+30;
    }else if(month=="November"){
        day+=31+28+31+30+31+30+31+31+30+31;
    }else if(month=="December"){
        day+=31+28+31+30+31+30+31+31+30+31+30;
    }
    return day;
}
void DayOfTheYear::setDate(int day){
    date = dayToDate(day);
}
string DayOfTheYear::getDate(){
    return date;
}
int DayOfTheYear::getDayOfYear(){
    return dayOfYear;
}
void DayOfTheYear::print(){
    cout<<"Day of the year: "<<getDayOfYear()<<endl;
}
string intToString(int day){
    string date="";
    int tens;
    int ones;
    tens= day/10;
    ones= day%10;
    if(tens!=0){
        date+=static_cast<unsigned char>(tens)+48;
    }
    date+=static_cast<unsigned char>(ones)+48;
    return date;
}
void isDayValid(string month, int &day){
    if(month=="January"||month=="March"||month=="May"||month=="July"||month=="August"||month=="October"||month=="December"){
        while(day<1||day>31){
        cout<<"Please enter a valid day(1-31)"<<endl;
        cin>>day;
        }
    }else if(month=="April"||month=="June"||month=="September"||month=="November"){
        while(day<1||day>30){
        cout<<"Please enter a valid day(1-30)"<<endl;
        cin>>day;
        }
    }else if(month=="February"){
        while(day<1||day>28){
        cout<<"Please enter a valid day(1-28)"<<endl;
        cin>>day;
        }
    }
}
void isMonthValid(string &month){
    while(!(month=="January"||month=="March"||month=="May"||month=="July"||month=="August"||month=="October"||month=="December"||month=="April"||month=="June"||month=="September"||month=="November"||month=="February")){
        cout<<"January\n"
                "February\n"
                "March\n"
                "April\n"
                "May\n"
                "June\n"
                "July\n"
                "August\n"
                "September\n"
                "October\n"
                "November\n"
                "December\n"
                "Enter a valid month:";
        cin>>month;
    }
}

FeetInches::FeetInches(){
    inches=0;
    feet=0;
}
FeetInches::FeetInches(int feet){
    inches=0;
    this->feet=feet;
}
FeetInches::FeetInches(int feet,int inches){
    this->inches=inches;
    this->feet=feet;
    this->simplify();
}
FeetInches::FeetInches(const FeetInches &feetInches){
    this->inches=feetInches.inches;
    this->feet=feetInches.feet;
}
FeetInches::~FeetInches(){
}
FeetInches FeetInches::operator=(const FeetInches &feetInches){
    this->inches=feetInches.inches;
    this->feet=feetInches.feet;
}
FeetInches FeetInches::operator++(){
    //increment is first
    inches++;
    simplify();
    return *this;
}
FeetInches FeetInches::operator++(int){
    //increment comes after
    FeetInches temp(*this);    //copy constructor
    inches++;
    simplify();
    return temp;
}
FeetInches FeetInches::operator--(){
    //increment is first
    inches--;
    simplify();
    return *this;
}
FeetInches FeetInches::operator--(int){
    //increment comes after
    FeetInches temp(*this);    //copy constructor
    inches--;
    simplify();
    return temp;
}
void FeetInches::simplify(){
    while(inches>=12){
        inches=inches-12;
        feet++;
    }
}
bool FeetInches::operator<=(const FeetInches &feetInches){
    if(this->feet<=feetInches.feet){
        if(this->feet<feetInches.feet){
            return true;
        }else if(this->feet==feetInches.feet && this->inches<=feetInches.inches){
            return true;
        }
    }else
            return false;
}
bool FeetInches::operator>=(const FeetInches &feetInches){
    
        if(this->feet>feetInches.feet){
            return true;
        }else if(this->feet==feetInches.feet && this->inches>=feetInches.inches){
            return true;
        }
    else
            return false;
}
bool FeetInches::operator!=(const FeetInches &feetInches){
        if(this->feet!=feetInches.feet){
            return true;
        }else if(this->feet==feetInches.feet && this->inches!=feetInches.inches){
            return true;
        }else
            return false;
}
int FeetInches::getInches(){
    return inches;
}
int FeetInches::getFeet(){
    return feet;
}
void FeetInches::setInches(int inches){
    this->inches=inches;
}
void FeetInches::setFeet(int feet){
    this->feet=feet;
}
void FeetInches::output(){
    cout<<feet<<"ft "<<inches<<"in"<<endl;
}
FeetInches FeetInches::operator*(const FeetInches &feetInches){
    FeetInches temp;
    temp.inches=this->inches*feetInches.inches;
    temp.feet=this->feet*feetInches.feet;
    temp.simplify();
    return temp;
}
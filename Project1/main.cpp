/* 
 * File:   main.cpp
 * Author: Jesus Parias-Castillo
 * Purpose Project1
 * Created on May 11, 2019, 10:28 PM
 */

#include <cstdlib>
#include <iostream>
#include <time.h>
#include <string.h>
#include <iomanip>
#include <fstream>

using namespace std;

enum PotMaterial{
    clay,
    plastic,
    fabric,
    glass,
    ceramic,
    wood,
    metal,
    potMaterials
};
enum windStrength{
    none,
    draft,
    breeze,
    windy,
    strong,
    hazard
};

class Journal{
private:
    unsigned const short entrySize = 1000;
    unsigned const short dateSize = 100;
    
    char* entry= new char[entrySize];
    char* date= new char[dateSize];
public:
    Journal();
    Journal(char*); //given date
    Journal(char*,char*); //given date then entry
    Journal(const Journal &);
    Journal operator=(const Journal &);
    ~Journal();
    void setEntry(char*);
    void setDate(char*);
    char* getEntry();
    char* getDate();
};
class Pot{
    
};
class GrowingInformation{

};
class GrowingMedium{
private:
    string name;
    unsigned short totalComponents;
    string** component;     //2d array, 1 for component and 2 for parts/percentage
public:
    GrowingMedium();
    GrowingMedium(string);
    GrowingMedium(string,unsigned short);
    GrowingMedium(string,unsigned short,string**);
    ~GrowingMedium();
    GrowingMedium(const GrowingMedium &);
    GrowingMedium operator=(const GrowingMedium &);
    string getName();
    unsigned short getTotalComponents();
    string** getComponents();
    void setName(string);
    void setTotalComponents(unsigned short);
    void setComponents(string**);
    void assignComponent(string,unsigned short);
    void assignPart(string,unsigned short);
    string getComponent(unsigned short);
    string getPart(unsigned short);
    void menu();
    void output();
};
class Plant{
private:
    unsigned const short genSize = 100;
    unsigned const short descriptionSize = 1000;
    unsigned const short journalSize = 1000;
    
    char* name= new char[genSize];
    char* type= new char[genSize];
    char* description= new char[descriptionSize];
    unsigned short lifeSpan;
    unsigned short age;
    string sex;
    short pot;
    GrowingMedium soil;
    GrowingInformation whatIs;
    Journal* journal= new Journal[journalSize];
    
public:
    Plant();
    Plant(const Plant &);
    Plant operator= (const Plant &);
    Plant operator* (const Plant &);
    bool operator== (const Plant &);
    bool operator> (const Plant &);
    bool operator< (const Plant &);
    bool operator>= (const Plant &);
    bool operator<= (const Plant &);
    ~Plant();
    string getName(string);
    char* getName();
};
class Garden{
private:
    unsigned short gardenCapacity= 1000;
    unsigned short typeSize= 1000;
    unsigned short nameSize= 1000;
    
    char* name= new char[nameSize];
    char* type= new char[typeSize];
    Plant* plant = new Plant[gardenCapacity];
    unsigned short gardenSize;
    unsigned short surfaceArea;
    GrowingMedium growingMedium;
    unsigned short hoursOfLight;
    unsigned short maxTemperature;
    unsigned short minTemperature;
    unsigned short maxHumidity;
    unsigned short minHumidity;
    short MaxWindSpeeds;
    
public:
    string getName();
    string getTypeString();
    short getGardenSize();
    string getPlantName();
};

void mainMenu(GrowingMedium,unsigned short,char*);
void mainMenuValidation(char*,unsigned short);
void myGardenMenu();
void plantSpeciesRecordMenu();
void growingInstructionsMenu();
void growingMediumsMenu(GrowingMedium,unsigned short,char*);
void growingMediumMenuValidation(char*,unsigned short);
void helpMenu(GrowingMedium,unsigned short,char*);
void initializeGardens(Garden*,unsigned short &);
void initializePlants(Plant*,unsigned short &);
void initializeGrowingInformation(GrowingInformation*,unsigned short &);
void initializeGrowingMedium(GrowingMedium,unsigned short &);

int main(){
    const unsigned short choiceSize=30;
    unsigned short gardens;
    unsigned short plantsSoFar;
    unsigned short numberOfHowTos;
    unsigned short mediumRecipes;
    Garden* myGardens;
    Plant* myPlantsSoFar;
    GrowingInformation* howToFiles;
    GrowingMedium growingMedium;
    char* choice= new char[choiceSize];
    
    initializeGardens(myGardens,gardens);
    initializePlants(myPlantsSoFar,plantsSoFar);
    initializeGrowingInformation(howToFiles,numberOfHowTos);
    initializeGrowingMedium(growingMedium,mediumRecipes);
    cout<<gardens<<"done"<<endl;                                  //test
    cout<<plantsSoFar<<"done"<<endl;                                  //test
    cout<<numberOfHowTos<<"done"<<endl;  
    cout<<mediumRecipes<<"done"<<endl;  //test
    mainMenu(growingMedium,mediumRecipes,choice);
    
    delete[] choice;
    choice=NULL;
    return 0;
}
void growingMediumsMenu(GrowingMedium mediums,unsigned short size,char* choice){
    cout<<"Growing Mediums"<<endl;
    //for(int n=0;n<mediumCount;n++){
        mediums.getName();
    //}
    cout<<"[E] Edit Growing Mediums"<<endl;
    cout<<"[B] <--Back"<<endl;
    
    cin>>choice;
    growingMediumMenuValidation(choice,size);
    
    if(isdigit(choice[0])){
        cout<<choice;
        mediums.output();
    }
    if(choice[0]=='B'||choice[0]=='b'){
        cout<<"Backing"<<endl;
    }
}
void growingMediumMenuValidation(char* choice, unsigned short max){
    bool choiceValidation;
    if(choice[0]=='B'||choice[0]=='b'){
        choiceValidation=true;
    }
    if(choice[0]=='E'||choice[0]=='e'){
        choiceValidation=true;
    }
    if(atoi(choice)>0&&atoi(choice)<=max){
        choiceValidation=true;
    }
    if(choiceValidation==false){
        cout<<"Please enter a valid option: ";
        cin>>choice;
        growingMediumMenuValidation(choice,max);
    }
}
void EditGrowingMediumsMenu(){
    cout<<"[A] Add Growing Medium"<<endl;
    cout<<"[D] Delete Growing Medium"<<endl;
    cout<<"[S] Sort Mediums"<<endl;
    cout<<"[B] <--Back"<<endl;
}
void initializeGrowingMedium(GrowingMedium growingMedium,unsigned short &mediumRecipes){
    cout<<"     Bagging Soil..."<<endl;
    const unsigned short maxRecipes=50;
    unsigned short x;
    ifstream inFile;
    fstream mediumFile;
    
    //get number of recipes
    inFile.open("numberOfMediumRecipes.txt");
    //check for error
    if(inFile.fail()){
        cerr << "Error Opening File1"<<endl;
        exit(1);
    }
    inFile>>x;
    mediumRecipes=x;
    cout<<mediumRecipes<<endl;
    inFile.close();
    if(mediumRecipes>maxRecipes){
        cout<<"I read you have over "<<maxRecipes<<" medium recipes."<<endl;
        cout<<"I will only include the first "<<maxRecipes<<endl;
        cout<<"recipes for viewing."<<endl;
        mediumRecipes=maxRecipes;
    }
    
    //get recipes
    mediumFile.open("mediumRecipes.txt");
    if(mediumFile.fail()){
        cerr << "Error Opening File2"<<endl;
        exit(1);
    }
    string* temp= new string[mediumRecipes];
    //read file until reached end
    for(int line=0;line<mediumRecipes;line++){
        if(line==0){
                mediumFile>>temp[line];
                growingMedium.setName(temp[line]);
                cout<<growingMedium.getName()<<endl;
            }else if(line>0){
                mediumFile>>temp[line];
                growingMedium.assignComponent(temp[line],line);
                cout<<growingMedium.getComponent(line)<<" ";
                
                mediumFile>>temp[line];
                growingMedium.assignPart(temp[line],line);
                cout<<growingMedium.getPart(line)<<endl;
            }
    }
    mediumFile.close();
    
}
void initializeGrowingInformation(GrowingInformation* howToFiles,unsigned short &numberOfHowTos){
    const unsigned short maxHowToFiles=150;
    fstream in;
    in.open("numberOfHowToFiles.txt");
    for(int line=0;line<1;line++){
        in>>numberOfHowTos;
    }
    in.close();
    if(numberOfHowTos>maxHowToFiles){
        cout<<"I read you have over "<<maxHowToFiles<<" How-To files."<<endl;
        cout<<"I will only include the first "<<maxHowToFiles<<endl;
        cout<<"files for viewing."<<endl;
        numberOfHowTos=maxHowToFiles;
    }
    cout<<"     Organizing How-To files..."<<endl;
    howToFiles = new GrowingInformation[numberOfHowTos];
}
void initializePlants(Plant* myPlantsSoFar,unsigned short &plantsSoFar){
    const unsigned short maxPlants=150;
    fstream in;
    in.open("numberOfDifferentPlantsObtained.txt");
    for(int line=0;line<1;line++){
        in>>plantsSoFar;
    }
    in.close();
    if(plantsSoFar>maxPlants){
        cout<<"I read you have over "<<maxPlants<<" plants."<<endl;
        cout<<"I will only include the first "<<maxPlants<<endl;
        cout<<"plants for viewing."<<endl;
        plantsSoFar=maxPlants;
    }
    cout<<"     Counting the plants..."<<endl;
    myPlantsSoFar = new Plant[plantsSoFar];
}
void initializeGardens(Garden* myGarden,unsigned short &gardens){
    const unsigned short maxGardens=10;
    fstream in;
    in.open("numberOfGardens.txt");
    for(int line=0;line<1;line++){
        in>>gardens;
    }
    in.close();
    if(gardens>maxGardens){
        cout<<"I read you have over "<<maxGardens<<" gardens."<<endl;
        cout<<"I will only include the first "<<maxGardens<<endl;
        cout<<"gardens for viewing."<<endl;
        gardens=maxGardens;
    }
    cout<<"     Getting the gardens ready..."<<endl;
    myGarden = new Garden[gardens];
}
void mainMenu(GrowingMedium mediums,unsigned short mediumRecipes, char* choice){
    cout<<"Gardeners Journal"<<endl;
    cout<<"[1] My Gardens"<<endl;
    cout<<"[2] What I've Seen"<<endl;
    cout<<"[3] Growing Instructions"<<endl;
    cout<<"[4] Growing Mediums"<<endl;
    cout<<"[X] Exit App"<<endl;
    cout<<"For Help press H"<<endl;
    
    cin>>choice;
    mainMenuValidation(choice,4);
    
    if(choice[0]=='1'){
        //myGardenMenu();
    }
    if(choice[0]=='2'){
        //plantSpeciesRecordMenu();
    }
    if(choice[0]=='3'){
        //growingInstructionsMenu();
    }
    if(choice[0]=='4'){
        growingMediumsMenu(mediums,mediumRecipes,choice);
    }
    if(choice[0]=='X'||choice[0]=='x'){
        cout<<"Bye"<<endl;
    }
    if(choice[0]=='H'||choice[0]=='h'){
        helpMenu(mediums,mediumRecipes,choice);
    }
}
void mainMenuValidation(char* choice, unsigned short max){
    bool choiceValidation;
    if(choice[0]=='x'||choice[0]=='X'){
        choiceValidation=true;
    }
    if(choice[0]=='h'||choice[0]=='H'){
        choiceValidation=true;
    }
    if(atoi(choice)>0&&atoi(choice)<=max){
        choiceValidation=true;
    }
    if(choiceValidation==false){
        cout<<"Please enter a valid option: ";
        cin>>choice;
        mainMenuValidation(choice,max);
    }
}
void helpMenu(GrowingMedium growingMediums,unsigned short mediumRecipes,char* choice){
    cout<<"To navigate press the key on the left colum that is within the brackets [_] "<<endl;
    cout<<"You can always go back at any moment to the previous screen"<<endl;
    cout<<"If you would like to use a text file to input information, please"<<endl;
    cout<<"     modify the text file templates located within the home folders"<<endl;
    cout<<"[B] <--Back"<<endl;
    cin>>choice;
    mainMenu(growingMediums,mediumRecipes,choice);
}



void growingInstructionsMenu(){
    cout<<"How To Grow"<<endl;
    cout<<"[P] Pick a plant to grow"<<endl;
    cout<<"[E] Edit Growing Instructions"<<endl;
    cout<<"[B] <--Back"<<endl;
}
void EditGrowingInstructionsMenu(){
    cout<<"[A] Add Growing Instructions"<<endl;
    cout<<"[D] Delete Growing Instructions"<<endl;
    cout<<"[S] Sort Instructions"<<endl;
    cout<<"[B] <--Back"<<endl;
}

void plantSpeciesRecordMenu(Plant* plant,short numberOfPlants){
    cout<<"Plant Species Record"<<endl;
    for(int n=0;n<numberOfPlants;n++){
        cout<<"["<<n+1<<"]"<<plant[n].getName()<<endl;
    }cout<<endl;
    cout<<"[E] Edit Records"<<endl;
    cout<<"[B] <--Back"<<endl;
}
void editrecordsMenu(){
    cout<<"[A] Add a Plant to Record"<<endl;
    cout<<"[D] Get Rid of a Plant"<<endl;
    cout<<"[S] Sort Record"<<endl;
    cout<<"[B] <--Back"<<endl;
}

void myGardenMenu(Garden* gardens,short numberOfGardens){
    cout<<"My Gardens"<<endl;
    for(int n=0;n<numberOfGardens;n++){
        cout<<"["<<n+1<<"]"<<" "<<gardens[n].getName()<<endl;
    }cout<<endl;
    cout<<"[A] Change Gardens"<<endl;
    cout<<"[B] <-- Back"<<endl;
}
void gardenMenu(Garden garden){
    cout<<garden.getName()<<endl;
    cout<<"Type of Garden: "<<garden.getTypeString()<<endl;
    cout<<"Plants in Garden: "<<garden.getGardenSize()<<endl;
    for(int n=0;n<garden.getGardenSize();n++){
        cout<<"["<<n+1<<"]"<<" "<<garden.getPlantName()[n]<<endl;
    }
    cout<<"[E] Edit Garden"<<endl;
    cout<<"[B] <--Back"<<endl;
}
void editGardenMenu(){
    cout<<"[A] Add a Plant"<<endl;
    cout<<"[C] Clone a Plant"<<endl;
    cout<<"[X] Breed Plants"<<endl;
    cout<<"[D] Get Rid of a Plant"<<endl;
    cout<<"[S] Sort Plants"<<endl;
}
void changeGardensMenu(){
    cout<<"[A] Create a New Garden"<<endl;
    cout<<"[C] Copy a Garden"<<endl;
    cout<<"[X] Merge Gardens"<<endl;
    cout<<"[D] Delete a Garden"<<endl;
    cout<<"[S] Sort Gardens"<<endl;
}

GrowingMedium::GrowingMedium(){
    name="";
    totalComponents=0;
    component=new string*[2];
    for(int n=0;n<2;n++){
        component[n]=new string[totalComponents];
    }
}
GrowingMedium::GrowingMedium(string name){
    this->name=name;
    totalComponents=0;
    component=new string*[2];
    for(int n=0;n<2;n++){
        component[n]=new string[totalComponents];
    }
    for(int n=0;n<2;n++){
            for(int i=0;i<this->totalComponents;i++){
                component[n][i]="";
            }
        }
}
GrowingMedium::GrowingMedium(string name,unsigned short totalComponents){
    this->name=name;
    this->totalComponents=totalComponents;
    component=new string*[2];
    for(int n=0;n<2;n++){
        component[n]=new string[this->totalComponents];
    }
    for(int n=0;n<2;n++){
        for(int i=0;i<this->totalComponents;i++){
            this->component[n][i]="";
        }
    }
}
GrowingMedium::GrowingMedium(string name,unsigned short totalComponents, string** component){
    this->name=name;
    this->totalComponents=totalComponents;
    this->component=new string*[2];
    for(int n=0;n<2;n++){
        this->component[n]=new string[this->totalComponents];
    }
    for(int n=0;n<2;n++){
        for(int i=0;i<this->totalComponents;i++){
            this->component[n][i]=component[n][i];
        }
    }
}
GrowingMedium::~GrowingMedium(){
    for(int n=0;n<2;n++){
        delete[] component[n];
    }
    delete[] component;
    component=NULL;
}
GrowingMedium::GrowingMedium(const GrowingMedium &medium){
    if(this==&medium){
        this->name="";
        this->totalComponents=0;
        this->component=new string*[2];
        for(int n=0;n<2;n++){
            this->component[n]=new string[this->totalComponents];
        }
        for(int n=0;n<2;n++){
            for(int i=0;i<this->totalComponents;i++){
                this->component[n][i]="";
            }
        }
    }
    else{
        this->name=medium.name;
        this->totalComponents=medium.totalComponents;
        this->component=new string*[2];
        for(int n=0;n<2;n++){
            this->component[n]=new string[this->totalComponents];
        }
        for(int n=0;n<2;n++){
            for(int i=0;i<this->totalComponents;i++){
                this->component[n][i]=medium.component[n][i];
            }
        }
    }
}
GrowingMedium GrowingMedium::operator=(const GrowingMedium &medium){
    if(this==&medium){
        return *this;
    }
    else{
        this->name=medium.name;
        this->totalComponents=medium.totalComponents;
        this->component=new string*[2];
        for(int n=0;n<2;n++){
            this->component[n]=new string[this->totalComponents];
        }
        for(int n=0;n<2;n++){
            for(int i=0;i<this->totalComponents;i++){
                this->component[n][i]=medium.component[n][i];
            }
        }
    }
}
string GrowingMedium::getName(){
    return name;
}
unsigned short GrowingMedium::getTotalComponents(){
    return totalComponents;
}
string** GrowingMedium::getComponents(){
    return component;
}
void GrowingMedium::setName(string name){
    this->name=name;
}
void GrowingMedium::setTotalComponents(unsigned short totalComponents){
    this->totalComponents=totalComponents;
}
void GrowingMedium::setComponents(string** component){
    this->component=new string*[2];
    for(int n=0;n<2;n++){
        this->component[n]=new string[totalComponents];
    }
    for(int n=0;n<2;n++){
        for(int i=0;i<this->totalComponents;i++){
            this->component[n][i]=component[n][i];
        }
    }
}
void GrowingMedium::output(){
    cout<<name<<endl;
    cout<<"Components - Parts"<<endl;
    for(int n=0;n<totalComponents;n++){
        cout<<n+1<<" "<<component[0][n]<<" "<<component[1][n]<<endl;
    }
}
void GrowingMedium::assignComponent(string component,unsigned short loc){
    this->component[0][loc]=component;
}
void GrowingMedium::assignPart(string part,unsigned short loc){
    this->component[1][loc]=part;
}
string GrowingMedium::getComponent(unsigned short loc){
    return component[0][loc];
}
string GrowingMedium::getPart(unsigned short loc){
    return component[1][loc];
}

string Garden::getName(){
    string temp;
    for(int n=0;n<strlen(name);n++){
        temp[n]=name[n];
    }
    return temp;
}
string Garden::getPlantName(){
    string temp;
    for(int n=0;n<strlen(plant->getName());n++){
        temp[n]=plant->getName()[n];
    }
    return temp;
}
string Garden::getTypeString(){
    string temp;
    for(int n=0;n<strlen(type);n++){
        temp[n]=type[n];
    }
    return temp;
}
short Garden::getGardenSize(){
    return gardenSize;
}

Plant::Plant(){
    
}
string Plant::getName(string){
    string temp;
    for(int n=0;n<strlen(name);n++){
        temp[n]=name[n];
    }
    return temp;
}
char* Plant::getName(){
    return name;
}
Plant::~Plant(){
    delete[] name;
    name=NULL;
    delete[] description;
    description=NULL;
    delete[] journal;
    journal=NULL;
}

Journal::Journal(){
    entry[0]='\0';
    date[0]='\0';
}
Journal::Journal(char* date){
    entry[0]='\0';
    this->date=date;
}
Journal::Journal(char* date,char*entry){
    this->entry=entry;
    this->date=date;
}
Journal::Journal(const Journal &journal){
    if(this==&journal){
        this->entry[0]='\0';
        this->date[0]='\0';
    }else{
        this->entry=new char[entrySize];
        for(int n=0;n<entrySize;n++){
            this->entry[n]=journal.entry[n];
        }
        this->date=new char[dateSize];
        for(int n=0;n<dateSize;n++){
            this->date[n]=journal.date[n];
        }
    };
}
Journal Journal::operator=(const Journal &journal){
    if(this==&journal){
        return *this;
    }else{
        this->entry=new char[entrySize];
        for(int n=0;n<entrySize;n++){
            this->entry[n]=journal.entry[n];
        }
        this->date=new char[dateSize];
        for(int n=0;n<dateSize;n++){
            this->date[n]=journal.date[n];
        }
    };
}
void Journal::setEntry(char* entry){
    this->entry=entry;
}
void Journal::setDate(char* date){
    this->date=date;
}
char* Journal::getEntry(){
    return entry;
}
char* Journal::getDate(){
    return date;
}
Journal::~Journal(){
    delete[] entry;
    entry=NULL;
    delete[] date;
    date=NULL;
}
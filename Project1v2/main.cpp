/* 
 * File:   main.cpp
 * Author: Jesus Parias-Castillo
 * Purpose Project1
 * Created on May 11, 2019, 10:28 PM
 */

#include <cstdlib>
#include <iostream>
#include <ctime>
#include <string>
#include <iomanip>
#include <fstream>

using namespace std;

enum PotMaterial{
    None,
    Clay,
    Plastic,
    Fabric,
    Glass,
    Ceramic,
    Wood,
    Metal
};
enum PotColour{
    Black,
    White,
    Grey,
    Brown,
    Blue,
    Red,
    Green,
    Yellow,
    Purple,
    Pink,
    Orange,
    Clear,
    Pattern
};
enum Climate{
    Forrest,
    Desert,
    Tropic,
    Mountain,
    Tundra,
    Savanah
};
enum MediumComponents{
    Soil,
    Compost,
    PeatMoss,
    Perlite,
    Vermiculite
};
enum GardenType{
    ground,
    potted,
    hanging,
    raisedBed,
    hydroponic,
    aquaponic
};
enum Lighting{
    fullShade,
    partSun,
    partShade,
    fullSun,
    indirectLight
};

class Journal{
private:
    string name;
    int numPages;
    string* entries;
    string* dates;
public:
    Journal();
    Journal(string); //given date
    Journal(string,int); //given date then entry
    Journal(const Journal &);
    Journal operator=(const Journal &);
    ~Journal();
    void setNumPages(int);
    void setName(string);
    void setEntry(string,int);
    void setDate(string,int);
    void setEntries(string*,int);
    void setDates(string*,int);
    int getNumPages();
    string getName();
    string getEntry(int);
    string getDate(int);
    string* getEntries();
    string* getDates();
    void output();
    void output(int);
};

class GrowMedium{
private:
    string name;
    int numberOfComps;
    int*comps;
public:
    GrowMedium();
    GrowMedium(string,int);
    GrowMedium(const GrowMedium &);
    GrowMedium operator=(const GrowMedium &);
    ~GrowMedium();
    void setName(string);
    void setNumberOfComps(int);
    void setCompPart(int,int);
    string getName();
    int getNumberOfComps();
    int getComp(int);
    void output();
    
    int& operator[](int);
};
class Pot{
private:
    int material;
    int color;
    int size;   //in gallons
public:
    Pot();
    Pot(int);   //size
    Pot(int,int);   //size,material
    Pot(int,int,int);   //,all 3
    Pot(const Pot &);
    Pot operator=(const Pot &);
    ~Pot();
    void setMaterial(int);
    void setColor(int);
    void setSize(int);
    int getMaterial();
    int getColor();
    int getSize();
    void output();
};
class Plant{
private:
    string name;
    GrowMedium medium;
    Pot pot;
    int age;
    int height;
    Journal journal;
    string description;
public:
    Plant();
    Plant (const Plant &);
    Plant operator=(const Plant &);
    ~Plant();
    void setJournalName(string);
    void setJournalNumPages(int);
    void setName(string);
    void setMedium(GrowMedium);
    void setPot(Pot);
    void setAge(int);
    void setHeight(int);
    void setDescription(string);
    string getName();
    GrowMedium getMedium();
    Pot getPot();
    int getAge();
    int getHeight();
    string getDescription();
    void output();
    void setJournalDate(string,int);
    void setJournalEntry(string,int);
    int getJournalNumPages();
    string getJournalName();
};
class Garden{
private:
    string name;
    int numberOfPlants;
    int area;
    int lighting;
    int climate;
    bool insidePlant;
    Plant* plants;
};

string getMediumComponentString(int);
string getMaterialString(int);
string getColorString(int);

int main(){
    GrowMedium soil;
    soil.setName("Mixed Soil");
    soil.setNumberOfComps(2);
    soil.setCompPart(0,Soil);
    soil.setCompPart(1,Compost);
    soil.output();
    
    Pot plastic;
    plastic.setMaterial(Plastic);
    plastic.setColor(Black);
    plastic.setSize(2);
    plastic.output();
    
    Plant hemp;
    hemp.setName("Hemp");
    hemp.setAge(10);
    hemp.setDescription("To make rope \n and stuff");
    hemp.setHeight(48);
    hemp.setPot(plastic);
    hemp.setMedium(soil);
    //hemp.output();
    
    hemp.setJournalNumPages(4);
    hemp.setJournalDate("2/7/96",0);
    //hemp.setJournalEntry("Today was a very good Day. I really had fun with my family at the mall today.",0);
    //hemp.setJournalDate("2/9/96",1);
    //hemp.setJournalEntry("I hate today so much. That bitch Abigail cut my leg with her stupid pencil.",1);
    //hemp.setJournalDate("2/9/96",2);
    //hemp.setJournalEntry("I hate today so much. That bitch Abigail cut my leg with her stupid pencil.",2);
    hemp.output();
    return 0;
}

Plant::Plant(){
    name="";
    age=0;
    height=0;
    description="";
}
Plant::Plant(const Plant &plant){
    this->age=plant.age;
    this->height=plant.height;
    this->medium=plant.medium;
    this->description=plant.description;
    this->journal=plant.journal;
    this->pot=plant.pot;
    this->name=plant.name;
}
Plant Plant::operator=(const Plant &plant){
    if(this==&plant){
        return *this;
    }
    this->age=plant.age;
    this->height=plant.height;
    this->medium=plant.medium;
    this->description=plant.description;
    this->journal=plant.journal;
    this->pot=plant.pot;
    this->name=plant.name;
}
Plant::~Plant(){
}
void Plant::setJournalName(string x){
    journal.setName(x);
}
void Plant::setJournalNumPages(int numPages){
    journal.setNumPages(numPages);
}
void Plant::setName(string name){
    this->name=name;
}
void Plant::setMedium(GrowMedium m){
    this->medium=m;
}
void Plant::setPot(Pot pot){
    this->pot=pot;
}
void Plant::setAge(int age){
    this->age=age;
}
void Plant::setHeight(int height){
    this->height=height;
}
void Plant::setDescription(string description){
    this->description=description;
}
string Plant::getName(){
    return name;
}
GrowMedium Plant::getMedium(){
    return medium;
}
Pot Plant::getPot(){
    return pot;
}
int Plant::getAge(){
    return age;
}
int Plant::getHeight(){
    return height;
}
string Plant::getDescription(){
    return description;
}
void Plant::output(){
    cout<<name<<endl;
    cout<<"Age: "<<age<<" days old"<<endl;
    cout<<"Height: "<<height<<"cm"<<endl;
    cout<<pot.getSize()<<" gallon pot"<<endl;
    cout<<medium.getName()<<" soil mix"<<endl;
    cout<<"Description: "<<description<<endl;
    cout<<"Journal Entries: "<<getJournalNumPages()<<endl;
}
void Plant::setJournalEntry(string entry,int loc){
    journal.setEntry(entry,loc);
}
void Plant::setJournalDate(string date,int loc){
    journal.setDate(date,loc);
}
int Plant::getJournalNumPages(){
    return journal.getNumPages();
}
string Plant::getJournalName(){
    return journal.getName();
}

Pot::Pot(){
    material=0;
    color=0;
    size=0;
}
Pot::Pot(int size){
    material=0;
    color=0;
    this->size=size;
}
Pot::Pot(int size,int material){
    this->material=material;
    color=0;
    this->size=size;
}
Pot::Pot(int size,int material,int color){
    this->material=material;
    this->color=color;
    this->size=size;
}
Pot::Pot(const Pot &pot){
    this->color=pot.color;
    this->size=pot.size;
    this->material=pot.material;
}
Pot Pot::operator=(const Pot &pot){
    if(this==&pot){
        return *this;
    }
    this->color=pot.color;
    this->size=pot.size;
    this->material=pot.material;
}
Pot::~Pot(){
    
}
string getMaterialString(int material){
    if(material==None){
        return "No";
    }else if(material==Clay){
        return "Clay";
    }else if(material==Plastic){
        return "Plastic";
    }else if(material==Fabric){
        return "Fabric";
    }else if(material==Ceramic){
        return "Ceramic";
    }else if(material==Wood){
        return "Wood";
    }else if(material==Metal){
        return "Metal";
    }
}
string getColorString(int color){
    if(color==Black){
        return "Black";
    }else if(color==White){
        return "White";
    }else if(color==Grey){
        return "Grey";
    }else if(color==Blue){
        return "Blue";
    }else if(color==Red){
        return "Red";
    }else if(color==Yellow){
        return "Yellow";
    }else if(color==Orange){
        return "Orange";
    }else if(color==Purple){
        return "Purple";
    }else if(color==Green){
        return "Green";
    }else if(color==Pink){
        return "Pink";
    }else if(color==Clear){
        return "Clear";
    }else if(color==Brown){
        return "Brown";
    }else if(color==Pattern){
        return "Pattern";
    }
}
void Pot::setMaterial(int material){
    this->material=material;
}
void Pot::setColor(int color){
    this->color=color;
}
void Pot::setSize(int size){
    this->size=size;
}
int Pot::getMaterial(){
    return material;
}
int Pot::getColor(){
    return color;
}
int Pot::getSize(){
    return size;
}
void Pot::output(){
    cout<<getMaterialString(material)<<" Pot"<<endl;
    cout<<size<<" gallons"<<endl;
    cout<<getColorString(color)<<endl;
}

GrowMedium::GrowMedium(){
    name="";
    numberOfComps=0;
    comps=new int[numberOfComps];
}
GrowMedium::GrowMedium(string name,int numberOfComps){
    this->name=name;
    this->numberOfComps=numberOfComps;
    comps=new int[this->numberOfComps];
}
GrowMedium::GrowMedium(const GrowMedium &growMedium){
    this->name=growMedium.name;
    this->numberOfComps=growMedium.numberOfComps;
    this->comps=new int[this->numberOfComps];
    for(int n=0;n<this->numberOfComps;n++){
        this->comps[n]=growMedium.comps[n];
    }
}
GrowMedium GrowMedium::operator =(const GrowMedium &growMedium){
    if(this==&growMedium){
        return *this;
    }
    this->name=growMedium.name;
    this->numberOfComps=growMedium.numberOfComps;
    delete[] this->comps;
    this->comps=new int[this->numberOfComps];
    for(int n=0;n<this->numberOfComps;n++){
        this->comps[n]=growMedium.comps[n];
    }
}
GrowMedium::~GrowMedium(){
    delete[] comps;
    comps = NULL;
}
string getMediumComponentString(int material){
    if(material==Soil){
        return "Soil";
    }else if(material==Compost){
        return "Compost";
    }else if(material==PeatMoss){
        return "Peat Moss";
    }else if(material==Perlite){
        return "Perlite";
    }else if(material==Vermiculite){
        return "Vermiculite";
    }else return "Error";
}
string GrowMedium::getName(){
    return name;
}
int GrowMedium::getNumberOfComps(){
    return numberOfComps;
}
int GrowMedium::getComp(int loc){
    return comps[loc];
}
void GrowMedium::setName(string name){
    this->name=name;
}
void GrowMedium::setNumberOfComps(int numberOfComps){
    this->numberOfComps=numberOfComps;
    this->comps=new int[numberOfComps];
}
void GrowMedium::setCompPart(int loc,int comp){
    comps[loc]=comp;
}
int& GrowMedium::operator[](int loc){
    return comps[loc];
}
void GrowMedium::output(){
    cout<<name<<endl;
    cout<<"Components:"<<endl;
    for(int n=0;n<numberOfComps;n++){
        cout<<n+1<<": "<<getMediumComponentString(comps[n])<<endl;
    }
}

Journal::Journal(){
    name="";
    numPages=1;
    entries=new string[numPages];
    dates=new string[numPages];
    entries[0]="";
    dates[0]="";
}
Journal::Journal(string name){
    this->name=name;
    this->numPages=1;
    this->entries=new string[this->numPages];
    this->dates=new string[this->numPages];
    this->entries[0]="";
    this->dates[0]="";
}
Journal::Journal(string name,int numPages){
    this->name=name;
    this->numPages=numPages;
    this->entries=new string[this->numPages];
    this->dates=new string[this->numPages];
    for(int n=0;n<numPages;n++){
        this->entries[n]="";
        this->dates[n]="";
    }
}
Journal::Journal(const Journal &journal){
    this->name=journal.name;
    this->numPages=journal.numPages;
    this->entries=new string[this->numPages];
    this->dates=new string[this->numPages];
    for(int n=0;n<numPages;n++){
        this->entries[n]=journal.entries[n];;
        this->dates[n]=journal.dates[n];
    }
}
Journal Journal::operator=(const Journal &journal){
    if(this==&journal){
        return *this;
    }
    this->name=journal.name;
    this->numPages=journal.numPages;
    delete[] entries;
    delete[] dates;
    this->entries=new string[this->numPages];
    this->dates=new string[this->numPages];
    for(int n=0;n<numPages;n++){
        this->entries[n]=journal.entries[n];;
        this->dates[n]=journal.dates[n];
    }
}
void Journal::setName(string n){
    this->name=n;
}
void Journal::setNumPages(int numPages){
    this->numPages=numPages;
}
void Journal::setEntry(string entry,int loc){
    this->entries[loc]=entry;
}
void Journal::setDate(string date,int loc){
    this->dates[loc]=date;
}
void Journal::setDates(string* dates, int numPages){
    this->numPages=numPages;
    this->dates=new string[numPages];
    for(int n=0;n<this->numPages;n++){
        this->dates[n]=dates[n];
    }
}
void Journal::setEntries(string* entries, int numPages){
    this->numPages=numPages;
    this->entries=new string[numPages];
    for(int n=0;n<this->numPages;n++){
        this->entries[n]=entries[n];
    }
}
int Journal::getNumPages(){
    return numPages;
}
string Journal::getName(){
    return name;
}
string* Journal::getDates(){
    return dates;
}
string* Journal::getEntries(){
    return entries;
}
string Journal::getEntry(int loc){
    return entries[loc];
}
string Journal::getDate(int loc){
    return dates[loc];
}
Journal::~Journal(){
    delete[] entries;
    entries=NULL;
    delete[] dates;
    dates=NULL;
}
void Journal::output(){
    cout<<name<<endl;
    for(int n=0;n<numPages;n++){
        output(n);
    }
}
void Journal::output(int loc){
    cout<<"Entry "<<loc+1<<endl;
    cout<<"   Date: "<<dates[loc]<<endl;
    cout<<"   "<<entries[loc]<<endl<<"   ";
}
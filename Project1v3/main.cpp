/* 
 * File:   main.cpp
 * Author: Jesus Parias-Castillo
 * Purpose Project1
 * Created on May 11, 2019, 10:28 PM
 */

#include <cstdlib>
#include <iostream>
#include <ctime>
#include <string>
#include <iomanip>
#include <fstream>

using namespace std;

enum PotMaterial{
    None,
    Clay,
    Plastic,
    Fabric,
    Glass,
    Ceramic,
    Wood,
    Metal
};
enum PotColour{
    Black,
    White,
    Grey,
    Brown,
    Blue,
    Red,
    Green,
    Yellow,
    Purple,
    Pink,
    Orange,
    Clear,
    Pattern
};
enum Climate{
    Forrest,
    Desert,
    Tropic,
    Mountain,
    Tundra,
    Savanah
};
enum MediumComponents{
    Soil,
    Compost,
    PeatMoss,
    Perlite,
    Vermiculite
};
enum GardenType{
    ground,
    potted,
    hanging,
    raisedBed,
    hydroponic,
    aquaponic
};
enum Lighting{
    fullShade,
    partSun,
    partShade,
    fullSun,
    indirectLight
};

struct Entry{
    char* entry;
    char* date;
};
struct Journal{
    char* name;
    int numPages;
    int numEntrys;
    Entry* entries;
};


class GrowMedium{
private:
    string name;
    int numberOfComps;
    int*comps;
public:
    GrowMedium();
    GrowMedium(string,int);
    GrowMedium(const GrowMedium &);
    GrowMedium operator=(const GrowMedium &);
    ~GrowMedium();
    void setName(string);
    void setNumberOfComps(int);
    void setCompPart(int,int);
    string getName();
    int getNumberOfComps();
    int getComp(int);
    void output();
    
    int& operator[](int);
};
class Pot{
private:
    int material;
    int color;
    int size;   //in gallons
public:
    Pot();
    Pot(int);   //size
    Pot(int,int);   //size,material
    Pot(int,int,int);   //,all 3
    Pot(const Pot &);
    Pot operator=(const Pot &);
    ~Pot();
    void setMaterial(int);
    void setColor(int);
    void setSize(int);
    int getMaterial();
    int getColor();
    int getSize();
    void output();
};
class Plant{
private:
    string name;
    GrowMedium medium;
    Pot pot;
    int age;
    int height;
    Journal journal;
    string description;
public:
    Plant();
    Plant (const Plant &);
    Plant operator=(const Plant &);
    ~Plant();
    void setJournalName(char*);
    void setJournalNumPages(int);
    void setName(string);
    void setMedium(GrowMedium);
    void setPot(Pot);
    void setAge(int);
    void setHeight(int);
    void setDescription(string);
    string getName();
    GrowMedium getMedium();
    Pot getPot();
    int getAge();
    int getHeight();
    string getDescription();
    void output();
    void setJournalDate(char*,int);
    void setJournalEntry(char*,int);
    int getJournalNumPages();
    string getJournalName();
    Journal initializeJournal();
    Journal initializeJournal(int);
    Journal initializeJournal(const Journal &journal);
    Journal initializeJournal(char*,int);
    Plant &operator[](int);
};

string getMediumComponentString(int);
string getMaterialString(int);
string getColorString(int);
void mainMenu(char*,char*,int&,Plant*);
void mainMenuValidation(char*,int);
void gardenMenuValidation(char*,int);
void editGardenValidation(char*);
void myGardenMenu(char*,char*,int&,Plant*);
void plantSpeciesRecordMenu();
void growingInstructionsMenu();
void growingMediumsMenu(GrowMedium,unsigned short,char*);
void growingMediumMenuValidation(char*,unsigned short);
void helpMenu(char*,char*,int&,Plant*);
void editGardenMenu(char*,char*,int&,Plant*);
void addPlantMenu(char*,char*,int&,Plant*);
void removePlantMenu(char*,char*,int&,Plant*);

int main(){
    GrowMedium soil;
    soil.setName("Mixed Soil");
    soil.setNumberOfComps(2);
    soil.setCompPart(0,Soil);
    soil.setCompPart(1,Compost);
    //soil.output();
    
    Pot plastic;
    plastic.setMaterial(Plastic);
    plastic.setColor(Black);
    plastic.setSize(2);
    //plastic.output();
    
    Plant hemp;
    hemp.setName("Hemp");
    hemp.setAge(10);
    hemp.setDescription("To make rope \n and stuff");
    hemp.setHeight(48);
    hemp.setPot(plastic);
    hemp.setMedium(soil);
    //hemp.output();
    
    hemp.setJournalNumPages(4);
    hemp.setJournalName("Journal Buddy");
    hemp.setJournalDate("2/7/96",0);
    hemp.setJournalEntry("Today was a very good Day. I really had fun with my family at the mall today.",0);
    hemp.setJournalDate("2/9/96",1);
    hemp.setJournalEntry("I hate today so much. That bitch Abigail cut my leg with her stupid pencil.",1);
    hemp.setJournalDate("2/9/96",2);
    hemp.setJournalEntry("I hate today so much. That bitch Abigail cut my leg with her stupid pencil.",2);
    //hemp.output();
    
    int numberOfPlants=2;
    Plant* plants= new Plant[numberOfPlants];
    char* gardenName=new char[30];
    gardenName="My Garden Thing";
    char* choice=new char[20];
    mainMenu(choice,gardenName,numberOfPlants,plants);
    
    delete[] plants;
    plants=NULL;
    delete[] gardenName;
    gardenName=NULL;
    delete[] choice;
    choice=NULL;
    return 0;
}
void growingMediumsMenu(GrowMedium mediums,unsigned short size,char* choice){
    cout<<"Growing Mediums"<<endl;
    //for(int n=0;n<mediumCount;n++){
        mediums.getName();
    //}
    cout<<"[E] Edit Growing Mediums"<<endl;
    cout<<"[B] <--Back"<<endl;
    
    cin>>choice;
    growingMediumMenuValidation(choice,size);
    
    if(isdigit(choice[0])){
        cout<<choice;
        mediums.output();
    }
    if(choice[0]=='B'||choice[0]=='b'){
        cout<<"Backing"<<endl;
    }
}
void growingMediumMenuValidation(char* choice, unsigned short max){
    bool choiceValidation;
    if(choice[0]=='B'||choice[0]=='b'){
        choiceValidation=true;
    }
    if(choice[0]=='E'||choice[0]=='e'){
        choiceValidation=true;
    }
    if(atoi(choice)>0&&atoi(choice)<=max){
        choiceValidation=true;
    }
    if(choiceValidation==false){
        cout<<"Please enter a valid option: ";
        cin>>choice;
        growingMediumMenuValidation(choice,max);
    }
}
void EditGrowingMediumsMenu(){
    cout<<"[A] Add Growing Medium"<<endl;
    cout<<"[D] Delete Growing Medium"<<endl;
    cout<<"[S] Sort Mediums"<<endl;
    cout<<"[B] <--Back"<<endl;
}
void mainMenu(char* choice,char* gardenName,int &numberOfPlants,Plant* plants){
    cout<<"Gardeners Journal"<<endl;
    cout<<"[1] My Plants so Far"<<endl;
    cout<<"[2] Growing Mediums"<<endl;
    cout<<"[3] Garden Journal"<<endl;
    cout<<"[X] Exit App"<<endl;
    cout<<"For Help press H"<<endl;
    
    cin>>choice;
    mainMenuValidation(choice,3);
    
    if(choice[0]=='1'){
        myGardenMenu(choice,gardenName,numberOfPlants,plants);
    }
    if(choice[0]=='2'){
        //displayMediums();
    }
    if(choice[0]=='3'){
        //diplayGardenJournal();
    }
    if(choice[0]=='X'||choice[0]=='x'){
        cout<<"Bye"<<endl;
    }
    if(choice[0]=='H'||choice[0]=='h'){
        helpMenu(choice,gardenName,numberOfPlants,plants);
    }
}
void mainMenuValidation(char* choice, int max){
    bool choiceValidation;
    if(choice[0]=='x'||choice[0]=='X'){
        choiceValidation=true;
    }
    if(choice[0]=='h'||choice[0]=='H'){
        choiceValidation=true;
    }
    if(atoi(choice)>0&&atoi(choice)<=max){
        choiceValidation=true;
    }
    if(choiceValidation==false){
        cout<<"Please enter a valid option: ";
        cin>>choice;
        mainMenuValidation(choice,max);
    }
}
void gardenMenuValidation(char* choice, int max){
    bool choiceValidation;
    if(choice[0]=='A'||choice[0]=='a'){
        choiceValidation=true;
    }
    if(choice[0]=='b'||choice[0]=='B'){
        choiceValidation=true;
    }
    if(atoi(choice)>0&&atoi(choice)<=max){
        choiceValidation=true;
    }
    if(choiceValidation==false){
        cout<<"Please enter a valid option: ";
        cin>>choice;
        gardenMenuValidation(choice,max);
    }
}
void editGardenValidation(char* choice){
    bool choiceValidation;
    if(choice[0]=='A'||choice[0]=='a'){
        choiceValidation=true;
    }
    if(choice[0]=='D'||choice[0]=='d'){
        choiceValidation=true;
    }
    if(choice[0]=='B'||choice[0]=='b'){
        choiceValidation=true;
    }
    if(choiceValidation==false){
        cout<<"Please enter a valid option: ";
        cin>>choice;
        editGardenValidation(choice);
    }
}
void helpMenu(char* choice,char* gardenName,int &numberOfPlants,Plant* plants){
    cout<<"To navigate press the key on the left colum that is within the brackets [_] "<<endl;
    cout<<"You can always go back at any moment to the previous screen"<<endl;
    cout<<"If you would like to use a text file to input information, please"<<endl;
    cout<<"     modify the text file templates located within the home folders"<<endl;
    cout<<"[B] <--Back"<<endl;
    cin>>choice;
    mainMenu(choice,gardenName,numberOfPlants,plants);
}
void myGardenMenu(char* choice,char* gardenName,int &numberOfPlants,Plant* plants){
    cout<<gardenName<<endl;
    for(int n=0;n<numberOfPlants;n++){
        cout<<"["<<n+1<<"]"<<" "<<plants[n].getName()<<endl;
    }cout<<endl;
    cout<<"[A] Edit Plants"<<endl;
    cout<<"[B] <-- Back"<<endl;
    cin>>choice;
    gardenMenuValidation(choice,numberOfPlants);
    if(atoi(choice)){
        plants[atoi(choice)-1].output();
    }
    if(choice[0]=='A'||choice[0]=='a'){
        editGardenMenu(choice,gardenName,numberOfPlants,plants);
    }
    if(choice[0]=='B'||choice[0]=='b'){
        mainMenu(choice,gardenName,numberOfPlants,plants);
    }
}
void editGardenMenu(char* choice,char* gardenName,int &numberOfPlants,Plant* plants){
    cout<<"[A] Add a Plant"<<endl;
    cout<<"[D] Get Rid of a Plant"<<endl;
    cout<<"[B] <--Back"<<endl;
    cin>>choice;
    editGardenValidation(choice);
    if(choice[0]=='A'||choice[0]=='a'){
        addPlantMenu(choice,gardenName,numberOfPlants,plants);
    }
    if(choice[0]=='D'||choice[0]=='d'){
        //removePlantMenu(choice,gardenName,numberOfPlants,plants);
    }
    if(choice[0]=='B'||choice[0]=='b'){
        myGardenMenu(choice,gardenName,numberOfPlants,plants);
    }
}
void addPlantMenu(char* choice,char*gardenName,int &numberOfPlants,Plant* plants){
    numberOfPlants++;
    Plant* temp=new Plant[numberOfPlants];
    for(int n=0;n<numberOfPlants-1;n++){
        temp[n] = plants[n];
    }
    delete[] plants;
    plants=new Plant[numberOfPlants];
    for(int n=0;n<numberOfPlants;n++){
        plants[n].operator =(temp[n]);
    }
    delete[]temp;
    
    cout<<"What would you like to name this plant?:";
    cin>>choice;
    //plants[numberOfPlants-1].setName(choice);
}
void removePlantMenu(char* choice,char*gardenName,int &numberOfPlants,Plant* plants){
    
}
Plant& Plant::operator[](int loc){
    return this[loc];
}


Plant::Plant(){
    name="";
    age=0;
    height=0;
    description="";
    journal=initializeJournal();
}
Plant::Plant(const Plant &plant){
    this->age=plant.age;
    this->height=plant.height;
    this->medium=plant.medium;
    this->description=plant.description;
    this->journal=plant.journal;
    this->pot=plant.pot;
    this->name=plant.name;
    this->journal=initializeJournal(plant.journal.name,plant.journal.numPages);
}
Plant Plant::operator=(const Plant &plant){
    if(this==&plant){
        return *this;
    }
    this->age=plant.age;
    this->height=plant.height;
    this->medium=plant.medium;
    this->description=plant.description;
    this->journal=plant.journal;
    this->pot=plant.pot;
    this->name=plant.name;
    this->initializeJournal();
    for(int n=0;n<journal.numEntrys;n++){
        this->journal.entries[n].date=journal.entries[n].date;
        this->journal.entries[n].entry=journal.entries[n].entry;
    }
}
Plant::~Plant(){
    delete[] journal.entries;
    journal.entries=NULL;
}
void Plant::setJournalName(char* name){
    journal.name=new char[100];
    for(int n=0;name[n]!='\0';n++){
        journal.name[n]=name[n];
    }
}
void Plant::setJournalNumPages(int numPages){
    Journal temp;
    temp.entries=new Entry[numPages];
    for(int n=0;n<numPages;n++){
        temp.entries[n].entry=new char[1000];
        temp.entries[n].date=new char[20];
    }
    for(int n=0;n<journal.numPages;n++){
        for(int i=0;journal.entries[n].entry[i]!='\0';i++){
            temp.entries[n].entry[i]=journal.entries[n].entry[i];
        }
        for(int i=0;journal.entries[n].date[i]!='\0';i++){
            temp.entries[n].date[i]=journal.entries[n].date[i];
        }
    }  
    delete[] journal.entries;
    this->journal.entries=new Entry[numPages];
    for(int n=0;n<numPages;n++){
        journal.entries[n].entry=new char[1000];
        journal.entries[n].date=new char[20];
    }
    for(int n=0;n<numPages;n++){
        for(int i=0;temp.entries[n].entry[i]!='\0';i++){
           journal.entries[n].entry[i]=temp.entries[n].entry[i];
        }
        for(int i=0;temp.entries[n].date[i]!='\0';i++){
           journal.entries[n].date[i]=temp.entries[n].date[i];
        }
    }
    journal.numPages=numPages;
}
void Plant::setName(string name){
    this->name=name;
}
void Plant::setMedium(GrowMedium medium){
    this->medium=medium;
}
void Plant::setPot(Pot pot){
    this->pot=pot;
}
void Plant::setAge(int age){
    this->age=age;
}
void Plant::setHeight(int height){
    this->height=height;
}
void Plant::setDescription(string description){
    this->description=description;
}
string Plant::getName(){
    return name;
}
GrowMedium Plant::getMedium(){
    return medium;
}
Pot Plant::getPot(){
    return pot;
}
int Plant::getAge(){
    return age;
}
int Plant::getHeight(){
    return height;
}
string Plant::getDescription(){
    return description;
}
void Plant::output(){
    cout<<name<<endl;
    cout<<"Age: "<<age<<" days old"<<endl;
    cout<<"Height: "<<height<<"cm"<<endl;
    cout<<pot.getSize()<<" gallon pot"<<endl;
    cout<<medium.getName()<<" soil mix"<<endl;
    cout<<"Description: "<<description<<endl;
    cout<<endl<<journal.name<<endl;
    cout<<"Journal Entries: "<<journal.numPages<<endl;
    for(int n=0;n<journal.numPages;n++){
        cout<<"Entry "<<n+1<<endl;
        cout<<"     "<<journal.entries[n].date<<endl;
        cout<<"     "<<journal.entries[n].entry<<endl;
    }
}
void Plant::setJournalEntry(char* entry,int loc){
    for(int n=0;entry[n]!='\0';n++){
        journal.entries[loc].entry[n]=entry[n];
    }
}
void Plant::setJournalDate(char* date,int loc){
    for(int n=0;date[n]!='\0';n++){
        journal.entries[loc].date[n]=date[n];
    }
}
int Plant::getJournalNumPages(){
    return journal.numPages;
}
string Plant::getJournalName(){
    return journal.name;
}
Journal Plant::initializeJournal(){
    Journal journal;
    journal.name=new char[20];
    journal.name[0]='\0';
    journal.numPages=5;
    journal.numEntrys=0;
    journal.entries=new Entry[journal.numPages];
    for(int n=0;n<journal.numPages;n++){
        journal.entries[n].entry=new char[1000];
        journal.entries[n].date=new char[20];
        journal.entries[n].entry[0]='\0';
        journal.entries[n].date[0]='\0';
    }
    return journal;
}
Journal Plant::initializeJournal(int numPages){
    Journal journal;
    journal.name=new char[20];
    journal.name[0]='\0';
    journal.numPages=numPages;
    journal.numEntrys=0;
    journal.entries=new Entry[journal.numPages];
    for(int n=0;n<journal.numPages;n++){
        journal.entries[n].entry=new char[1000];
        journal.entries[n].date=new char[20];
        journal.entries[n].entry[0]='\0';
        journal.entries[n].date[0]='\0';
    }
    return journal;
}
Journal Plant::initializeJournal(char* name,int numPages){
    Journal journal;
    journal.name=new char[20];
    for(int n=0;name[n]!='\0';n++){
        journal.name[n]=name[n];
    }
    journal.name[0]='\0';
    journal.numPages=numPages;
    journal.numEntrys=0;
    journal.entries=new Entry[journal.numPages];
    for(int n=0;n<journal.numPages;n++){
        journal.entries[n].entry=new char[1000];
        journal.entries[n].date=new char[20];
        journal.entries[n].entry[0]='\0';
        journal.entries[n].date[0]='\0';
    }
    return journal;
}
Journal Plant::initializeJournal(const Journal &journal){
    Journal temp(journal);
    temp.name=new char[20];
    for(int n=0;name[n]!='\0';n++){
        temp.name[n]=journal.name[n];
    }
    temp.numPages=journal.numPages;
    temp.numEntrys=journal.numEntrys;
    temp.entries=new Entry[temp.numPages];
    for(int n=0;n<journal.numPages;n++){
        temp.entries[n].entry=new char[1000];
        temp.entries[n].date=new char[20];
    }
    return temp;
}

Pot::Pot(){
    material=0;
    color=0;
    size=0;
}
Pot::Pot(int size){
    material=0;
    color=0;
    this->size=size;
}
Pot::Pot(int size,int material){
    this->material=material;
    color=0;
    this->size=size;
}
Pot::Pot(int size,int material,int color){
    this->material=material;
    this->color=color;
    this->size=size;
}
Pot::Pot(const Pot &pot){
    this->color=pot.color;
    this->size=pot.size;
    this->material=pot.material;
}
Pot Pot::operator=(const Pot &pot){
    if(this==&pot){
        return *this;
    }
    this->color=pot.color;
    this->size=pot.size;
    this->material=pot.material;
}
Pot::~Pot(){
    
}
string getMaterialString(int material){
    if(material==None){
        return "No";
    }else if(material==Clay){
        return "Clay";
    }else if(material==Plastic){
        return "Plastic";
    }else if(material==Fabric){
        return "Fabric";
    }else if(material==Ceramic){
        return "Ceramic";
    }else if(material==Wood){
        return "Wood";
    }else if(material==Metal){
        return "Metal";
    }
}
string getColorString(int color){
    if(color==Black){
        return "Black";
    }else if(color==White){
        return "White";
    }else if(color==Grey){
        return "Grey";
    }else if(color==Blue){
        return "Blue";
    }else if(color==Red){
        return "Red";
    }else if(color==Yellow){
        return "Yellow";
    }else if(color==Orange){
        return "Orange";
    }else if(color==Purple){
        return "Purple";
    }else if(color==Green){
        return "Green";
    }else if(color==Pink){
        return "Pink";
    }else if(color==Clear){
        return "Clear";
    }else if(color==Brown){
        return "Brown";
    }else if(color==Pattern){
        return "Pattern";
    }
}
void Pot::setMaterial(int material){
    this->material=material;
}
void Pot::setColor(int color){
    this->color=color;
}
void Pot::setSize(int size){
    this->size=size;
}
int Pot::getMaterial(){
    return material;
}
int Pot::getColor(){
    return color;
}
int Pot::getSize(){
    return size;
}
void Pot::output(){
    cout<<getMaterialString(material)<<" Pot"<<endl;
    cout<<size<<" gallons"<<endl;
    cout<<getColorString(color)<<endl;
}

GrowMedium::GrowMedium(){
    name="";
    numberOfComps=0;
    comps=new int[numberOfComps];
}
GrowMedium::GrowMedium(string name,int numberOfComps){
    this->name=name;
    this->numberOfComps=numberOfComps;
    comps=new int[this->numberOfComps];
}
GrowMedium::GrowMedium(const GrowMedium &growMedium){
    this->name=growMedium.name;
    this->numberOfComps=growMedium.numberOfComps;
    this->comps=new int[this->numberOfComps];
    for(int n=0;n<this->numberOfComps;n++){
        this->comps[n]=growMedium.comps[n];
    }
}
GrowMedium GrowMedium::operator =(const GrowMedium &growMedium){
    if(this==&growMedium){
        return *this;
    }
    this->name=growMedium.name;
    this->numberOfComps=growMedium.numberOfComps;
    delete[] this->comps;
    this->comps=new int[this->numberOfComps];
    for(int n=0;n<this->numberOfComps;n++){
        this->comps[n]=growMedium.comps[n];
    }
}
GrowMedium::~GrowMedium(){
    delete[] comps;
    comps = NULL;
}
string getMediumComponentString(int material){
    if(material==Soil){
        return "Soil";
    }else if(material==Compost){
        return "Compost";
    }else if(material==PeatMoss){
        return "Peat Moss";
    }else if(material==Perlite){
        return "Perlite";
    }else if(material==Vermiculite){
        return "Vermiculite";
    }else return "Error";
}
string GrowMedium::getName(){
    return name;
}
int GrowMedium::getNumberOfComps(){
    return numberOfComps;
}
int GrowMedium::getComp(int loc){
    return comps[loc];
}
void GrowMedium::setName(string name){
    this->name=name;
}
void GrowMedium::setNumberOfComps(int numberOfComps){
    this->numberOfComps=numberOfComps;
    this->comps=new int[numberOfComps];
}
void GrowMedium::setCompPart(int loc,int comp){
    comps[loc]=comp;
}
int& GrowMedium::operator[](int loc){
    return comps[loc];
}
void GrowMedium::output(){
    cout<<name<<endl;
    cout<<"Components:"<<endl;
    for(int n=0;n<numberOfComps;n++){
        cout<<n+1<<": "<<getMediumComponentString(comps[n])<<endl;
    }
}